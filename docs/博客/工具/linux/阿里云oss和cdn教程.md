---
title: 阿里云oss和cdn教程
date: 2020-08-12 14:53:40
permalink: /pages/577dfe/
categories: 
  - linux
tags: 
  - 
---


![alicloud_logo](https://img.wuxd.top/img/alicloud_logo.png)

在项目部署阶段，因为本身服务器在带宽，性能上的差距，免不了会使用一些云服务。国内比较有名的云服务商有阿里云，腾讯云，华为云等，本次以阿里云为例，记录一下阿里云oss和cdn的使用。

<!-- more -->

名词解释：

* OSS：(Object Storage Service) 对象存储，是一种云存储服务，主要面向服务器使用。阿里云的OSS可以提供数据存储归档，静态网页托管，图片处理，外链访问等等功能。本次主要使用OSS实现静态网页托管和图床服务。
* CDN：(Content Delivery Network或Content Distribution Network) 即内容分发网络，原理主要是在全国或全球设立许多的缓存服务器，将你网站的内容缓存到这些服务器，用户访问网站的时候，会通过DNS解析到就近的缓存服务器，从而实现内容的快速获取。我们主要使用阿里云的CDN服务来实现网站加速。

&nbsp;

&nbsp;

## 阿里云OSS+CDN实现静态网页托管并加速

### 一、新建Bucket

前端静态页面(包括html、css、js等文件)如果放在轻量的学生服务器上，1Mbps的带宽，对应128KB/s的理论下载速度，可以使得打开一个网页有7~8秒的延迟，让人不会再有打开的欲望。因此实现网站内容的加速是很必要的工作，我采用的方式是完全将前端页面放入阿里云OSS中，开启静态网页托管，再使用CDN加速，从而使得前端的数据可以快速加载。



首先打开阿里云OSS对象存储的管理页面，在右侧Bucket管理里点击创建Bucket。Bucket桶相当于一个独立的空间，可以使得不同的项目存储互不干扰。

![image-20200720135927877](https://img.wuxd.top/img/image-20200720135927877.png)

&nbsp;

部分关键词解释

详细信息可先查看[官方文档——创建存储空间](https://help.aliyun.com/document_detail/31885.html?spm=a2c4g.11174283.2.20.4b737da2B74yaq)

* Bucket名称：名称唯一，且创建后无法修改，该名称会作为Bucket域名的前缀，建议取得简洁优雅一点:yum:
* 区域：选择后无法修改，如果你有阿里云的服务器，最好和服务器处于同一区域，这样CDN回源会通过内网，不会产生计费。
* 存储类型：默认标准存储就行，低频存储（平均每月访问频率1到2次）和归档存储（最低存储时间（60天）和最小计量单位（64 KB）要求。数据需解冻（约1分钟）后访问）不太适合
* 读写权限：一般选择私有或公共读。因为我们的静态网站要供别人访问，这里选公共读。
* 其他都选择不开通好了，或者按需选择。我下面的截图是新建Bucket时的默认选项。

![image-20200720140637215](https://img.wuxd.top/img/image-20200720140637215.png)

### 二、托管静态网站

进入新建的Bucket后选择`文件管理-->上传文件`，然后在网页上传，注意index.html 要在Bucket根目录

![image-20200720143151385](https://img.wuxd.top/img/image-20200720143151385.png)

然后是`基础设置-->静态页面`，默认首页填入index.html，子目录首页默认不开通，想使用子目录首页可以自己研究下。

![image-20200720143500654](https://img.wuxd.top/img/image-20200720143500654.png)

引用一段官方文档的注意事项：

> - 出于安全考虑，中国区域自2018年8月13日起，中国以外区域自2019年9月25日起，通过浏览器访问OSS上网页类型文件（mimetype为text/html，扩展名包括HTM、HTML、JSP、PLG、HTX、STM）：
>
>   - 使用OSS默认域名访问时，Response Header中会自动加上` Content-Disposition:'attachment=filename;'`。即从浏览器访问网页类型文件时，不会显示文件内容，而是以附件形式进行下载。
>  - 使用绑定的自定义域名访问OSS时，Response Header中不会加上` Content-Disposition:'attachment=filename;'`，只要您的浏览器支持该类型文件的预览，可以直接预览文件内容。绑定自定义域名步骤请参见[绑定自定义域名](https://help.aliyun.com/document_detail/31902.html#concept-ozw-m2r-5fb)。
> 
> - 同一个Bucket中，版本控制和静态网站托管无法同时配置。若Bucket已开启版本控制功能，则无法再配置静态网站托管。版本控制功能详情请参见[版本控制介绍](https://help.aliyun.com/document_detail/109695.html#concept-jdg-4rx-bgb)。
>
> - 静态网站是指所有的网页都由静态内容构成，包括客户端执行的脚本，例如JavaScript。OSS不支持涉及到需要服务器端处理的内容，例如PHP、JSP、ASP.NET等。
>
> - 将一个Bucket设置成静态网站托管模式时：
>
>   - 若未开通子目录首页，需保证根目录下有默认首页和默认404页对应的文件，且文件可被匿名访问（即文件读写权限为公共读、公共读写或通过Bucket Policy设置了允许匿名访问）。
>  - 若开通子目录首页，需保证根目录和子目录下有默认首页文件、根目录下有默认404页文件，且这些文件可被匿名访问。
> 
>   **说明** 默认首页仅可以设置一个，若需要使用子目录首页，则子目录下的首页文件需要与根目录下的首页文件名称相同，但内容可以不同。
>
> - 将一个Bucket设置成静态网站托管模式后：
>
>   - 对静态网站域名的匿名访问，OSS将返回索引页面；对静态网站根域名的签名访问，OSS将返回GetBucket结果。
>  - 访问不存在的Object时，OSS会返回给用户设定的默认404页对应的文件，对此返回的流量和请求将会计费。

在第一条中由于阿里云政策变化，我们需要绑定一个已备案的域名才能访问静态网站首页。

打开`传输管理-->域名管理-->绑定域名`，填入自己的自定义域名，一级域名、二级域名都可以，下面的自动添加 CNAME 记录不勾选，因为我们最终要让自定义域名指向CDN的加速域名而不是这个OSS的Bucket 域名

![image-20200720145758341](https://img.wuxd.top/img/image-20200720145758341.png)

如果选择了自动添加 CNAME 记录的话，在填入自定义域名后，浏览器中输入自定义域名就能访问到我们的静态网站了。但是我们没有添加，接下来我们要引入CDN加速，实现网站的更快速访问。

### 三、CDN加速

`阿里云控制台-->CDN-->域名管理-->添加域名`

* 加速域名：刚刚OSS绑定的自定义域名
* 业务类型：图片小文件
* 源站信息：OSS域名，在下面的域名中下拉，选择刚刚的OSS域名
* 其余默认就好，或者按需选择

![image-20200720160652343](https://img.wuxd.top/img/image-20200720160652343.png)

之后就会如图所示，生成自定义域名对应的CDN加速的CNAME

![image-20200720161530558](https://img.wuxd.top/img/image-20200720161530558.png)

然后就是进行DNS解析了([官方文档--CDN解析](https://help.aliyun.com/document_detail/27144.html?spm=5176.11785003.0.0.e7e4142f0GISxC))：

1. 复制CNAME值
2. 进入`控制台-->云解析DNS-->域名解析-->解析设置`
3. 添加解析记录，记录类型为CNAME，主机记录为自定义域名，记录值为复制的CNAME值

然后就是等CDN域名管理页面的CNAME值和状态都变为绿色，就说明解析到DNS服务器了，此时就能很快地打开静态网站的网页了。

也可以在cmd中ping一下自定义域名来验证，如果看到实际上是ping的CNAME的值，也即CDN的加速服务器的域名，就说明CDN加速成功了。

### 四、原理解释

这里列一下出现的几个域名：

* 自定义域名：cuit.echoiot.com
* OSS的Bucket 域名：cuit-vue.oss-cn-chengdu.aliyuncs.com
* CDN加速域名：cuit.echoiot.com.w.kunlungr.com

首先我们在OSS的一个Bucket桶cuit-vue中绑定了自定义域名cuit.echoiot.com，如果我在域名解析中将自定义域名CNAME指向Bucket 域名，就能很方便地访问到OSS中的静态网站。这其中用户向cuit.echoiot.com发出请求，DNS系统将它解析到Bucket 域名，相当于是向Bucket 域名访问。这样设置比较方便直接，但是访问速度还不算太快。

于是我没有直接将自定义域名解析到OSS域名，而是引入CDN加速，在CDN域名添加阶段，加速域名填入了自定义域名，源站信息选择了OSS域名，在这里就把两个信息绑定到一起。完成之后生成了CDN加速域名也即我们刚刚复制的CNAME值，在DNS解析设置中将自定义域名指向CDN加速域名，就完成了设置。

![cdn](https://img.wuxd.top/img/cdn-1595237482674.png)

### 五、调整优化

1. `CDN-->域名管理——>回源配置——>回源HOST——>开启`
2. `CDN-->域名管理——>访问控制-->Referer防盗链`
3. `CDN-->域名管理——>性能优化`
4. 。。。未完成

## 阿里云CDN实现网站加速





## 阿里云OSS图片存储和使用