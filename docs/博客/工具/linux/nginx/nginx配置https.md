---
title: nginx配置https
date: 2020-09-22 14:23:11
permalink: /pages/eb6e02/
categories: 
  - 后端
  - nginx
tags: 
  - nginx
article: false
---

![nginx-https](https://img.wuxd.top/img/2020092214242016.png)

>  背景：目前[易控社区](http://cuit.echoiot.com)的前端部署方案是阿里云oss+cdn，各家cdn、oss服务也提供了一些防护方案，比如防盗链，黑/白名单，单 IP 单节点 QPS 限制等等，一圈看下来，想要攻击恶意刷流量还是有办法的，成本和技术高低的区别而已。目前我的博客是托管在coding.net的静态网站服务上的，也把社区前端迁移到coding.net上试一试吧，分担一些风险。不过目前coding也在升级静态网站服务到v2版，似乎使用的是腾讯云的cos+cdn，我的服务目前还没有别强制修改，但是学弟新注册的账号就直接跳到v2版的静态网站服务了，看还能白嫖多久吧，再后期可能是采用内网穿透方案或者体验一下腾讯云吧。

<!--more-->

好了话归正题，当我高高兴兴地尝试迁移到coding后，发现没法获取接口数据，如下：

![image-20200922144750164](https://img.wuxd.top/img/2020092214484153.png)

报错`This request has been blocked; the content must be served over HTTPS.`，看起来需要我把后端从http升级到https了。

开工。

## 申请SSL证书

我的域名是在阿里云买的，可以直接在阿里云控制台申请