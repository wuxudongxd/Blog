---
title: nginx
date: 2020-04-12 14:53:40
permalink: /pages/ecba04/
categories: 
  - nginx
tags: 
  - 
---


## 一、简介

nginx是一个高性能的web服务器和反向代理服务器

nginx的特点是占用内存少，并发处理能力强，

### 1、正向代理，反向代理

正向代理代理对象是客户端，反向代理代理对象是服务端

启动 Nginx 服务

systemctl start nginx



开机自启动

systemctl enable nginx



重启 Nginx 服务

systemctl restart nginx



查看 Nginx 服务状态

systemctl status nginx



重载 Nginx 服务

systemctl reload nginx



停止 Nginx 服务

systemctl stop nginx



