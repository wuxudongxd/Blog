---
title: docker数据卷迁移
date: 2020-09-12 14:55:56
permalink: /pages/ec0289/
categories: 
  - linux
  - docker
tags: 
  - linux
  - docker
---

```shell
# 备份数据卷
docker run --rm   --volumes-from code_db_1 -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar -C  /var/lib/mysql ./


# 在新服务器创建中间容器
docker create -v code_echo_web_db_vol:/var/lib/mysql:rw --name for_migrate busybox true


# 解压 tar 文件至 volume 对应目录
docker run --rm --volumes-from for_migrate -v $(pwd):/backup busybox tar xvf /backup/backup.tar -C /var/lib/mysql
```

