---
title: docker
date: 2020-04-12 14:53:40
permalink: /pages/8c76b2/
categories: 
  - linux
  - docker
tags: 
  - 
---

![docker](https://img.wuxd.top/img/ZQ6eFOB8hJvNR2f.png)



#### 4.6 查看容器

1. 查看容器详情`docker container inspect CONTAINER`
2. 查看容器内进程`docker top CONTAINER `
3. 查看统计信息`docker stats CONTAINER`

#### 4.7 其他容器命令

1. 复制文件

   将本地文件data复制到test容器的/tmp/路径下：

   `docker ps data test:/tmp/`

2. 查看变更`docker diff CONTAINER`

3. 查看端口映射`docker container port CONTAINER`

4. 更新配置`docker update CONTAINER`



### 五、访问docker仓库

#### 5.1 创建私有仓库

1. 使用官方registry镜像搭建`docker run -d -p 5000:5000 registry:2`

   默认目录`/var/lib/registry`



### 六、docker数据管理

docker的数据管理方式：

* 数据卷：容器内数据直接映射到本地主机环境

* 数据卷容器：使用特定容器维护数据卷

#### 6.1 数据卷

1. 创建数据卷`docker volume create -d local test`

   数据卷位置：`/var/lib/docker/volumes`

   查看详细信息：`docker volume inspect`

   列出数据卷：`docker volume ls`

   清理无用数据卷：`docker volume prune`

   删除数据卷：`docker volume rm`

2. 绑定数据卷

   在使用`docker run`命令的时候，可以使用`-mount`选项来使用数据卷

   `-mount`选项支持三种类型的数据卷，包括

   * volume：普通数据卷，映射到数据`/var/lib/docker/volumes`路径下
   * bind：绑定数据卷，映射到主机指定路径下
   * tmpfs：临时数据卷，只存在于内存中

#### 6.2 数据卷容器

1. 可以使用`--volumes-from`来挂载数据卷容器中的数据卷

#### 6.3 利用数据卷容器来迁移数据

1. 备份

   ```shell
   docker run --volumes-from dbdata -v $(pwd):/backup --name worker ubuntu tar cvPf /backup/backup.tar /dbdata
   ```

2. 恢复

   ```shell
   # 首先创建一个带有数据卷的容器dbdata2
   docker run -v /dbdata --name dbdata2 ubuntu /bin/bash
   # 然后创建另一个新的容器，挂载dbdata2的容器，并使用untar解压备份文件到所挂载的容器卷中
   docker run --volumes-from dbdata2 -v $(pwd):/backup busybox tar xvf /backup/backup.tar
   ```



### 七、端口映射和容器互联

