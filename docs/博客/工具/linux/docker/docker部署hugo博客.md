---
title: docker部署hugo博客
date: 2020-08-12 14:53:40
permalink: /pages/ae11f3/
categories: 
  - linux
  - docker
tags: 
  - 
---


![docker](https://img.wuxd.top/img/ZQ6eFOB8hJvNR2f.png)

我平时的博客都是使用Hugo将markdown生成网页的，在大概学习了docker之后我决定使用docker来部署我的博客。过程比较简单，主要是使用ngnix来访问静态文件，权当是做个记录，以供总结和回顾之用。

<!-- more -->

## 使用工具

服务器我使用的是一台阿里云学生机，通过ssh访问服务器。

软件：

> Visual Studio Code  1.47.0
>
> vs code 插件 Remote-SSH 0.51.0



## 搭建git私服

搭建git私服的目的是后续可以直接通过git将代码推送到服务器实现同步，如果你愿意，也可以直接通过vs code 将代码复制到服务器相应目录，跳过搭建这一步。

### 连接到服务器

可以在阿里云初始化服务器时选择密钥连接，也可以选择密码，然后手动将电脑的ssh密码上传到`/root/.ssh/authorized_keys`，之后就可以ssh登录了。这里只简略说一下，之后我写vs code的插件的用法的时候会详细再讲。

### 创建裸仓库

通常需要在服务器中创建一个裸仓库（中心仓库），这个仓库可以正常地clone和push

1. 选择任意目录，如我在根目录下创建`www/hugo`目录作为项目的目录

2. 创建代码目录和裸仓库

   ```shell
   # 创建存放代码的目录
   mkdir code
   
   # 创建裸仓库, --bare 用于创建裸仓库，仓库名为repo.git(.git后缀为约定俗成)
   git init --bare repo.git
   ```

   生成目录如下：

   ```shell
   [root@alicloud hugo]# tree repo.git/
   repo.git/
   ├── branches
   ├── config
   ├── description
   ├── HEAD
   ├── hooks
   │   ├── applypatch-msg.sample
   │   ├── commit-msg.sample
   │   ├── post-update.sample
   │   ├── pre-applypatch.sample
   │   ├── pre-commit.sample
   │   ├── prepare-commit-msg.sample
   │   ├── pre-push.sample
   │   ├── pre-rebase.sample
   │   └── update.sample
   ├── info
   │   └── exclude
   ├── objects
   │   ├── info
   │   └── pack
   └── refs
       ├── heads
       └── tags
   
   9 directories, 13 files
   ```

   

3. 添加裸仓库配置

   ```shell
   [root@alicloud hugo]# pwd
   /www/hugo
   [root@alicloud hugo]# ls
   code  repo.git
   [root@alicloud hugo]# cat > repo.git/hooks/post-receive << EOF
   > #!/bin/sh
   > git --work-tree=/www/hugo/code --git-dir=/www/hugo/repo.git checkout -f
   > EOF
   [root@alicloud hugo]# chmod +x repo.git/hooks/post-receive 
   ```

   主要是在`repo.git/hooks`中创建文件`post-receive`，并写入`git --work-tree=/www/hugo/code --git-dir=/www/hugo/repo.git checkout -f`。

   `--work-tree`为项目代码存放的位置，`--git-dir`为裸仓库的位置。

   最后记得加入可执行权限

   上面是演示了命令行添加，使用vs code也可以很方便地使用编辑器添加，方法多样，实现效果就行。

   

4. 本地项目连接裸仓库

   现在切换到本地终端，进入本地hugo目录

   ```powershell
   PS C:\Dropbox\article\Hugo> git init
   PS C:\Dropbox\article\Hugo> git remote add ali ssh://root@47.107.172.119:/www/hugo/repo.git
   PS C:\Dropbox\article\Hugo> git remote -v
   ali     ssh://root@47.107.172.119:/www/hugo/repo.git (fetch)
   ali     ssh://root@47.107.172.119:/www/hugo/repo.git (push)
   PS C:\Dropbox\article\Hugo> git add .
   PS C:\Dropbox\article\Hugo> git commit -m "init"
   PS C:\Dropbox\article\Hugo> git push ali master
   Enumerating objects: 468, done.
   Counting objects: 100% (468/468), done.
   Delta compression using up to 8 threads
   Compressing objects: 100% (366/366), done.
   Writing objects: 100% (468/468), 19.05 MiB | 3.53 MiB/s, done.
   Total 468 (delta 80), reused 0 (delta 0)
   To ssh://47.107.172.119:/www/hugo/repo.git
    * [new branch]      master -> master
   ```

顺利的话，服务器这边在`/www/hugo/code` 里就可以看到推送过来的代码了



## 编写docker-compose.yml

此时的项目目录结构如下：

```powershell
.
├── archetypes
├── config.toml
├── content
├── docker-compose.yml	# 新建docker-compose.yml
├── nginx				# 新建nginx目录及其子目录
    ├───conf
    │──────nginx.conf
    ├───log
    └───ssl
├── public
├── resources
└── themes
```



`docker-compose.yml`内容如下：

```yml
version: "3"

services:
  nginx:
    image: nginx:1.19.0
    ports:
      - "8005:8005"	# 映射8005端口，因为不想挤占服务器的80端口，随便选了个8005端口
    expose:
      - "8005"		# 暴露端口
    volumes:
      - ./:/www/hugo  #挂载hugo目录
      - ./nginx/conf:/etc/nginx/conf.d	# 挂载nginx配置
      - ./nginx/ssl:/usr/share/nginx/ssl # 挂载ssl证书目录
      - ./nginx/log:/var/log/nginx # 挂载日志      
    restart: always	# always表容器运行发生错误时一直重启
    tty: true		# 模拟一个假的远程控制台，作为前置命令开启常驻运行，防止容器自动退出
    stdin_open: true	# 打开标准输入，可以接受外部输入
```



## 编写nginx.conf

```nginx
server {
        listen       8005;
        root         /www/hugo/public;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }

```

编写完成以后将修改好的配置文件一并推送到服务器

## 启动服务

### 安装docker和docker-compose

我使用的是centos7，就以此为例。安装可参考[官方文档](https://docs.docker.com/engine/install/)

#### 一、卸载老版本

`Centos 7`默认是不会安装`Docker`的，但是如果安装了老版本的话可以使用下面的命令进行卸载。

```shell
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

#### 二、安装 Docker CE

更新`yum`包索引：

```shell
sudo yum update
```

安装一些必要的依赖包：

```shell
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
```

配置 docker-ce 仓库：

```shell
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

安装 docker-ce:

```shell
sudo yum install docker-ce # 安装过程中跳出确认则输入 y
```

启动docker并设置开机启动：

```shell
sudo systemctl start docker
sudo systemctl enable docker
```

查看docker版本：

```shell
docker --version
```

#### 三、安装docker-compose

```shell
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

docker-compose --version	# 查看版本
```

### 配置镜像加速器

dockerhub的镜像在国外拉取较慢，我们使用阿里提供的镜像加速服务

阿里云控制台——>容器镜像服务——>镜像加速器——>选择操作文档CentOS

针对Docker客户端版本大于 1.10.0 的用户

您可以通过修改daemon配置文件/etc/docker/daemon.json来使用加速器

```shell
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://XXXXXXXX.mirror.aliyuncs.com"]	# 这里的加速器地址需自行查看
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

### 放行端口

云服务器会提供两层防护，云服务器提供的安全组和服务器本身的防火墙都需要打开相应的端口才能进行访问

#### 一、阿里云安全组

阿里云——>服务器ECS——>实例——>本实例安全组——>入方向

| 协议类型  | 端口范围  | 授权对象  | 描述         |
| :-------- | :-------- | :-------- | :----------- |
| 自定义TCP | 8005/8005 | 0.0.0.0/0 | Hugo博客测试 |

#### 二、服务器防火墙

CentOS7使用的防火墙是firewalld，我们开启防火墙的8005端口

firewalld相关命令

```shell
# 防火墙的开启、关闭、禁用命令

systemctl start firewalld	# 启动防火墙

systemctl stop firewalld	# 关闭防火墙

systemctl enable firewalld.service	# 设置开机启用防火墙

systemctl disable firewalld.service	# 设置开机禁用防火墙

systemctl status firewalld 	# 检查防火墙状态

# 使用firewall-cmd配置端口

firewall-cmd --state	# 查看防火墙状态

firewall-cmd --reload	# 重新加载配置

firewall-cmd --list-ports	# 查看开放的端口

firewall-cmd --zone=public --add-port=8005/tcp --permanent	# 开启防火墙端口
命令含义:
　　–zone #作用域
　　–add-port=8005/tcp #添加端口，格式为：端口/通讯协议
　　–permanent #永久生效，没有此参数重启后失效
　　注意：添加端口后，必须用命令firewall-cmd --reload重新加载一遍才会生效

firewall-cmd --zone=public --remove-port=8005/tcp --permanent	# 关闭防火墙端口
```

端口放行、关闭示例：

```shell
[root@alicloud /]# systemctl start firewalld
[root@alicloud /]# firewall-cmd --state
running
[root@alicloud /]# firewall-cmd --list-ports
8005/tcp
[root@alicloud /]# firewall-cmd --zone=public --add-port=8006/tcp --permanent
success
[root@alicloud /]# firewall-cmd --reload
success
[root@alicloud /]# firewall-cmd --list-ports
8005/tcp 8006/tcp
[root@alicloud /]# firewall-cmd --zone=public --remove-port=8006/tcp --permanent
success
[root@alicloud /]# firewall-cmd --list-ports
8005/tcp 8006/tcp
[root@alicloud /]# firewall-cmd --reload
success
[root@alicloud /]# firewall-cmd --list-ports
8005/tcp
```

我将8005端口，防火墙都关闭，模拟初始情况：

```shell
[root@alicloud /]# firewall-cmd --state
not running
[root@alicloud /]# systemctl start firewalld
[root@alicloud /]# systemctl enable firewalld
Created symlink from /etc/systemd/system/dbus-org.fedoraproject.FirewallD1.service to /usr/lib/systemd/system/firewalld.service.
Created symlink from /etc/systemd/system/multi-user.target.wants/firewalld.service to /usr/lib/systemd/system/firewalld.service.
[root@alicloud /]# firewall-cmd --state
running
[root@alicloud /]# firewall-cmd --list-ports

[root@alicloud /]# firewall-cmd --zone=public --add-port=8005/tcp --permanent
success
[root@alicloud /]# firewall-cmd --reload
success
[root@alicloud /]# firewall-cmd --list-ports
8005/tcp
```

如此防火墙的8005端口也打开了



### 运行容器

```shell
[root@alicloud code]# cd /www/hugo/code/
[root@alicloud code]# ls
archetypes  config.toml  config.toml.even  content  docker-compose.yml  nginx  public  resources  themes
[root@alicloud code]# docker-compose up --build
Pulling nginx (nginx:1.19.0)...
Trying to pull repository docker.io/library/nginx ... 
1.19.0: Pulling from docker.io/library/nginx
8559a31e96f4: Pull complete
8d69e59170f7: Pull complete
3f9f1ec1d262: Pull complete
d1f5ff4f210d: Pull complete
1e22bfa8652e: Pull complete
Digest: sha256:21f32f6c08406306d822a0e6e8b7dc81f53f336570e852e25fbe1e3e3d0d0133
Status: Downloaded newer image for docker.io/nginx:1.19.0
Creating code_nginx_1 ... done
Attaching to code_nginx_1
nginx_1  | /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
nginx_1  | /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
nginx_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
nginx_1  | 10-listen-on-ipv6-by-default.sh: /etc/nginx/conf.d/default.conf is not a file or does not exist, exiting
nginx_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
nginx_1  | /docker-entrypoint.sh: Configuration complete; ready for start up
```

如果顺利，在服务器ip:8005 就能看到生成的博客了

## 配置域名

待新增