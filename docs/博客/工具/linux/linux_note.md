---
title: linux_note
date: 2020-08-12 14:53:40
permalink: /pages/e3f4e1/
categories: 
  - linux
tags: 
  - 
article: false
---


## 前言：

1. 本笔记基于B站 [【狂神说Java】Linux最通俗易懂的教程阿里云真实环境学习](https://www.bilibili.com/video/BV187411y7hF), 感谢大佬发布教程视频
2. 本笔记基于Ubuntu 20.04

![image-20200519140013063](https://img.wuxd.top/img/f3IagkTMbwOSDle.png)

<!-- more -->



==Linux ——  一切皆文件：文件就是读、写 （权限）==

## 学习方式：

1. 认识Linux
2. 基本的命令（文件操作、目录管理、文件属性、vim编辑器、账号管理、磁盘管理）
3. 软件的安装和部署（java、tomcat、docker）

## 入门阐述

> 为什么要学Linux

linux诞生了这么多年，以前还喊着如何能取代windows系统，现在这个口号已经小多了，任何事物发展 都有其局限性都有其天花板。就如同在国内再搞一个社交软件取代腾讯一样，想想而已基本不可能，因 为用户已经习惯于使用微信交流，不是说技术上实现不了解而是老百姓已经习惯了，想让他们不用，即 使他们自己不用亲戚朋友还是要用，没有办法的事情。

用习惯了windows操作系统，再让大家切换到别的操作系统基本上是不可能的事情，改变一个人已经养 成的习惯太难。没有办法深入到普通老百姓的生活中，并不意味着linux就没有用武之地了。在服务器 端，在开发领域linux倒是越来越受欢迎，很多程序员都觉得不懂点linux都觉得不好意思，linux在开源 社区的地位依然岿然不动。

尤其是作为一个后端程序员，是必须要掌握Linux的，因为这都成为了你找工作的基础门槛了，所以不得 不学习！

> Linux简介

Linux 内核最初只是由芬兰人林纳斯·托瓦兹（Linus Torvalds）在赫尔辛基大学上学时出于个人爱好而 编写的。

Linux 是一套免费使用和自由传播的类 Unix 操作系统，是一个基于 POSIX（可移植操作系统接口） 和 UNIX 的多用户、多任务、支持多线程和多 CPU 的操作系统。

Linux 能运行主要的 UNIX 工具软件、应用程序和网络协议。它支持 32 位和 64 位硬件。Linux 继承了 Unix 以网络为核心的设计思想，是一个性能稳定的多用户网络操作系统。

> Linux

Linux 的发行版说简单点就是将 Linux 内核与应用软件做一个打包。 

Kali linux：安全渗透测试使用！（有兴趣做安全的同学可以了解一下！）


目前市面上较知名的发行版有：Ubuntu、RedHat、CentOS、Debian、Fedora、SuSE、OpenSUSE、 Arch Linux、SolusOS 等。



今天各种场合都有使用各种 Linux 发行版，从嵌入式设备到超级计算机，并且在服务器领域确定了地 位，通常服务器使用 LAMP（Linux + Apache + MySQL + PHP）或 LNMP（Linux + Nginx+ MySQL + PHP）组合。

目前 Linux 不仅在家庭与企业中使用，并且在政府中也很受欢迎。

* 巴西联邦政府由于支持 Linux 而世界闻名。 
* 有新闻报道俄罗斯军队自己制造的 Linux 发布版的，做为 G.H.ost 项目已经取得成果。 
* 印度的 Kerala 联邦计划在向全联邦的高中推广使用 Linux。 
* 中华人民共和国为取得技术独立，在龙芯处理器中排他性地使用 Linux。 
* 在西班牙的一些地区开发了自己的 Linux 发布版，并且在政府与教育领域广泛使用，如 Extremadura 地区的 gnuLinEx 和 Andalusia 地区的 Guadalinex。 
* 葡萄牙同样使用自己的 Linux 发布版 Caixa Mágica，用于 Magalh?es 笔记本电脑和 e-escola 政府 软件。
* 法国和德国同样开始逐步采用 Linux。

> Linux vs Windows


## 走进Linux系统

> 开机登录

开机会启动许多程序。它们在Windows叫做"服务"（service），在Linux就叫做"守护进程"（daemon）。

开机成功后，它会显示一个文本登录界面，这个界面就是我们经常看到的登录界面，在这个登录界面中 会提示用户输入用户名，而用户输入的用户将作为参数传给login程序来验证用户的身份，密码是不显示 的，输完回车即可！
一般来说，用户的登录方式有三种：

* 命令行登录 
* ssh登录
* 图形界面登录 

最高权限账户为 root，可以操作一切！

> 关机

在linux领域内大多用在服务器上，很少遇到关机的操作。毕竟服务器上跑一个服务是永无止境的，除非 特殊情况下，不得已才会关机。

关机指令为：shutdown 

```
sync # 将数据由内存同步到硬盘中。 
shutdown # 关机指令，你可以man shutdown 来看一下帮助文档。例如你可以运行如下命令关机： 
shutdown –h 10 # 这个命令告诉大家，计算机将在10分钟后关机 
shutdown –h now # 立马关机 
shutdown –h 20:25 # 系统会在今天20:25关机 
shutdown –h +10 # 十分钟后关机 
shutdown –r now # 系统立马重启 
shutdown –r +10 # 系统十分钟后重启 
reboot # 就是重启，等同于 
shutdown –r now halt # 关闭系统，等同于
shutdown –h now 和 poweroff
```

最后总结一下，不管是重启系统还是关闭系统，首先要运行 sync 命令，把内存中的数据写到磁盘中。



> 目录系统结构

1. 一切皆文件
2. 根目录 / ，所有的文件都挂载在这个节点下

登录系统后，在当前命令窗口下输入命令：

```shell
ls /
```


```
bin  boot  data  dev  etc  home  lib  lib64  lost+found  
media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
```



以下是对这些目录的解释：

* /bin： bin是Binary的缩写, 这个目录存放着最经常使用的命令。 
* /boot： 这里存放的是启动Linux时使用的一些核心文件，包括一些连接文件以及镜像文件。（不 要动） 
* /dev ： dev是Device(设备)的缩写, 存放的是Linux的外部设备，在Linux中访问设备的方式和访问 文件的方式是相同的。 
* /etc： 这个目录用来存放所有的系统管理所需要的配置文件和子目录。 
* /home：用户的主目录，在Linux中，每个用户都有一个自己的目录，一般该目录名是以用户的账 号命名的。
*  /lib： 这个目录里存放着系统最基本的动态连接共享库，其作用类似于Windows里的DLL文件。 （不要动） 
* /lost+found： 这个目录一般情况下是空的，当系统非法关机后，这里就存放了一些文件。（存放 突然关机的一些文件） 
* /media：linux系统会自动识别一些设备，例如U盘、光驱等等，当识别后，linux会把识别的设备 挂载到这个目录下。 
* /mnt：系统提供该目录是为了让用户临时挂载别的文件系统的，我们可以将光驱挂载在/mnt/上， 然后进入该目录就可以查看光驱里的内容了。（我们后面会把一些本地文件挂载在这个目录下） 
* /opt：这是给主机额外安装软件所摆放的目录。比如你安装一个ORACLE数据库则就可以放到这个 目录下。默认是空的。 
* /proc： 这个目录是一个虚拟的目录，它是系统内存的映射，我们可以通过直接访问这个目录来获 取系统信息。（不用管） 
* /root：该目录为系统管理员，也称作超级权限者的用户主目录。 
* /sbin：s就是Super User的意思，这里存放的是系统管理员使用的系统管理程序。 
* /srv：该目录存放一些服务启动之后需要提取的数据。 
* /sys：这是linux2.6内核的一个很大的变化。该目录下安装了2.6内核中新出现的一个文件系统 sysfs 。 
* /tmp：这个目录是用来存放一些临时文件的。用完即丢的文件，可以放在这个目录下，安装包！
* /usr：这是一个非常重要的目录，用户的很多应用程序和文件都放在这个目录下，类似于windows 下的program files目录。 
* /usr/bin： 系统用户使用的应用程序。 
* /usr/sbin： 超级用户使用的比较高级的管理程序和系统守护程序。Super 
* /usr/src： 内核源代码默认的放置目录。 
* /var：这个目录中存放着在不断扩充着的东西，我们习惯将那些经常被修改的目录放在这个目录 下。包括各种日志文件。 
* /run：是一个临时文件系统，存储系统启动以来的信息。当系统重启时，这个目录下的文件应该被 删掉或清除。 
* /www：存放服务器网站相关的资源，环境，网站的项目



## 常用的基本命令

### 目录管理

> 绝对目录，相对目录

cd ： 切换目录命令！ 

./ ： 当前目录 

cd .. ： 返回上一级目录

> ls （列出目录）

在Linux中 ls 可能是最常常被使用的 ! 

-a参数：all ，查看全部的文件，包括隐藏文件 

-l 参数 列出所有的文件，包含文件的属性和权限，没有隐藏文件 

所有Linux可以组合使用！


> cd 命令 切换目录

cd 目录名

* 绝对路径都是以 / 开头

* 相对路径开头没有 /

```sh
root@dongdong-ubuntu:~# tree 	# 查看目录情况
.
└── test2
    └── test3

2 directories, 0 files
root@dongdong-ubuntu:~# cd test2		# 进入当前文件夹下目录
root@dongdong-ubuntu:~# cd ./test2
root@dongdong-ubuntu:~# cd test2/
root@dongdong-ubuntu:~# cd ./test2/

root@dongdong-ubuntu:~/test2# cd ..		# 返回上一级目录
root@dongdong-ubuntu:~/test2# cd ../
root@dongdong-ubuntu:~/test2# cd ./..
root@dongdong-ubuntu:~/test2# cd ./../

root@dongdong-ubuntu:~/test2# cd ~		# 回到当前的用户目录
root@dongdong-ubuntu:~# pwd
/root
```



> pwd 显示当前用户所在的目录

```shell
dongdong@dongdong-ubuntu:/$ pwd
/
dongdong@dongdong-ubuntu:/$ cd /bin
dongdong@dongdong-ubuntu:/bin$ pwd
/bin
dongdong@dongdong-ubuntu:/bin$ cd /usr/local
dongdong@dongdong-ubuntu:/usr/local$ pwd
/usr/local
```

> mkdir 创建一个目录

```shell
root@dongdong-ubuntu:~# mkdir test1
root@dongdong-ubuntu:~# ls
test1
root@dongdong-ubuntu:~# cd test1
root@dongdong-ubuntu:~/test1# cd ..
root@dongdong-ubuntu:~# mkdir test2/test3/test4
mkdir: 无法创建目录 “test2/test3/test4”: 没有那个文件或目录
root@dongdong-ubuntu:~# mkdir test2/test3/test4
root@dongdong-ubuntu:~# ls
test1  test2
root@dongdong-ubuntu:~# cd test2
root@dongdong-ubuntu:~/test2# ls
test3
root@dongdong-ubuntu:~/test2# cd test3
root@dongdong-ubuntu:~/test2/test3# ls
test4
```

> rmdir 删除目录

```shell
root@dongdong-ubuntu:~# ls
test1  test2
root@dongdong-ubuntu:~# rmdir test1
root@dongdong-ubuntu:~# ls
test2
root@dongdong-ubuntu:~# rmdir test2
rmdir: 删除 'test2' 失败: 目录非空
root@dongdong-ubuntu:~# rmdir -p test2
rmdir: 删除 'test2' 失败: 目录非空
root@dongdong-ubuntu:~# rmdir -p test2/test3/test4
root@dongdong-ubuntu:~# ls
root@dongdong-ubuntu:~# mkdir test2/test3/test4
mkdir: 无法创建目录 “test2/test3/test4”: 没有那个文件或目录
root@dongdong-ubuntu:~# mkdir -p test2/test3/test4
root@dongdong-ubuntu:~# ls
test2
root@dongdong-ubuntu:~# rmdir test2/test3/test4
root@dongdong-ubuntu:~# ls
test2
root@dongdong-ubuntu:~# cd test2/test3
root@dongdong-ubuntu:~/test2/test3# cd ../../
root@dongdong-ubuntu:~# cd ./test2/test3/
root@dongdong-ubuntu:~/test2/test3# 
```

rmdir 仅能删除空的目录，如果下面存在文件，需要先删除文件，递归删除多个目录 -p 参数即可

> cp (复制文件或者目录)

cp 原来的地方 新的地方

```shell
root@dongdong-ubuntu:~# tree
.
├── test1.sh
├── test2
│   └── test3
└── test2.sh

2 directories, 2 files
root@dongdong-ubuntu:~# cp test1.sh test2/test3
root@dongdong-ubuntu:~# tree
.
├── test1.sh
├── test2
│   └── test3
│       └── test1.sh
└── test2.sh

2 directories, 3 files
root@dongdong-ubuntu:~# cp test2.sh test2/test3/
root@dongdong-ubuntu:~# tree
.
├── test1.sh
├── test2
│   └── test3
│       ├── test1.sh
│       └── test2.sh
└── test2.sh

2 directories, 4 files
```

> rm （移除文件或者目录）

* -f 忽略不存在的文件，不会出现警告，强制删除！
* -r 递归删除目录！
* -i 互动，删除询问是否删除

`rm -rf / # 系统中所有的文件就被删除了，删库跑路就是这么操作的！`

```shell
root@dongdong-ubuntu:~# ls
test1.sh  test2  test2.sh
root@dongdong-ubuntu:~# rm test1.sh 
root@dongdong-ubuntu:~# ls
test2  test2.sh
```

> mv 移动文件或者目录！重命名文件

* -f 强制 
* -u 只替换已经更新过的文件

```shell
root@dongdong-ubuntu:~# tree
.
├── test2
│   └── test3
└── test2.sh

2 directories, 1 file
root@dongdong-ubuntu:~# mv test2.sh test2/test3/	#移动
root@dongdong-ubuntu:~# tree
.
└── test2
    └── test3
        └── test2.sh

2 directories, 1 file
root@dongdong-ubuntu:~# mv test2 test1			# 重命名
root@dongdong-ubuntu:~# tree
.
└── test1
    └── test3
        └── test2.sh

2 directories, 1 file
```

### 基本属性

Linux系统是一种典型的多用户系统，不同的用户处于不同的地位，拥有不同的权限。为了保护系统的安 全性，Linux系统对不同的用户访问同一文件（包括目录文件）的权限做了不同的规定。
在Linux中我们可以使用 ll 或者 ls –l 命令来显示一个文件的属性以及文件所属的用户和组， 如：

```shell
root@dongdong-ubuntu:/# pwd
/
root@dongdong-ubuntu:/# ls -l
总用量 1918444
lrwxrwxrwx   1 root root          7 5月  19 00:33 bin -> usr/bin
drwxr-xr-x   4 root root       4096 5月  27 12:17 boot
drwxrwxr-x   2 root root       4096 5月  19 00:35 cdrom
drwxr-xr-x   2 root root       4096 5月  19 23:43 data
drwxr-xr-x  18 root root       4140 5月  27 12:33 dev
drwxr-xr-x 131 root root      12288 5月  27 12:17 etc
drwxr-xr-x   3 root root       4096 5月  19 00:35 home
lrwxrwxrwx   1 root root          7 5月  19 00:33 lib -> usr/lib
lrwxrwxrwx   1 root root          9 5月  19 00:33 lib32 -> usr/lib32
lrwxrwxrwx   1 root root          9 5月  19 00:33 lib64 -> usr/lib64
lrwxrwxrwx   1 root root         10 5月  19 00:33 libx32 -> usr/libx32
drwx------   2 root root      16384 5月  19 00:33 lost+found
drwxr-xr-x   3 root root       4096 5月  19 00:44 media
drwxr-xr-x   3 root root       4096 5月  18 17:27 mnt
drwxr-xr-x   3 root root       4096 5月  19 15:04 opt
dr-xr-xr-x 305 root root          0 5月  27 12:33 proc
drwx------   4 root root       4096 5月  27 16:21 root
drwxr-xr-x  38 root root       1020 5月  27 16:07 run
lrwxrwxrwx   1 root root          8 5月  19 00:33 sbin -> usr/sbin
drwxr-xr-x   8 root root       4096 5月  19 00:42 snap
drwxr-xr-x   2 root root       4096 4月  23 15:32 srv
-rw-------   1 root root 1964400640 5月  19 00:33 swapfile
dr-xr-xr-x  13 root root          0 5月  27 12:33 sys
drwxrwxrwt  21 root root       4096 5月  27 16:07 tmp
drwxr-xr-x  14 root root       4096 4月  23 15:34 usr
drwxr-xr-x  14 root root       4096 4月  23 15:42 var
```

实例中，boot文件的第一个属性用"d"表示。"d"在Linux中代表该文件是一个目录文件。

在Linux中第一个字符代表这个文件是目录、文件或链接文件等等：

* 当为[ d ]则是目录 
* 当为[ - ]则是文件； 
* 若是[ l ]则表示为链接文档 ( link file )； 
* 若是[ b ]则表示为装置文件里面的可供储存的接口设备 ( 可随机存取装置 )； 
* 若是[ c ]则表示为装置文件里面的串行端口设备，例如键盘、鼠标 ( 一次性读取装置 )。

接下来的字符中，以三个为一组，且均为『rwx』 的三个参数的组合。

其中，[ r ]代表可读(read)、[ w ]代表可写(write)、[ x ]代表可执行(execute)。

要注意的是，这三个权限的位置不会改变，如果没有权限，就会出现减号[ - ]而已。

每个文件的属性由左边第一部分的10个字符来确定（如下图）：

| 文件类型 | 属主权限   | 属组权限   | 其他用户权限 |
| -------- | ---------- | ---------- | ------------ |
| 0        | 1 2 3      | 4 5 6      | 7 8 9        |
| d        | r w x      | r - x      | r - x        |
| 目录文件 | 读 写 执行 | 读 写 执行 | 读 写 执行   |

从左至右用0-9这些数字来表示。
第0位确定文件类型，第1-3位确定属主（该文件的所有者）拥有该文件的权限。第4-6位确定属组（所有 者的同组用户）拥有该文件的权限，第7-9位确定其他用户拥有该文件的权限。 

其中：

 第1、4、7位表示读权限，如果用"r"字符表示，则有读权限，如果用"-"字符表示，则没有读权限； 第2、5、8位表示写权限，如果用"w"字符表示，则有写权限，如果用"-"字符表示没有写权限；第3、6、9位表示可执行权限，如果用"x"字符表示，则有执行权限，如果用"-"字符表示，则没有执行权 限。

对于文件来说，它都有一个特定的所有者，也就是对该文件具有所有权的用户。 同时，在Linux系统中，用户是按组分类的，一个用户属于一个或多个组。 文件所有者以外的用户又可以分为文件所有者的同组用户和其他用户。 因此，Linux系统按文件所有者、文件所有者同组用户和其他用户来规定了不同的文件访问权限。 在以上实例中，boot 文件是一个目录文件，属主和属组都为 root。

> 修改文件属性

#### 1、chgrp：更改文件属组

`chgrp [-R] 属组名 文件名`

-R：递归更改文件属组，就是在更改某个目录文件的属组时，如果加上-R的参数，那么该目录下的所有 文件的属组都会更改。

#### 2、chown：更改文件属主，也可以同时更改文件属组

`chown [–R] 属主名 文件名 `

`chown [-R] 属主名：属组名 文件名`

#### 3、chmod：更改文件9个属性（必须要掌握）

`chmod [-R] xyz 文件或目录`

Linux文件属性有两种设置方法，一种是数字（常用的是数字），一种是符号。
Linux文件的基本权限就有九个，分别是owner/group/others三种身份各有自己的read/write/execute 权限。
先复习一下刚刚上面提到的数据：文件的权限字符为：『-rwxrwxrwx』， 这九个权限是三个三个一组 的！其中，我们可以使用数字来代表各个权限，各权限的分数对照表如下：

```
r:4		w:2		x:1

可读可写不可执行	rw		6
可读可写不课执行	rwx		7
chomd 777 文件赋予所有用户可读可执行！
```

每种身份(owner/group/others)各自的三个权限(r/w/x)分数是需要累加的，例如当权限为：[-rwxrwx---] 分数则是：

* owner = rwx = 4+2+1 = 7 
* group = rwx = 4+2+1 = 7 
* others= --- = 0+0+0 = 0

`chmod 770 filename`

```shell
root@dongdong-ubuntu:~# ls
test1
root@dongdong-ubuntu:~# chmod 700 test1
root@dongdong-ubuntu:~# ls -l
总用量 4
drwx------ 3 root root 4096 5月  27 13:38 test1
root@dongdong-ubuntu:~# chmod 761 test1/
root@dongdong-ubuntu:~# ls -l
总用量 4
drwxrw---x 3 root root 4096 5月  27 13:38 test1
```



### 文件内容查看

我们会经常使用到文件查看！ 

Linux系统中使用以下命令来查看文件的内容：

* cat 由第一行开始显示文件内容，用来读文章，或者读取配置文件啊，都使用cat
* tac 从最后一行开始显示，可以看出 tac 是 cat 的倒着写！
* nl 显示的时候，顺道输出行号！ 看代码的时候，希望显示行号！ 常用
* more 一页一页的显示文件内容，带余下内容的（空格代表翻页，enter 代表向下看一行， :f 行 号）
* less 与 more 类似，但是比 more 更好的是，他可以往前翻页！ （空格下翻页，pageDown， pageUp键代表翻动页面！退出 q 命令，查找字符串 /要查询的字符向下查询，向上查询使用？要 查询的字符串，n 继续搜寻下一个，N 上寻找！）
* head 只看头几行 通过 -n 参数来控制显示几行！
* tail 只看尾巴几行 -n 参数 要查看几行！

> 拓展：Linux 链接的概念（了解即可！）

Linux的链接分为两种：硬链接、软链接

**硬链接**：A---B，假设B是A的硬链接，那么他们两个指向了同一个文件！允许一个文件拥有多个路径，用 户可以通过这种机制建立硬链接到一些重要文件上，防止误删！ （相当于复制？）

**软链接**： 类似Window下的快捷方式，删除的源文件，快捷方式也访问不了！ 

* **ln** 创建链接
* **touch** 命令创建文件
* **echo** 输入字符串,也可以输入到文件中

```shell
root@dongdong-ubuntu:~# pwd
/root
root@dongdong-ubuntu:~# touch f1
root@dongdong-ubuntu:~# ls
f1  test1
root@dongdong-ubuntu:~# ln f1 f2
root@dongdong-ubuntu:~# ls
f1  f2  test1
root@dongdong-ubuntu:~# ln -s f1 f3
root@dongdong-ubuntu:~# ls
f1  f2  f3  test1
root@dongdong-ubuntu:~# ls -l
总用量 4
-rw-r--r-- 2 root root    0 5月  27 18:13 f1
-rw-r--r-- 2 root root    0 5月  27 18:13 f2
lrwxrwxrwx 1 root root    2 5月  27 18:18 f3 -> f1
drwxrw---x 3 root root 4096 5月  27 13:38 test1
root@dongdong-ubuntu:~# echo "i love linux" >> f1
root@dongdong-ubuntu:~# cat f1
i love linux
root@dongdong-ubuntu:~# cat f2
i love linux
root@dongdong-ubuntu:~# cat f3
i love linux
root@dongdong-ubuntu:~# rm -rf f1
root@dongdong-ubuntu:~# ls
f2  f3  test1
root@dongdong-ubuntu:~# cat f2
i love linux
root@dongdong-ubuntu:~# cat f3
cat: f3: 没有那个文件或目录
root@dongdong-ubuntu:~# 
```

### Vim 编辑器

> 什么是vim编辑器

vim 通过一些插件可以实现和IDE一样的功能！
Vim是从 vi 发展出来的一个文本编辑器。代码补完、编译及错误跳转等方便编程的功能特别丰富，在程 序员中被广泛使用。尤其是Linux中，必须要会使用Vim（查看内容，编辑内容，保存内容！） 

简单的来说， vi 是老式的字处理器，不过功能已经很齐全了，但是还是有可以进步的地方。 

vim 则可以说是程序开发者的一项很好用的工具。 

所有的 Unix Like 系统都会内建 vi 文书编辑器，其他的文书编辑器则不一定会存在。 

连 vim 的官方网站 (http://www.vim.org) 自己也说 vim 是一个程序开发工具而不是文字处理软件。 vim 键盘图：

![20190906144435.png](https://img.wuxd.top/img/peK7SLyX8kFxJ6u.png)



> 三种使用模式

基本上 vi/vim 共分为三种模式，分别是**命令模式（Command mode）**，**输入模式（Insert mode）**和 **底线命令模式（Last line mode）**。这三种模式的作用分别是：
