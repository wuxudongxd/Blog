---
title: Ubuntu-20.04
date: 2021-02-15 15:01:27
permalink: /pages/066080/
categories:
  - linux
tags:
  - 后端
  - linux
article: false
---
## 修改用户名密码

注：为了保证修改顺利完成，或者出错了也可以修改，请多开几个Terminal终端，并都切换到root账户下。

```bash
sudo su
```

**切记切换到root用户下在进行操作。**

**切记切换到root用户下进行操作。*

**切记切换到root用户下进行操作。**

### 1 修改主机名

修改主机名，也就是修改Terminal上，提示文字的@后面的对应的名字，如图所示的主机名是master，可以通过“hostname”命令查看主机名。

#### 1）修改 hostname文件

使用vim编辑器打开hostname文件，也可以使用gedit文本编辑器打开。

```bash
sudo vi /etc/hostname

sudo gedit /etc/hostname
```

![img](https://img-blog.csdnimg.cn/20200621155659621.png)

![img](https://img-blog.csdnimg.cn/20200621154559461.png)

 也可以通过hostnamectl 来修改主机名：

```bash
hostnamectl set-hostname master
```

#### 2）修改hosts文件

修改hosts文件

```bash
sudo vi /etc/hosts

sudo gedit /etc/hosts
```

![img](https://img-blog.csdnimg.cn/20200621155901827.png)

完成上面两个步骤后重启机器可以看到主机名已经为修改后的主机名 

![img](https://img-blog.csdnimg.cn/20200621160610213.png)

### 2 修改用户名

***\*切记切换到root用户下进行修改，普通用户下修改用户名后，执行sudo命令会提示密码错误。\****

***\*切记切换到root用户下进行修改，普通用户下修改用户名后，执行sudo命令会提示密码错误。\****

***\*切记切换到root用户下进行修改，普通用户下修改用户名后，执行sudo命令会提示密码错误。\****

第4）步中建议使用方法一：即修改用户组的方式，两种都执行也可以。

可以使用sed命令进行批量修改：（在被修改的str两边加上“\b”来限定字符界限，表示全部匹配才修改，以免修改错误。）

```bash
sed -i "s/\b<srcStr>\b/<desStr>/g" `grep <srcStr> -rl <filename>`
```

#### 1）修改passwd文件

将passwd中原用户名修改成新用户名：

```
vi /etc/passwd
```

![img](https://img-blog.csdnimg.cn/20200621163959375.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM0MTYwODQx,size_16,color_FFFFFF,t_70)

或者使用如下命令修改： 

```
sed -i "s/\bmaster\b/andy/g" `grep master -rl /etc/passwd` 
```

####  2）修改shadow文件

将shadow中原用户名修改成新用户名：

```
vi /etc/shadow
```

![img](https://img-blog.csdnimg.cn/20200621164041972.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM0MTYwODQx,size_16,color_FFFFFF,t_70)

或者用如下命令修改：

```
sed -i "s/\bmaster\b/andy/g" `grep master -rl /etc/shadow`
```

#### 3）修改home目录下文件夹名

将home目录下用户文件夹名修改为新用户的名：

```
mv /home/master/ /home/andy
```

![img](https://img-blog.csdnimg.cn/20200621164729756.png)

#### 4）修改sudo权限

建议使用方法1，即修改用户组，想两种都执行也可以。

##### 方法1：修改group用户组

修改group文件，将原来的用户名替换成新用户名。

```
vi /etc/group
```

![img](https://img-blog.csdnimg.cn/20200622080323922.png)

或者用如下命令修改：

```
sed -i "s/\bmaster\b/andy/g" `grep master -rl /etc/group`
```

 

##### 方法2：修改sudoers文件

将sudoers文件中原用户名替换成新用户名。如果没有，则可以直接添加新用户名。

```
Andy      ALL=(ALL:ALL) ALL             #用户andy需要输入密码执行sudo命令



%andy     ALL=(ALL) AL                  #用户组andy里的用户输入密码执行sudo命令



andy ALL=(ALL) NOPASSWD: ALL            #用户andy免密执行sudo命令



%andy ALL=(ALL) NOPASSWD: ALL           #用户组里的用户andy免密执行sudo命令
```

![wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==](data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==)

![img](https://img-blog.csdnimg.cn/20200622080613381.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM0MTYwODQx,size_16,color_FFFFFF,t_70)

![wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==](data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==)

###  

#### 5）重启机器

执行完上述步骤后重启机器，即可以新用户名登录。这里没有修改密码，密码是原用户的密码。

![img](https://img-blog.csdnimg.cn/20200621164650904.png)

![wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==](data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==)

### 3 修改用户密码

```bash
sudo passwd username          #修改用户密码

sudo passwd root              #修改root密码
```





## 安装docker

```bash
sudo apt update
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
```

把 docker 加入当前用户组，然后重启，用非 root 运行 docker。

```bash
sudo usermod -aG docker $USER
```

添加国内的镜像源。

```bash
sudo vim /etc/docker/daemon.json

// 加速源
{
    "registry-mirrors": ["https://docker.mirrors.ustc.edu.cn/"]
}

# 重启 docker服务
systemctl restart docker.service
# 自启动
systemctl enable docker.service
```





##  常用解压命令

### tar 命令

```shell
# 1. 生成 .tar 包
tar -cvf <dest.tar> <source_file>
# 2. 将tar包解压,-C表示指定解压目录，默认当前目录
tar -xvf <file.tar> [-C <dest_dir>]
# 3. 生成 .tar.gz 包
tar -czvf <dest.tar.gz> <source_file>
# 4. 将.tar.gz包解压，-C表示指定解压目录，默认当前目录
tar -xzvf <file.tar.gz> [-C <dest_dir>]
```



### zip 与 unzip

```shell
# 1. 压缩为 .zip 包, -r 表示可递归执行，用于压缩文件夹
zip -r <dest.zip> <source_file>
# 2. 解压缩，默认解压到当前目录，-d 可指定解压的目标目录
unzip <file.zip> [-d <dest_dir>]
# 3. 压缩 -P 表示加密码压缩
zip -r -P <password> <dest.zip> <source_file>
# 4. 使用密码解压缩 -P，如果不加 -P 去解压加密的文件时，会使用交互模式提醒用户输入密码
unzip -P <password> <file.zip> [-d <dest_dir>]
```



## 安装zsh和oh-my-zsh

apt-get install zsh

chsh -s $(which zsh)





参考：

1. https://blog.csdn.net/qq_34160841/article/details/106886306