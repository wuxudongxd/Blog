---
title: "git部分问题记录"
date: 2020-05-14T16:42:58+08:00
permalink: /pages/e3cdd4/
categories: 
  - 工具
tags: 
  - 工具
  - git
---

![git](https://img.wuxd.top/img/2color-lightbg@2x.png)

本文主要记录在使用git的过程中遇到的一些问题及解决方法，随遇随写，毕竟不能一口吃成个胖子:smile:

<!-- more -->

## git配置代理

在对GitHub克隆项目、上传代码时总是很慢，需要在本机配置一下使git可以走代理请求。

> 系统：Windows10
>
> 代理软件：[Clash for Windows](https://github.com/Fndroid/clash_for_windows_pkg/releases) ，既支持 **HTTP / HTTPS** 协议代理，也支持 **SOCKS v5** 协议代理
>
> 端口：默认HTTP 代理端口**7890**，SOCKS5 代理端口**7891**

tip1：以下内容皆以上述环境为基础，根据我的具体情况进行总结，Linux或macOS会有些许差异。

tip2：由于本地仓库可能使用https也可能使用ssh，所以下面两种协议最好都配置上。



### SSH协议走代理：

ssh协议是我默认使用的协议，它可以通过本机生成的密钥进行验证，不需要输入密码，比较方便。

在GitHub的**settings —> SSH and GPG keys** 中添加本机生成的ssh公钥 `用户名/.ssh/id_rsa.pub` ，这样向自己的GitHub仓库发出请求的时候，公私钥进行验证即可完成认证。

ssh协议走代理主要是通过配置`用户名/.ssh/config`这个文件，默认应该是没有这个文件的，先新建一个，然后加入如下内容：

```
Host github.com
    User git
    ProxyCommand connect -H 127.0.0.1:7890 %h %p
```

或

```
Host github.com
    User git
    ProxyCommand connect -S 127.0.0.1:7891 %h %p
```

> 简单说明：
>
> 1. 这个是针对GitHub的
> 2. 代理软件的**代理协议**和本地**端口号**要对应，-H是http协议，对应7890端口；-S是socks协议，对应7891端口

### HTTPS协议走代理：

在终端中执行

```
git config --global http.https://github.com.proxy http://127.0.0.1:7890
```

或

```
git config --global http.https://github.com.proxy socks5://127.0.0.1:7891
```

可以配置git的http走http代理或者socks5代理

上面的命令相当于在`~/.gitconfig`文件中添加了

```
[http "https://github.com"]
	proxy = http://127.0.0.1:7890
```

或

```
[http "https://github.com"]
	proxy = socks5://127.0.0.1:7891
```

## HTTPS协议添加密码凭证：

使用 HTTPS 协议后 push 时需要密码，但是现在操作系统或者其他git工具都提供了 `keychain` 的功能，可以把你的账户密码记录在系统里，例如OSX 的 Keychain 或者 Windows 的凭证管理器。如果仍然需要输入密码，可以考虑使用git的凭据管理方式。

```
# 建立凭据文件
$ touch ~/.git-credentials
$ vim ~/.git-credentials
```

在文件中加入带凭据的url信息:

```
https://{username}:{passwd}@github.com
```

然后告诉git使用这个凭据管理器:

```
$ git config --global credential.helper store
```

上面命令会在git配置文件 `～/.gitconfig` 中设置上一个凭据地址:

```
[credential]
    helper = store
```

## 	参考文章：

1. [一文让你了解如何为 Git 设置代理](https://ericclose.github.io/git-proxy-config.html)，作者[Eric](https://ericclose.github.io/about/)
2. [论git中使用https和ssh协议的区别](https://blog.cuiyongjian.com/engineering/git-https-ssh/)，作者[sheldoncui](https://blog.cuiyongjian.com/about/)

