---
title: vs和vsc对gbk编码的处理
date: 2020-06-23 09:57:47
permalink: /pages/2866af/
categories:
  - 博客
  - 工具
tags:
  - 
---
前提：系统默认编码为utf8，这个主要影响vs

对于同一个gbk编码问题，vsc或者jetbrains家的软件可以很好的处理，vsc先可以设法使用正确的编码读取，然后可以正确地转换到其他编码。

vs则在打开gbk先进行Unicode编码替换（在确认打开后未进行人为修改的情况下是修改但未保存的状态），然后在高级保存选项里可以选择保存为utf8无签名（无bom）或者gbk2312编码，选择为utf8无签名，然后保存，可以强制将文件保存为utf8格式，但是文件乱码；选择gbk2312（即文件原本的格式），会提示无法保存。 vs似乎没法很好地处理编码转换问题，它不能修改打开文件的编码，先默认使用utf8，然后使用平台的默认编码尝试进行识别（Windows大陆中文是gbk2312，我手动改成了utf8）。没法识别就显示乱码，在开启高级保存选项后可以手动选择保存编码，不过只是保存，在没法正确识别编码的情况下强制保存，最后仍然是乱码。总之，打开其他编码文件时就默认进行unicode字符替换+没法选择打开文件时的编码+在未正确识别的情况下强制保存，导致文件编码越来越乱。vs在这方面我觉得很智障。

目前用vs开发的问题就是系统默认编码为gbk会导致新建项目为gbk格式，系统默认编码为utf8会导致vs不能很好地识别gbk文件。



对于纯英文文本，由于gbk和utf8无bom在英文字符上都和基础ASCII码一致，所以保存的二进制数据是一样的，所以一份英文文本保存为gbk还是utf8，数据都是一样的，使用gbk或者utf8都可以正确读取，没有任何问题，因为数据是一模一样的，使用gbk还是utf8都是保存成一样的数据，这份文件保存后并没有任何标记来说明是采用的gbk还是utf8，都是与ACSII编码一致。如果有中文或者其他字符，gbk和utf8采用的编码方式不同，二进制数据也出现了差异，使用与保存方式不同的编码读取就会出现乱码。





```shell
root@dongdong-ubuntu:/home/dongdong/下载# echo -n "中文" > foo.utf8
root@dongdong-ubuntu:/home/dongdong/下载# od -t x1 foo.utf8
0000000 e4 b8 ad e6 96 87
0000006
root@dongdong-ubuntu:/home/dongdong/下载# iconv -f utf-8 -t utf16 foo.utf8 > foo.utf16
iconv: 不支持转换到 utf16
iconv: 尝试用‘iconv -l’获取所支持的编码列表
root@dongdong-ubuntu:/home/dongdong/下载# iconv -f utf-8 -t utf-16 foo.utf8 > foo.utf16
root@dongdong-ubuntu:/home/dongdong/下载# od -t x1 foo.utf16
0000000 fe ff 4e 2d 65 87
0000006
root@dongdong-ubuntu:/home/dongdong/下载# iconv -f utf-16 -t utf-32 foo.utf16 > foo.utf32
root@dongdong-ubuntu:/home/dongdong/下载# od -t x1 foo.utf32
0000000 00 00 fe ff 00 00 4e 2d 00 00 65 87
0000014
root@dongdong-ubuntu:/home/dongdong/下载# iconv -f utf-8 -t gb2312 foo.txt > foo.gb2312
iconv: foo.txt: 没有那个文件或目录
root@dongdong-ubuntu:/home/dongdong/下载# iconv -f utf-8 -t gb2312 foo.utf8 > foo.gb2312
root@dongdong-ubuntu:/home/dongdong/下载# od -t x1 foo.gb2312
0000000 d6 d0 ce c4
0000004
root@dongdong-ubuntu:/home/dongdong/下载# iconv -f utf-8 -t gbk foo.txt > foo.gbk
iconv: foo.txt: 没有那个文件或目录
root@dongdong-ubuntu:/home/dongdong/下载# iconv -f utf-8 -t gbk foo.utf8 > foo.gbk
root@dongdong-ubuntu:/home/dongdong/下载# od -t x1 foo.gbk
0000000 d6 d0 ce c4
0000004
root@dongdong-ubuntu:/home/dongdong/下载# iconv -f utf-8 -t gb18030 foo.utf8 > foo.gb18030
root@dongdong-ubuntu:/home/dongdong/下载# od -t x1 foo.gb18030
0000000 d6 d0 ce c4
0000004
root@dongdong-ubuntu:/home/dongdong/下载# 

```

```shell
dongdong@dongdong-ubuntu:~/下载$ echo -n  'a' > foo.utf8
dongdong@dongdong-ubuntu:~/下载$ od -t x1 foo.utf8
0000000 61
0000001
dongdong@dongdong-ubuntu:~/下载$ iconv -f utf-8 -t utf-16 foo.utf8 > foo.utf16
dongdong@dongdong-ubuntu:~/下载$ od -t x1 foo.utf16
0000000 fe ff 00 61
0000004
dongdong@dongdong-ubuntu:~/下载$ iconv -f utf-16 -t utf-32 foo.utf16 > foo.utf32
dongdong@dongdong-ubuntu:~/下载$ od -t x1 foo.utf32
0000000 00 00 fe ff 00 00 00 61
0000010
dongdong@dongdong-ubuntu:~/下载$ iconv -f utf-8 -t gb2312 foo.utf8 > foo.gb2312
dongdong@dongdong-ubuntu:~/下载$ od -t x1 foo.gb2312
0000000 61
0000001
dongdong@dongdong-ubuntu:~/下载$ iconv -f utf-8 -t gbk foo.utf8 > foo.gbk
dongdong@dongdong-ubuntu:~/下载$ od -t x1 foo.gbk
0000000 61
0000001
dongdong@dongdong-ubuntu:~/下载$ iconv -f utf-8 -t gb18030 foo.utf8 > foo.gb18030
dongdong@dongdong-ubuntu:~/下载$ od -t x1 foo.gb18030
0000000 61
0000001

```



```shell
dongdong@dongdong-ubuntu:~/test$ echo -n  'ab' > foo.utf8
dongdong@dongdong-ubuntu:~/test$ od -t x1 foo.utf8
0000000 61 62
0000002
dongdong@dongdong-ubuntu:~/test$ iconv -f utf-8 -t utf-16 foo.utf8 > foo.utf16
dongdong@dongdong-ubuntu:~/test$ od -t x1 foo.utf16
0000000 fe ff 00 61 00 62
0000006
dongdong@dongdong-ubuntu:~/test$ iconv -f utf-16 -t utf-32 foo.utf16 > foo.utf32
dongdong@dongdong-ubuntu:~/test$ od -t x1 foo.utf32
0000000 00 00 fe ff 00 00 00 61 00 00 00 62
0000014
dongdong@dongdong-ubuntu:~/test$ iconv -f utf-8 -t gb2312 foo.utf8 > foo.gb2312
dongdong@dongdong-ubuntu:~/test$ od -t x1 foo.gb2312
0000000 61 62
0000002
dongdong@dongdong-ubuntu:~/test$ iconv -f utf-8 -t gbk foo.utf8 > foo.gbk
dongdong@dongdong-ubuntu:~/test$  od -t x1 foo.gbk
0000000 61 62
0000002
dongdong@dongdong-ubuntu:~/test$ iconv -f utf-8 -t gb18030 foo.utf8 > foo.gb18030
dongdong@dongdong-ubuntu:~/test$ od -t x1 foo.gb18030
0000000 61 62
0000002

```



```shell
dongdong@dongdong-ubuntu:~/下载/test$ od -t x1 a.txt 
0000000 61
0000001
dongdong@dongdong-ubuntu:~/下载/test$ od -t x1 a_withbom.txt 
0000000 ef bb bf 61
0000004

```

