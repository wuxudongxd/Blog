---
title: wsl
date: 2021-02-13 16:29:52
permalink: /pages/83caf4/
categories:
  - 工具
tags:
  - 工具
  - wsl
---
本文为WSL2的Ubuntu-20.04 配置记录，如果条件允许之后会写一个初始化脚本

wsl2安装后在Windows terminal 可以直接打开，在vscode中安装remote-wsl也可以在vscode中直接打开，不需要通过ssh连接



安装后初始化配置：

1. 创建root用户

   ```bash
   sudo passwd root
   ```

2. 换源

   1. 备份原软件源

      ```bash
      sudo cp /etc/apt/sources.list /etc/apt/sources.list.backup
      ```

   2. 修改软件源

      ```bash
      sudo vim /etc/apt/sources.list
      ```

      打开vim后按gg跳转到文本开头，按dG删除全文，ctrl+v通过终端粘贴到文本中

      ```bash
      deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
      deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
      
      deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
      deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
      
      deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
      deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
      
      deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
      deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
      
      deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
      deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
      ```

   3. 更新源

      ```bash
      sudo apt-get update
      sudo apt-get upgrade
      ```

3. 设置wsl2走clash代理

   1. 在当前用户的 `.bashrc`文件末尾加入

      ```
      export WHOST=`cat /etc/resolv.conf|grep nameserver|awk '{print $2}'` # 设置一个环境变量,通过在终端中输入 echo $WHOST 即可输出 Windows 的 ip 地址
      export http_proxy=http://`cat /etc/resolv.conf|grep nameserver|awk '{print $2}'`:7890 # 配置 http 代理
      export https_proxy=https://`cat /etc/resolv.conf|grep nameserver|awk '{print $2}'`:7890 # 配置 https 代理
      alias ALL_PROXY="export ALL_PROXY=socks5://$WHOST:7890" # 配置一个别名来临时开启所有代理,http,https以外的
      alias NONE_PROXY="unset ALL_PROXY"  # 别名取消所有的代理
      ```

   2. 配置完成后通过 `curl -v www.google.com` 来检验,如果输出如下,则说明开启成功

      ```
      Uses proxy env variable http_proxy == 'xxx'
      *   Trying xxx...
      * TCP_NODELAY set
      ```

4. 通过genie来使用systemctl

   1. genie基于.net core 3构建，所以要先安装.net core运行时环境。参考[微软的文档](https://docs.microsoft.com/zh-cn/dotnet/core/install/linux-ubuntu)，可以直接安装deb包来添加.net core的安装源

      ```bash
      wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
      sudo dpkg -i packages-microsoft-prod.deb
      
      ```

   2. 然后，genie的二进制包通过packagecloud.io分发，通过以下命令配置&安装：

      ```bash
      curl -s https://packagecloud.io/install/repositories/arkane-systems/wsl-translinux/script.deb.sh | sudo bash
      sudo apt install systemd-genie
      
      ```

   3. 这样genie就安装完毕了。genie的使用方法也相对简单：

      ```bash
      # 启动systemd环境
      genie -i
      # 启动systemd环境，并在环境中打开shell
      genie -s
      # 启动systemd环境，并在环境中运行命令
      genie -c command
      ```

   4. 所以systemctl可以通过两种方式使用，一种：

      ```bash
      genie -s
      # 这时应该启动了新的shell，shell的host后面会带上-wsl尾缀
      # 此时可以正常使用systemctl等命令
      systemctl start xxxx
      # 完事之后可以返回原shell
      exit
      ```

      另一种：

      ```bash
      genie -c systemctl start xxxx
      ```

   5. 配置脚本

      ```bash
      #!/bin/bash
      # 从apt源安装并使用docker.io，同时配置genie
      if [ "$EUID" -ne 0 ]
        then echo "Please run as root"
        exit
      fi
      wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
      dpkg -i packages-microsoft-prod.deb
      curl -s https://packagecloud.io/install/repositories/arkane-systems/wsl-translinux/script.deb.sh | sudo bash
      apt install systemd-genie docker.io
      # 避免可能的竞争条件，https://github.com/arkane-systems/genie/issues/70
      genie -i
      sleep 0.05
      # 按照推荐，因为不使用，所以禁用getty@tty1
      genie -c systemctl disable getty@tty1
      # 启用并启动docker
      genie -c systemctl enable docker
      genie -c systemctl start docker
      # 以下是配置不需要root权限也可以使用docker命令，不需要的话可以去掉
      groupadd docker
      gpasswd -a $SUDO_USER docker
      newgrp docker
      # 后面每次重启WSL2之后，只需要genie -i即可
      ```

   6. 如果需要启动WSL2的时候就启用genie，可以把`genie -i`命令加入.bashrc。

      如果需要卸载，可以使用：

      ```bash
      sudo apt purge systemd-genie apt-transport-https packages-microsoft-prod
      sudo rm /etc/apt/sources.list.d/arkane-systems_wsl-translinux.list
      # 别忘了使用apt update更新apt缓存
      ```

      





参考：

1. https://joyqat.top/posts/wsl2-systemctl/



