---
title: VScode remote-ssh配置记录
date: 2020-06-01 08:04:57
permalink: /pages/9372b1/
categories: 
  - 工具
tags: 
  - 工具
article: false
---

1. 远程服务器安装密钥，详见Windows Terminal那次笔记

2. vscode搜索安装remote-ssh安装，目前是0.51.0版本

3. ctrl+shift+p打开功能选择，输入remote-ssh选择 open configuraion file

4. 选择C:\Users\DongDong\\.ssh\config，添加字段如下

   ```
   Host alicloud
       HostName 47.107.172.119
       User root
   
   Host huaweicloud
       HostName 116.63.172.108
       User root
   ```

5. ctrl+shift+p打开功能选择，输入remote-ssh选择 connect to Host 连接远程服务器

6. 弹出提示选择系统，我选择linux

7. 网络顺利的话会成功连接