---
title: Vscode_setting_sync
date: 2020-06-01 08:22:00
permalink: /pages/ba86c3/
categories: 
  - 工具
tags: 
  - 工具
article: false
---

Setting Sync（3.4.3）目前可以很简单地通过GitHub账号的登录来完成同步，网上有一些前期版本的教程已经过时了，不需要手动去获取token和gist了

1. vscode搜索安装Setting Sync 
2. 选择LOGIN WITH GITHUB，并进行登录
3.  Upload Key : Shift + Alt + U
    Download Key : Shift + Alt + D
4. 同步的话在另一台设备上同样安装vscode和setting sync，LOGIN WITH GITHUB，登录成功的话会弹出gist选择，选最近时间的就行
5. Setting Sync同步受网络情况影响较大，不行就换个网络多试几次