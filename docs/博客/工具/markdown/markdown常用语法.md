---
title: markdown常用语法
date: 2020-04-12 00:07:47
permalink: /pages/743e48/
categories: 
  - 工具
tags:
  - 工具
  - markdown
---


# 这是一级标题

## 这是二级标题

### 这是三级标题

#### 这是四级标题

##### 这是五级标题

##### 这是六级标题

*这会是 斜体 的文字*
_这会是 斜体 的文字_

**这会是 粗体 的文字**
__这会是 粗体 的文字__

_你也 **组合** 这些符号_

~~这个文字将会被横线删除~~

* Item 1
* Item 2
  * Item 2a
  * Item 2b
    * item 3a
    * item 4a
      * item 5a
      * item 6a
        * Item 3

1. Item 1
2. Item 2
3. Item 3
    1. Item 3a
    2. Item 3b

空行的输入
&nbsp;


[GitHub](http://github.com)

正如 Kanye West 所说：

> We're living the future so
> the present is our past.

如下，三个或者更多的

---

连字符

***

星号

___

下划线

我觉得你应该在这里使用
`<addr>` 才对。

```c {.line-numbers}
# include<stdio.h>

int main(void)
{

}
```

- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item

| a | b |
|---|---|
|  | 2 |
| 3 ||

:smile:

30^th^

H~2~O

Content [^1]

[^1]: Hi! This is a footnote

The HTML specification
is maintained by the W3C.
*[HTML]: Hyper Text Markup Language
*[W3C]:  World Wide Web Consortium

==marked==

$f = sin(x)$
