---
title: "常用工具的uft8编码设置"
date: 2020-06-17T23:52:37+08:00
permalink: /pages/d01f81/
categories: 
  - 工具
tags: 
  - 工具
---

![uft8](https://img.wuxd.top/img/uft8.png)

目前我代码默认使用的编码格式是ufr8 without BOM ，以下是对Windows10、VS code、Visual Studio的一些设置，使它们都是ufr8 without BOM的状态，保持代码更好的跨平台特性和通用性。

<!-- more -->

&nbsp;

## Windows10

>  版本：Windows 10 教育版
>
> 版本号：1909

步骤：设置——时间和语言——语言——管理语言设置——更改系统区域设置——勾选 Beta版：使用 Unicode UIT-8 提供全球语言支持

![image-20200618004211591](https://img.wuxd.top/img/image-20200618004211591.png)

此选项是beat版，开启后可能有部分应用会乱码，如网易有道词典，鲁大师等，需权衡后开启。

&nbsp;
&nbsp;

## Visual Studio

> 版本：Community 2019
>
> 版本号：16.6.2
>
> 注：如果系统为utf8，visual studio基本不需要设置就是utf8编码了。如果系统为gbk编码，可参考下面的设置（自我体验，Force UTF-8 (No BOM) 插件并不好用，需要修改才能重新保存为utf8，默认新建未进行修改的文件仍为gbk）

步骤：拓展——管理拓展——搜索 utf —— 安装Force UTF-8 (No BOM) 插件 ——重启Visual Studio

ps1：此插件可以将所有文件强制转换为utf8 without BOM

ps2：如果系统编码为GBK，visual studio可能会报错 `warning C4819: 该文件包含不能在当前代码页(936)中表示的字符。请将该文件保存为 Unicode 格式以防止数据丢失` ，这时可以把所有文件转成utf8带BOM的，或者系统转为utf8格式，也可以直接屏蔽掉警告（项目=>属性=>配置属性=>C/C++=>高级=>禁用特定警告=>4819（添加数字应用确定即可））

![image-20200618002904519](https://img.wuxd.top/img/image-20200618002904519.png)

### 中文乱码问题：

如果控制台出现中文乱码，代码格式保存为uft8，控制台仍显示gbk格式，如下图，

![image-20200619115501615](https://img.wuxd.top/img/image-20200619115501615.png)



可尝试修改注册表，我的注册表路径为

```
计算机\HKEY_CURRENT_USER\Console\C:_Program Files (x86)_Microsoft Visual Studio_2019_Community_Common7_IDE_CommonExtensions_Platform_Debugger_VsDebugConsole.exe
```

然后修改如下

![image-20200619115928159](https://img.wuxd.top/img/image-20200619115928159.png)

&nbsp;

&nbsp;



## Visual Studio Code

> 版本：1.46.0

File(文件)——Preferences(首选项)——Usersettings(设置)——搜索 encod——设置为utf8

![image-20200618002832566](https://img.wuxd.top/img/image-20200618002832566.png)