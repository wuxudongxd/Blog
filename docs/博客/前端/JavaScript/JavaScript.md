---
title: JavaScript
date: 2021-01-31 23:22:22
permalink: /pages/9eed97/
categories:
  - JavaScript
tags:
  - JavaScript
---

## let和const

let命令：用来声明一个变量，和var非常相似

const命令：用来声明一个常量，常量就是不可以变化的量

### let声明注意事项

1. 用let声明的变量，所声明的变量只在命令所在的代码块内有效

   ```javascript
   {
       let a = 1;
       var b = 2;    
   	console.log(a);	// 1
   	console.log(b);	// 2    
   }
   console.log(a);	// ReferenceError
   console.log(b);	// 2
   ```

2. 使用let声明的变量在域解析的时候不会被提升

   ```javascript
   console.log(a);	// undefined
   var a = 1;
   
   console.log(b);	// ReferenceError
   let b = 2;
   
   typeof c;	// ReferenceError
   let c = 3;
   
   /* 
   如果区块中存在let和const命令，只要一进入当前作用域，使用let和const声明的变量就已经存在了，但是变量不可获取。
   只有等到声明变量的那一行代码出现，才可以获取和使用该变量，在声明之前就使用这些变量就会报错。
   这在语法上，称为“暂时性死区”（temporal dead zone，简称 TDZ）。
   */
   let f = 10;
   function fn(){
       f = 7;	// 暂时性死区
       let f = 2;
   }
   fn();	// ReferenceError
   ```

3. let不允许在同一个作用域下声明已经存在的变量

   ```javascript
   var a = 1;
   let a;	// SyntaxError
   
   let b = 1;
   let b = 2;	// SyntaxError
   
   // 在循环语句内是一个父作用域，在循环体之中是一个子作用域
   for (let i = 0; i < 3; i++){
       let i = 10;
       console.log(i);
   }	// 10 10 10
   console.log(i);	// ReferenceError
   
   // 上边的循环语句等价于
   {
       let i = 0;
       if (i < 3){	// i = 0;
           let i = 10;
           console.log(i);	// 10
       }
       i++;	// i = 1
       if (i < 3){	// i = 1;
           let i = 10;
           console.log(i);	// 10
       }
       i++;	// i = 2
       if (i < 3){	// i = 2;
           let i = 10;
           console.log(i);	// 10
       }
       i++;	// i = 3 不满足<3，退出
   }
   console.log(i);	// ReferenceError
   ```

### const声明注意事项

const命令同样具有上面let的1，2，3条特点。第一，所声明的变量通常只在所在的代码块内有效；第二，声明的变量通常不会被提升；第三，不能声明已经被声明的常量或者变量。除了这些，使用const声明常量的时候还需要注意两点：

1. 声明的时候必须赋值

   ```javascript
   const a;	// SyntaxError
   ```

2. 声明的常量存储基本数据类型的时候不可改变其值；如果存储的是对象，那么引用不可改变，对象内的数据可以改变

   ```javascript
   const a = 1;
   a = "a";	// SyntaxError
   
   const obj = {a: 10};
   obj.a = 20;
   console.log(obj);	// Object {a: 20}
   ```

   

## 变量的解构赋值

基本概念：本质是一种匹配模式，只要等号两边的模式相同，那么左边的变量就可以被赋予相应的值

解构赋值主要分为：

1. 数组的解构赋值（常用）
2. 对象的解构赋值（常用）
3. 基本类型的解构赋值（了解）

### 数组的解构赋值

```javascript
let [a, b, c] = [1, 2, 3];
console.log(a, b, c);	// 1 2 3

let [a, [b, [c]]] = [1, [2, [3]]];
console.log(a, b, c);	// 1 2 3 

let [, , c] = [1, 2, 3];
console.log(c);	// 3

let [x] = [];	// 解构不成功，相当于声明但没有赋值
console.log(x);	// undefined

let [y = 1] = [];	// 使用默认值
console.log(y);	// 1
```

### 对象的解构赋值

```javascript
let {a, b} = {b: 'bbb', a: 'aaa'};
console.log(a, b);	// aaa bbb

let {a: b} = {a: 1};
console.log(b);	// 1
console.log(a);	// ReferenceError
```

### 基本类型的解构赋值

```javascript
let [a, b, c, d] = '1234';
console.log(a, b, c, d);	// 1 2 3 4

let {length: len} = 'javascript';
console.log(let);	// 10

let {toString: ns} = 1;
let {toString: bs} = true;
console.log(ns === Number.prototype.toString);	// true
console.log(bs === Boolean.prototype.toString);	// true

// null和undefined不能进行解构赋值，使用它们解构赋值会报错
let [a] = null;	// TypeError
```



## 数据结构Set

集合的基本概念：集合是由一组无序且唯一（即不能重复）的项组成的。这个数据结构使用了与有限集合相同的数学概念，应用在计算机的数据结构中。

特点：key和value相同，没有重复的value

### 创建一个set

```javascript
const s = new Set([1, 2, 3]);
console.log(s);	// Set(3) {1, 2, 3}
```

### Set类的属性

```javascript
console.log(s.size);	// 3
```

### Set类的方法

1. set.add(value) 添加一个数据，返回Set结构本身

   ```
   s.add('a').add('b').add('c');
   console.log(s);	// Set(3) {1, 2, 3, "a", "b", "c"}
   ```

2. set.delete(value)  删除指定数据，返回一个布尔值，表示是否删除成功

   ```javascript
   console.log(s.delete('a'));	// true
   console.log(s);	// Set(3) {1, 2, 3, "b", "c"}
   console.log(s.delete('a'));	// false
   ```

3. set.has(value) 判断该值是否为set的成员，返回一个布尔值

   ```javascript
   console.log(s.has('a'));	// false
   console.log(s.has('1'));	// true
   ```

4. set.clear() 清空所有数据，没有返回值

   ```javascript
   s.clear();
   console.log(s);	// Set(0) {}
   ```

5. keys() 返回键名的遍历器

6. values() 返回键值的遍历器，在set中与keys()返回值相同

7. entries() 返回键值对的遍历器

8. forEach() 使用回调函数遍历每个成员

   ```javascript
   s.forEach(function(value, key, set){
      console.log(value, key, set);
   });
   ```

### 利用set为数组去重

```javascript
let arr = [1, 1, 2, 2, 3, 3];
console.log(arr); // [ 1, 1, 2, 2, 3, 3 ]
let dedupArr = [...new Set(arr)];
console.log(dedupArr); // [ 1, 2, 3 ]
```

## 数据结构Map

字典：是用来存储不重复key的Hash结构。不同于集合（Set)的是，字典使用的是[键，值]的形式来储存数据的。

JavaScript 的对象（Object:{})只能用字符串当作键。这给它的使用带来了很大的限制。

为了解决这个问题，ES6提供了Map数据结构。它类似于对象，也是键值对的集合，但是“键”的范围不限于字符串，各种类型的值（包括对象）都可以当作键。也就是说，Object结构提供了“字符串一值”的对应，Map结构提供了“值一值”的对应，是一种更完善的Hash结构实现。如果你需要“键值对”的数据结构，Map 比0bject更合适。

```javascript
var data1 = {a: 1}, data2 = {b: 2}, obj = {};
obj[data1]= 1;
obj[data2] = 2;
console.log(obj);	// { '[object Object]': 2 }
console.log(data1.toString());	// [object Object]
console.log(data1.toString()===data2.toString());	// true
// Object使用对象作为键，会先对对象调用toString方法，该方法返回[object Object]，data1和data2返回值相同，所以data1的值被data2覆盖
```

### 创建一个Map

```javascript
const map = new Map([
  ["a", 1],
  ["b", 2],
]);
console.log(map);	// Map(2) { 'a' => 1, 'b' => 2 }
```

### Map的属性

```javascript
console.log(map.size);	// 2
```

### Map类的方法

1. set(key, value) 设置键名key对应的键值为value,然后返回整个Map结构。如果key已经有值，则键值会被更新，否则就新生成该键。

   ```javascript
   map.set("c", 3).set("d", 4);
   console.log(map);	// Map(4) { 'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4 }
   ```

2. get(key) get方法读取key对应的键值，如果找不到key,返回undefined。

   ```javascript
   console.log(map.get("a"));	// 1
   console.log(map.get("z"));	// undefined
   ```

3. delete(key) 删除某个键，返回true。如果删除失败，返回false

4.  has(key) 方法返回一个布尔值，表示某个键是否在当前Map对象之中。

5. clear()清除所有数据，没有返回值。

6. keys()返回键名的遍历器

7. values()返回键值的遍历器

8. entries()返回键值对的遍历器

9. forEach()使用回调函数遍历每个成员

### 注意事项

1. ```javascript
   // NaN 被认为是同一个值，会被覆盖
   map.set(NaN, 10).set(NaN, 100);
   console.log(map);	// Map(5) { 'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, NaN => 100 }
   ```

2. ```javascript
   // 对象作为键，会比较对象的地址，所以同样是空对象{}，引用地址不同，认为是不同的键
   map.set({}, '').set({}, 'y');
   console.log(map);	// Map(6) { 'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, {} => '', {} => 'y' }
   ```
   
3. map里面的key的排列顺序是按照添加顺序进行排列的。

## Iterator和for... of 循环

基本概念：在ES6中新增了Set和Map两种数据结构，再加上JS之前原有的数组和对象，这样就有了四种数据集合，平时还可以组合使用它们，定义自己的数据结构，比如数组的成员是Map,Map的成员是对象等。这样就需要一种统一的接口机制，来处理所有不同的数据结构。

Iterator就是这样一种机制。它是一种接口，为各种不同的数据结构提供统一的访问机制。任何数据结构只要部署Iterator接口，就可以完成遍历操作，而且这种遍历操作是**依次**处理该数据结构的所有成员。

Iterator遍历器的作用：

* 为各种数据结构，提供一个统一的、简便的访问接口。
* 使得数据结构的成员能够按某种次序排列。
* ES6新增了遍历命令for...of循环，Iterator接口主要供for...of使用。

### 手写Iterator 接口

```javascript
const arr = [1, 2, 3];
function iterator(arr) {
  let index = 0;
  return {
    next: function () {
      return index < arr.length
        ? { value: arr[index++], done: false }
        : { value: undefined, done: true };
    },
  };
}
const it = iterator(arr);
console.log(it.next()); // { value: 1, done: false }
console.log(it.next()); // { value: 2, done: false }
console.log(it.next()); // { value: 3, done: false }
console.log(it.next()); // { value: undefined, done: true }
```

Iterator的遍历过程：

* 创建一个指针对象，指向当前数据结构的起始位置。也就是说，遍历器对象本质上，就是一个指针对象。
* 第一次调用指针对象的next方法，可以将指针指向数据结构的第一个成员。
* 第二次调用指针对象的next方法，指针就指向数据结构的第二个成员。
* 不断调用指针对象的next方法，直到它指向数据结构的结束位置。

每一次调用next方法，都会返回数据结构的当前成员的信息。具体来说，就是返回一个包含value和done两个属性的对象。其中，value属性是当前成员的值，done属性是一个布尔值，表示遍历是否结束。

### 具有Iterator 接口的类型

凡是具有 Symbol.iterator 属性的数据结构都具有 Iterator 接口

```javascript
const arr = [1, 2, 3];
const set = new Set(["a", "b", "c"]);
const map = new Map([["a", 1]]);
const str = 'xyz';

const itArr = arr[Symbol.iterator]();
const itSet = set[Symbol.iterator]();
const itMap = map[Symbol.iterator]();
const itStr = str[Symbol.iterator]();

console.log(itArr); // Object [Array Iterator] {}
console.log(itSet); // [Set Iterator] { 'a', 'b', 'c' }
console.log(itMap); // [Map Entries] { [ 'a', 1 ] }
console.log(itStr); // Object [String Iterator] {}

console.log(itSet.next());  // { value: 'a', done: false }
console.log(itSet.next());  // { value: 'b', done: false }
console.log(itSet.next());  // { value: 'c', done: false }
console.log(itSet.next());  // { value: undefined, done: true }

const obj = {};
console.log(obj[Symbol.iterator]);  // undefined
```

### 具有Iterator接口可进行的操作

1. 解构赋值

   ```javascript
   const set = new Set(["a", "b", "c"]);
   let [x, y] = set;
   console.log(x, y);	// a b
   ```

2. 扩展运算符

   ```javascript
   let str = 'javascript';
   let arrStr = [...str];
   console.log(arrStr);	
   // [ 'j', 'a', 'v', 'a', 's', 'c', 'r', 'i', 'p', 't']
   ```



### for ... of 循环

```javascript
const ofArr = [1, 2, 3, 4];
for (let i of ofArr) {
  console.log(i);
  /* 
    1
    2
    3
    4
  */
}

const m = new Map();
m.set("a", 1).set("b", 2).set("c", 3);
for (let data of m) {
  console.log(data);
  /* 
    [ 'a', 1 ]
    [ 'b', 2 ]
    [ 'c', 3 ]
    */
}
for (let [key, value] of m) {
  console.log(key, value);
  /* 
    a 1
    b 2
    c 3
    */
}
```



## class基本使用

JS语言的传统方法是通过构造函数，定义并生成新对象，是一种基于原型的面向对象系统。这种写法跟传统的面向对象语言（比如C++和Java)差异很大，很容易让新学习这门语言的人感到困惑。

所以，在ES6中新增加了类的概念，可以使用class 关键字声明一个类，之后以这个类来实例化对象。

```javascript
// ES5基于原型构造对象
const obj = function (a, b) {
  this.a = a;
  this.b = b;
  return this;
};
obj.prototype = {
  constructor: obj,
  print: function () {
    console.log(this.a + " " + this.b);
  },
};
const obj1 = new obj("hello", "world").print();	// hello world


// ES6基于类构造对象，其实是ES5写法的语法糖
class obj {
  constructor(a, b) {
    this.a = a;
    this.b = b;
    return this;
  }
  print() {
    console.log(this.a + " " + this.b);
  }
}
const obj2 = new obj("hello", "world").print();	// hello world
```

1. obj中的constructor方法是构造方法，this关键字则代表实例对象。也就是说，ES5的构造函数obj,对应ES6的obj这个类的构造方法。
2. obj这个类除了构造方法，还定义了一个print方法。注意，定义“类”的方法的时候，前面不需要加上function这个关键字，直接把函数定义放进去了就可以了。另外，方法之间不需要逗号分隔，加了会报错。
3. 构造函数的prototype属性，在ES6的“类”上面继续存在。而且类的所有方法都定义在类的prototype属性上面。
4. 定义在类中的方法都是不可以枚举的。
5. constructor方法是类的默认方法，通过new命令生成对象实例时，自动调用该方法。一个类必须有constructor方法，如果没有显式定义，一个空的constructor方法会被默认添加。
6. 生成类的实例对象的写法，与ES5完全一样，也是使用new命令。如果忘记加new,像函数那样调用Class将会报错



## class 的继承

* 子类继承父类使用extends关键字
* 为父类指定静态方法，使用static方法
* super :
  * 在构造函数中可以当一个函数来使用，相当于调用父类的构造函数
  * 在原型方法中，可以当一个对象来使用，相当于父类的原型对象，并且会自动绑定this到子类上




::: demo [vanilla]
```html
<html>
  <head>
    <meta charset="UTF-8" />
    <style>
      canvas {
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.5);
        border-radius: 5px;
      }
    </style>
  </head>
  <body>
    <canvas id="canvas"></canvas>
    <script>
      const canvas = document.querySelector("#canvas");
      const ctx = canvas.getContext("2d");

      const w = (canvas.width = 600);
      const h = (canvas.height = 350);

      class Ball {
        constructor(x, y, r) {
          this.x = x;
          this.y = y;
          this.r = r;
          /*
            ~是二进制的按位取反，~~可以理解为是取整的简写
            对于非数字，~~的结果为0，布尔的true是1
            对于大于0的数，~~相当于是Math.floor
            对于小于0的数，~~相当于是Math.ceil
            */
          this.color = `rgb(${~~Ball.rpFn([55, 255])}, ${~~Ball.rpFn([
            55,
            255,
          ])}, ${~~Ball.rpFn([55, 255])})`;
          return this;
        }

        render(ctx) {
          ctx.save();
          ctx.translate(this.x, this.y);
          ctx.fillStyle = this.color;
          ctx.beginPath();
          ctx.arc(0, 0, this.r, 0, 2 * Math.PI);
          ctx.fill();
          ctx.restore();
          return this;
        }

        static rpFn(arr) {
          // Ball.rpFn(m, n)
          let max = Math.max(...arr),
            min = Math.min(...arr);
          return Math.random() * (max - min) + min;
        }
      }

      //   const ball = new Ball(100, 100, 30).render(ctx);
      class SuperBall extends Ball {
        constructor(x, y, r) {
          // 相当于调用父类的构造函数，继承父类的构造函数的属性
          super(x, y, r);
          // 同时会自动继承父类的静态方法
          this.vy = SuperBall.rpFn([2, 4]);
          this.g = SuperBall.rpFn([0.2, 0.4]);
          this.a = 0;
          return this;
        }
        move(ctx) {
          this.y += this.vy;
          this.vy += this.g;
          let current = this.vy * -0.75;

          if (this.y + this.r >= ctx.canvas.height) {
            this.y = ctx.canvas.height - this.r;
            if (Math.abs(current - this.a) < 0.01) return false;
            this.a = this.vy *= -0.75;
          }
          ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
          // super这里是父类的原型
          super.render(ctx);

          return true;
        }
      }

      let ball, timer;
      canvas.onclick = function (e) {
        let x = e.offsetX,
          y = e.offsetY;
        let r = ~~Ball.rpFn([25, 55]);

        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ball = new SuperBall(x, y, r).render(ctx);
        ballMove();
      };

      function ballMove() {
        timer = window.requestAnimationFrame(ballMove);

        if (!ball.move(ctx)) {
          window.cancelAnimationFrame(timer);
        }
      }
    </script>
  </body>
</html>
```
:::



## Symbol 数据类型

什么是Symbol?  Symbol,表示独一无二的值。它是JS中的第七种数据类型。

基本的数据类型：Null Undefined Number Boolean String Symbol

引用数据类型：Object

```javascript
let s1 = Symbol();
let s2 = Symbol();
console.log(typeof s1); // symbol
console.log(s1===s2);   // false
// SymboL函数前不能使用new否则会报错，原因在于symboL是一个原始类型的值，不是对象。
let s3 = new Symbol();  // TypeError

/* SymboL函数接收一个字符串作为参数，表示对SymboL的描述，
主要是为了在控制台显示,或者转为字符串的时候，比较容易区分*/
let s4 = Symbol("aaa");
let s5 = Symbol("bbb");
console.log(s4, s5);    // Symbol(aaa) Symbol(bbb)
console.log(Symbol("ccc") === Symbol("ccc"));   // false
```

### SymboL数据类型的转换

```javascript
console.log(String(Symbol("ddd"))); //SymboL(ddd)
console.log(Symbol("eee").toString()); //Symbol(eee)
console.log(!!Symbol()); //true
console.log(Number(Symbol()));  // TypeError
// 不能转换为数字，不能运算，不能拼接
```

### 作为对象的属性名

```javascript
let fff = Symbol("fff");
const data1 = {};
data1[fff] = "hello";
console.log(data1);   // { [Symbol(fff)]: 'hello' }
console.log(data1[fff]);  // hello

let ggg = Symbol("ggg");
const data2 = {
  [ggg]: "world",
};
console.log(data2);  // { [Symbol(ggg)]: 'world' } 
console.log(data2[ggg]);  // world

const data3 = {
  [Symbol()]: 123,
  a: 1,
  b: 2,
};
console.log(data3); // { a: 1, b: 2, [Symbol()]: 123 }
// 没有保留Symbol对象的话，不能被直接访问到
console.log(data3["Symbol()"]);  // undefined
console.log(data3[Symbol()]);  // undefined
/*不能被for...in循环遍历, 虽然不能被遍历，但是也不是私有的属性，可以通过object.getOwnPropertySymbols方法获得一个对象的所有的Symbol属性 */ 
for (let i in data3) {
  console.log(i);
  /* 
  a
  b
  */
}
console.log(Object.getOwnPropertySymbols(data3));   // [ Symbol() ]
console.log(data3[Object.getOwnPropertySymbols(data3)[0]]); // 123
```



## Promise

基本概念：Promise:是ES6中新增的异步编程解决方案，体现在代码中它是一个对象，可以通过Promise构造函数来实例化。

new Promise(cb)  ===>  实例的基本使用 Pending Resolved Rejected

* 两个原型方法：
  * Promise.prototype.then()
  * Promise.prototype.catch()
* 两个常用的静态方法
  * Promise.all()
  * Promise.resolve()

new Promise(cb)

Pending(进行中）===> Resolved(已完成)

Pending(进行中）===> Rejected(已失败)



