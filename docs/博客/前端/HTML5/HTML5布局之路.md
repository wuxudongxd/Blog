---
title: HTML5布局之路
date: 2020-09-20 15:36:28
permalink: /pages/36617b/
categories:
  - 博客
  - 前端
  - html
tags:
  - 
---

## html模板

```html
 <!DOCTYPE HTML>
 <html>  
   <head>  
     <title>网页标题</title>
   </head>
   <body>
       
   </body>
 </html>
```

## 文档声明

DOCTYPE是Document Type(文档类型）的缩写。<!DOCTYPE>元素用于声明一个页面的文档类型定义（Document Type Declaration,DTD)。通过对其定义，告知浏览器当前文件的类型是HTML,这样浏览器才会以合适的方式加载它。

### 关于DOCTYPE

* DOCTYPE标签是单独出现的，没有结束标签
* 文档类型定义在HTML文档的第一行，在htm标签之前
* 文档类型，会使浏览器使用相应标准加载网页并显示
* 文档不定义DOCTYPE,浏览器将无法获知HTML或XHTML文档的类型，有些浏览器会进入怪异解析模式
* DOCTYPE与doctype同理，不区分大小写，均可使用

2. 当前使用的文档声明
   众多的文档声明当中，当前使用的是HTML5.0的版本，即文档声明的代码书写为：
   
   ```html
   <!DOCTYPE HTML> 即 <!doctype html>
   ```

## title 标题

title表示的是一个网页的标题，这个标题并不显示在网页的界面中，而是显示在打开的浏览器的选项卡上。

## meta 元信息


### 元信息

所谓元信息，指的是对信息进行描述的信息。

网页是一个信息，那么这个网页有什么属性呢？比如：这个网页主要讲解的是什么？作者是谁？采用了什么语言？这一系列“信息”都是在对这个网页进行描述，而网页又是信息，所以为了区分开这两种“信息”，就把meta称为“元信息”。

meta是用来在HTML文档中模拟HTTP的响应头报文。meta标签位于网页的`<head>`与`</head>`中，meta的属性有两种：name和http-equiv。

name属性主要用于描述网页，对应于content属性（网页内容，以便于搜索引擎机器人查找、分类）。

http-equiv属性，相当于是http的文件头作用，它可以向浏览器传递一些有用的信息，以帮助正确和精确地显示网页内容，与之对应的content属性，其内容就是各个参数的变量值。

### 元信息基本语法

基本语法：

```html
<meta 属性="该属性下的子属性" content="具体子属性对应的属性值">
```

代码示例：

```html
< meta name = " author" content = "lili" >
< meta http-equiv = "refresh" content = "4;url = http://www.h5course.com" >
```

### name属性

name属性下包含如下几种常见的子属性：generator、keywords、description、author、
copyright、robots。对于一些不太常见的子属性，在此不再介绍。

generator:代码的生成工具，用于向搜索引擎说明网页是使用哪种工具软件开发的。

keywords:关键词，用于向搜索引擎说明这个网页的关键词是什么，通常会填写多个词语，不同词语直接使用英文的逗号分隔开（也可以使用其他标点符号作为分隔符）。

description:描述信息，用于向搜索引擎概括性地介绍这个网页的主要内容是什么。

author:作者，用于向搜索引擎说明网站的作者。

copyright:版权，用于向搜索引擎说明版权信息。

renderer:渲染内核，用于控制浏览器以什么内核进行渲染。Chrome对应webkit，IE6/7（ie兼容内核）对应ie-comp，IE9/IE10/IE11/取决于用户的IE（ie标准内核）对应ie-stand

robots:机器人，用于向搜索引擎说明文件的检索方式，即哪些页面需要索引，哪些页面
不需要索引，主要包括all、none、index、noindex、follow和nofollow几种属性值。

* 设定为all:文件将被检索，且页面上的链接可以被查询
* 设定为none:文件将不被检索，且页面上的链接不可以被查询
* 设定为index:文件将被检索
* 设定为follow:页面上的链接可以被查询
* 设定为noindex:文件将不被检索，但页面上的链接可以被查询（不让robot/spider
  登录）
* 设定为nofollow:文件将不被检索，页面上的链接可以被查询（不让robot/spider
  顺着此页链接向下深入地探查）

**代码示例：**

```html
<meta name="generator" content="Sublime Text" >
<meta name="keywords" content="HTML5,布局，前端，Web">
<meta name="description" content="前端开发学习笔记">
<meta name="author" content="东东">
<meta name="copyright" content="东东">
<meta name="robots" content="all" >
<meta name="renderer" content="webkit">
```

### http-equiv属性

http-equiv属性下包含如下几种常见的子属性：content-type、content-language、refresh、expires、pragma、set-cookie、pics-label、windows-target、page-enter、page-exit。

content-type:用于设置网页内容的编码类型。

content-language:用于说明网页所使用的语言。

refresh:用于定时让网页在指定时间内刷新或跳转到其他页面。

expires:用于设定网页的到期时间，一旦过期则必须到服务器上重新调用。需要注意的是必须使用GMT时间格式。

pragma:用于设定禁止浏览器从本地机的缓存中调阅页面内容，设定后一旦离开网页就无法从Cache中再调出。

set-cookie:用于设置cookie(缓存）的过期时间，如果网页过期，存盘的cookie将被删除。需要注意的也是必须使用GMT时间格式。

pics-label:用于进行网页等级评定，在IE的Internet选项中有一项内容设置，可以防止浏览一些受限制的网站。

windows-target:用于告知网页在当前窗口中以独立页面显示，可以防止自己的网页被别人当作一个frame页调用，即防止被钓鱼。

page-enter:用于设定进入网页时的特殊效果，注意当前页面不能够是一个frame页面。

page-exit:用于设定离开网页时的特殊效果，注意当前页面不能够是一个frame页面。

**代码示例：**

```html
<meta http-equiv = "Content-Type" content = "text/html">
<meta http-equiv = "Content-Language" content = "zh-CN">
<meta http-equiv = "Refresh" content = "n;url=http://yourlink">
<meta http-equiv = "Expires" content = "Sun,31 July 2016 00:20:00 GMT">
<meta http-equiv = "Pragma" content = "no-cache">
<meta http-equiv = "set-cookie" content = "Mon,12 May 2001 00:20:00 GMT">
<meta http-equiv = "Pics-label" content = "">
<meta http-equiv = "windows-Target" content = "_top">
<meta http-equiv = "Page-Enter" content = "revealTrans(duration=10, transtion=50)">
<meta http-equiv = "Page-Exit" content = "revealTrans(duration=20, transtion=6)">
```

### 字符编码

```html
<meta charset="UTF-8">
```

UTF也可以使用小写的utf

**代码书写位置：**

该行代码的位置，建议书写在`<head></head>`标签中的第一位置（即作为head元系的第一个子元素)，放置位置要前于`<title></title>`。这样放置的主要原因在于：网页文档是自上而下加载的，在title当中有时也会使用汉字，如果将title标签放置在`<meta charset="UTF-8">`之前，那么title中的中文内容有可能变成乱码，同时在IE8等浏览器中，整个网页的加载都会被“阻塞”。

## div标签

div元素是一个块元素，默认情况下占据父级元素的宽度，由内容撑开高度；可以设置宽度和高度，设置后，依旧独自占据父元素的一行。

## link标签

```html
<link rel="stylesheet" type="text/css" href="css/index.css">
```

**rel**： 规定当前文档与被链接文档之间的关系，只有 rel 属性的 "stylesheet"(样式表) 值得到了所有浏览器的支持。其他值只得到了部分地支持

**type**：规定被链接文档的 MIME 类型 (互联网媒体类型)，该属性最常见的 MIME 类型是 "text/css"，该类型描述样式表

**href**：属性规定被链接文档的位置（URL）



## link与@import的区别与使用原则

这两种方法虽然都能够实现外部引入CSS,但是存在一定的区别。也正是由于这些区别，才导致多数人在实际开发中放弃了使用@import的引入方式。

1. link是XHTML标签，除了加载CSS外，还可以定义RSS等其他事务；@import属于CSS范畴，只能加载CSS
2. link引用CSS时，在页面载入时同时加载；@import需要页面网页完全载入以后加载
3. link是XHTML标签，无兼容问题；@import是在CSS2.1中提出的，低版本的浏览器不支持
4. link支持使用JavaScript控制DOM去改变样式；而@import不支持。



## CSS选择器优先级

1. 在行内，使用style属性书写的样式，优先级最高（但是通常都不这么写样式）
   ID选择器的优先级其次，类名选择器优先级再次，优先级最弱的是标签名选择器
2. 在CSS当中，由于网页的读取是自上而下的，因此，对于同种优先级的选择器，后书写的代码生效
3. 对于CSS样式，针对一个元素的同一种属性而设置的样式，会根据选择器的优先级进行覆盖；如果不同的选择器针对元素的不同属性进行了样式设置，这个元素会同时拥有所有符合条件的属性样式





## 样式书写顺序

样式属性可以简单地划分为4大类，分别是“显示样式”、“自身样式”、“文本样式”和“CSS3新样式”。

1. 显示样式：控制元素展示方式的属性，主要包括浮动（float)、定位（position)、展示
   方式（display)、超出状态以及可视化（overflow、visibility)等

2. 自身样式：关于元素自身的样式属性，主要就是本章涉及的5种属性（宽度width
   高度height、外边距margin、内边距padding、边框border)以及最大最小宽高

3. 文本样式：用于处理背景图片、段落文章、文字字体的样式

4. CSS3新样式：CSS3新增的属性，第13~16章当中讲解的属性

在进行编码时，建议遵循“显示样式”→“自身样式”→“文本样式”→“兼容与CSS3新样式”，一方面便于开发者查看，另一方面，浏览器对一个标签样式的解析，是按照“显示样式”→“自身样式”→“文本样式”的顺序执行的。

在此给出比较完整的代码书写顺序（推荐）:

* display;
* position;
* position相关的left、top、right、bottom、z-index;
* float;
* clear;
* width;
* height;
* margin;
* padding;
* border;
* background;
* color;
* font;
* text-decoration;
* text-align;
* vertical-align;
* white-space;
* text-XXX(其他的text类属性）;
* CSS3类。



## margin值

1. 设置margin为4个值时，值与方向的对应顺序为“上-右-下-左”（从顶部，顺时针转下来）。
   margin有4个属性值的代码：
	`margin : 10px 5px 20px 0px ;`
2. 设置margin为三个值时，值与方向的对应顺序为“上-左右-下”
3. 设置margin为两个值时，值与方向的对应顺序为“上下-左右”
4. 设置margin为一个值时，该值表示4个方向的外边距均设置为这个属性值
5. margin的特殊应用：
   将元素的水平方向margin值设置为auto,能够让块元素在父级当中水平居中。

margin属性的属性值：

| 值      | 描述                                       |
| ------- | ------------------------------------------ |
| auto    | 浏览器计算外边距                           |
| length  | 规定以具体单位计的外边距值，如px、em、cm等 |
| %       | 规定基于父元素的宽度的百分比的外边距       |
| inherit | 规定应该从父元素继承外边距                 |

父元素就是包括外边距在内的整个盒子的大小，也就是上层元素的内容区

## padding值

1. padding值并没有负值，设置负值时相当于0
2. padding值的单位设置为百分比时，也是按照父级宽度进行计算的。



## 盒模型的实践问题

* border:0;与border:none;的区别。

  border:0;表示设置边框，但是边框的宽度为0,此时浏览器会正常渲染元素的边框效果，会占用内存空间；并且，所有的浏览器均能够正常使用。

  border:none;表示不设置边框，浏览器不会进行任何渲染，不会占据内存空间；IE6、7不兼容。

* 盒模型大小与元素实际宽高值（width、height)并不相同。

  这个概念至关重要，盒模型大小与元素的实际宽高，并不相同！
  盒模型的宽度=左右外边距+左右边框+左右内边距+width
  盒模型的高度=上下外边距十上下边框十上下内边距+height

* 父子之间用padding,兄弟之间用margin



## float浮动特性

float:left;表示向左浮动，标签（或元素）从右边向上浮起，再从右向左浮动到水槽左边。

float:right;表示向右浮动，标签从左边向上浮起，再从左向右浮动到水槽的右边。

在浮动过程中，如果遇到同方向的其他元素，有可能会被“阻碍”。

在浮动过程中，左浮动的元素和右浮动的元素并不会互相干涉、阻碍对方运动。

在物理空间的占用方面，左右浮动元素会互相影响。



## 清除浮动影响

1. 并非浮动元素的所有兄弟级元素都需要清除浮动，只需要针对浮动元素的下一个兄弟级元素设置清浮动，后面所有元素的布局都会恢复
2. 如果希望在第二个div(浮动元素）与第三个div(清除浮动的兄弟级元素）之间有一定的间距，为浮动元素后的一个兄弟级元素设置顶部外边距时会失效（与上方空白区叠加，此时，可以为浮动元素设置下边距

使用after伪元素清除浮动

```css
.clearfix:after{ 
    content: '\200B';
    clear: both;
    display: block;
    height: 0;
}
.clearfix{
    *zoom: 1;
}
```

after伪元素清浮动的原理其实与空标签清浮动的原理类似。

content: '\200B';中 content表示的是内容，对于伪元素来说，如果没有内容，则不会显示在页面当中，因此需要设置内容，但是如果将内容设置出来，就会展示在页面上。在此只是想借助after伪元素来清浮动，自然不希望内容显示在页面当中，于是使用\200B,它表示一个零宽度的空格。因此，这句代码就相当于是为after伪元素定义了内容，使after伪元素能够显示出来；又将内容设置成了零宽度的空格，让这个特殊的内容不影响页面布局和显示

display: block;代码是将伪元素设置为“块状元素”，也就是让其具备div元素的特点。

height: 0; 代码是将伪元素的高度设置为0,目的在于防止高度对布局造成的影响。

在IE8+以及其他主流浏览器当中，当浮动元素与非浮动元素处于同一行时，浮动元素在前，非浮动元素在后；对于IE6、IE7浏览器，当非浮动元素在前，浮动元素在后时，会产生换行。

此处的问题仅针对行内元素。



## 加强版选择器优先级算法

| 选择器       | 示例                                          | 备注                                                         |
| ------------ | --------------------------------------------- | ------------------------------------------------------------ |
| 后代选择器   | 选择器1  选择器2  选择器3   .... 选择器n      | 选择到的时所有符合条件的后代                                 |
| 子代选择器   | 选择器1 > 选择器2 > 选择器3 >  .... > 选择器n | 选择到的时符合条件的子代                                     |
| 群组选择器   | 选择器1 , 选择器2 , 选择器3 ,  .... , 选择器n | 相当于多选                                                   |
| 通配符选择器 | * { }                                         |                                                              |
| 毗邻选择器   | 选择器1 + 选择器2                             | 选择器1与选择器2为同级，且选择器2在选择器1后边，选中的是选择器2，即使后面有和选择器2相同类型的也并不会被选中 |

在之前，讲解过ID选择器、类名选择器以及标签名选择器的优先级，那么对于后代、子代、群组选择器，优先级是如何计算的呢？

* 对于后代、子代选择器，其优先级是进行叠加运算（叠加但不进位）
* 对于群组选择器，每个逗号分隔不同的选择器，不同的选择器各自计算各自的优先级



## 伪类选择器的顺序

a标签书写要求遵循如下顺序：link→：visited→：hover→：active。
伪类选择器的顺序不能颠倒的最主要原因在于，4种伪类选择器的优先级相同，由于网页文档的加载是自上而下的，因此后加载的样式会覆盖掉之前加载的样式，如果顺序错误，那么会导致在某些状态下的显示样式被其他状态的显示样式所覆盖。之所以是按照link、visited、hover、active的顺序进行书写，是由于：

1. 当链接没有被访问的时候，显示link样式，也就是链接的默认样式。
2. 当访问过了某一个链接，应当让用户能够看出和未访问链接的不同，因此，visited伪类的样式应当覆盖link伪类的样式；可以得出书写顺序link→visited。
3. 当光标移动到链接上时，无论被访问过的标签还是没有被访问的标签，都应当出现光标移入即hover的样式效果，因此，hover这个伪类的样式，必须能够覆盖掉visited和link的样式；可以得出link→visited→hover
4. 当鼠标在标签上按下时，应当显示active样式。但是注意，当鼠标在标签上按下时，光标一直处于标签上，也就是说，hover伪类的样式一直处于激活状态，那么要区分开“光标移入不按下鼠标”和“光标移入并按下鼠标”两种状态，就需要用active伪类样式覆盖掉hover的伪类样式。自此，可以得出这样的书写顺序：link→visited→hover→active。



##  定位布局

如果元素设置了position: relative，并不会脱离文档流；如果设置了position: absolute，会使该元素脱离文档流

要激活对象的绝对定位，必须设置position的属性值为absolute,并且指定left,right,top,bottom中的至少一个属性，否则上述属性会使用它们的默认值auto,这将导致对象遵从正常的HTML布局规则，在前一个对象之后立即被呈递。

**绝对定位元素针对谁进行定位** :

* 如果父级（无限）没有设定position属性，那么当前的absolute则结合top,right,left,bottom属性以浏览器左上角为原始点进行定位。
* 如果父级（无限）设定position属性，且属性值为relative、absolute、fixed,那么当前的absolute则结合top,right,left,bottom属性以父级（最近）的左上角为原始点进行定位。

