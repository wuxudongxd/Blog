---
title: HTML5
date: 2021-01-20 20:45:08
permalink: /pages/76f4b3/
categories:
  - html
tags:
  - 前端
  - html
article: false
---
## 拖放事件——drag

### 一、什么是拖放

**拖放**就是通过鼠标放在一个物体上，按住鼠标不放就可以把一个物体托动到另一个位置。其实我们平时一直都有接触，如图

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200928082302632.gif#pic_center)
那么在网页上其实也可以实现同样效果的拖放功能，如图

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020092808263531.gif#pic_center)
那么，就让我们来看看如何实现的吧

### 二、拖放事件

在IE4的时候，Js就可以实现拖放功能了，当时只支持拖放图像和一些文本。后来随着IE版本的更新，拖放事件也在慢慢完善，HTML5就以IE的拖放功能制定了该规范，Firefox 3.5、Safari 3+、chrome以及它们之后的版本都支持了该功能。

默认情况下，网页中的图像、链接和文本是可以拖动的，而其余的元素若想要被拖动，必须将 `draggable` 属性设置为 `true`，这是HTML5规定的新属性，用于设置元素是否能被拖动。因此，图像、链接、文本的 `draggable` 属性默认为 `true`，其余元素的 `draggable` 属性默认为 `false`

在实现拖放功能时有这样两个概念，分别是**被拖动元素**和**目标元素**，它们都有各自支持的事件，那么我们来了解一下

#### （1）被拖动元素的事件

被拖动元素所支持的事件如下表所示

| 事件      | 含义                         |
| --------- | ---------------------------- |
| dragstart | 准备拖动被拖动元素时触发     |
| drag      | 拖动的过程中触发（频繁触发） |
| dragend   | 拖动结束时触发               |

我们来用一个例子测试一下这三个事件

::: demo [vanilla]
```html
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .box{
            width: 100px;
            height: 100px;
            background-color: lightgreen;
        }
    </style>
</head>
<body>
    <div class="box" draggable='true'></div>
    <script>
        let box = document.querySelector('.box')
        // 绑定dragstart事件
        box.addEventListener('dragstart', function() {
            console.log('拖拽开始了');
        })
        // 绑定drag事件
        box.addEventListener('drag', function() {
            console.log('元素被拖动');
        })
        // 绑定dragend事件
        box.addEventListener('dragend', function() {
            console.log('拖拽结束');
        })
    </script>
</body>
</html>
```
:::

我们来看看测试效果

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200928084509208.gif#pic_center)

#### （2）目标元素的事件

在实现拖放功能的过程中，目标元素上的事件有如下三个

| 事件      | 含义                                     |
| --------- | ---------------------------------------- |
| dragenter | 被拖放元素进入目标元素时触发             |
| dragover  | 被拖放元素在目标元素内时触发（频繁触发） |
| dragleave | 被拖动元素离开目标元素时触发             |
| drop      | 当被拖动元素被放到了目标元素中时触发     |

这里我要详细讲解一下这三个事件的触发规则：

1. **dragenter事件**与 `mouseover` 事件类似，那怎样才算被拖放元素进入目标元素呢？经过测试发现，当被拖放元素的一半以上面积在目标元素内才算进入了目标元素
2. **dragover事件**比较特殊，当拖放元素进入目标元素以后就会一直触发，就跟你设置了一个无限循环的定时器一样，即使你不移动元素也会触发，除非拖放事件结束或者被拖放元素离开目标元素
3. **dragleave事件** 的其触发条件正好与 `dragenter` 事件相反，它是当被拖放元素离开目标元素时触发，经过测试，离开目标元素的条件是：被拖放元素一半以上的面积离开目标元素
4. **drop事件** 可以叫做放置目标事件，它是当被拖放元素放置到了目标元素中时触发。虽然任何元素都支持该事件，但是所有元素默认都是不允许被放置的，所以在不做任何处理的情况下，该事件是不会触发的

同样的，我们来用具体的例子，先来体会一下前三个事件
::: demo [vanilla]
```html
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .box{
            width: 100px;
            height: 100px;
            background-color: lightgreen;
        }
        .location{
            width: 100px;
            height: 100px;
            background-color: lightpink;
        }
    </style>
</head>
<body>
    <div class="box" draggable='true'></div>
    <div class="location"></div>
    <script>
        let located = document.querySelector('.location')
        // 绑定dragenter事件
        located.addEventListener('dragenter', function() {
            console.log('元素进入了目标元素');
        })
        // 绑定dragover事件
        located.addEventListener('dragover', function() {
            console.log('元素在目标元素内');
        })
        // 绑定dragleave事件
        located.addEventListener('dragleave', function() {
            console.log('元素离开了目标元素');
        })
    </script>
</body>
</html>
```
:::

我们来看看测试效果，这里你们要仔细看**被拖放元素**多少面积进入或离开目标元素时才触发的对应事件

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200928090326384.gif#pic_center)
那么最后我们再来将一下如何才能触发 **drop事件**，只需要阻止 **dragenter事件** 和 **dragover事件** 的默认行为即可。

::: demo [vanilla]
```html
<html>
    <div class="box" draggable='true'></div>
    <div class="location"></div>
</html>
<style>
    .box{
        width: 100px;
        height: 100px;
        background-color: lightgreen;
    }
    .location{
        width: 100px;
        height: 100px;
        background-color: lightpink;
    }
</style>
<script>
    let located = document.querySelector('.location')
    located.addEventListener('dragenter', function(e) {
        e.preventDefault()
    })
    located.addEventListener('dragover', function(e) {
        e.preventDefault()
    })
    located.addEventListener('drop', function() {
        console.log('元素被放置');
    })
</script>
```
:::

来看下测试效果图

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200928091640987.gif#pic_center)

值得注意的是，在我们没有对 **drop事件** 做处理之前，将被拖放元素拖动到目标元素中时，鼠标样式会变成禁止的样式，如图
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200928091712604.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0xfUFBQ,size_16,color_FFFFFF,t_70#pic_center)
而当我们设置元素为可放置了以后，鼠标样式是这样的。如图

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200928091756298.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0xfUFBQ,size_16,color_FFFFFF,t_70#pic_center)

### 三、dataTransfer对象

上面只是简简单单地实现了拖放功能，但并没用利用该功能做出什么实际性的功能，这里我们介绍一个拖放事件中事件对象上的一个特别重要的属性——**dataTransfer**

我们通过 `event.dataTransfer` 来获取该对象，其主要的作用就是从被拖放元素向目标元素传递一个字符串数据

#### （1）方法

dataTransfer上有两个方法，如下表所示

| 方法    | 含义                       |
| ------- | -------------------------- |
| setData | 设置字符串，并设置数据类型 |
| getData | 获取对应数据类型的字符串   |

**setData()** 方法接收两个参数，**第一个参数**表示的是字符串的数据类型，HTML5规定了两种数据类型，分别是 `text/plain` 和 `text/uri-list`，前者表示普通字符串，后者表示URL字符串；**第二个参数** 就是用于存放的字符串

**getData()** 方法只接收一个参数，即需要接收的字符串类型

我们来简单使用一下这两个方法

::: demo [vanilla]
```html
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .box{
            width: 100px;
            height: 100px;
            background-color: lightgreen;
        }
        .location{
            width: 100px;
            height: 100px;
            background-color: lightpink;
        }
    </style>
</head>
<body>
    <div class="box" draggable='true'></div>
    <div class="location"></div>
    <script>
		let box = document.querySelector('.box')
		// 为被拖放元素绑定 dragstart 事件
        box.addEventListener('dragstart', function(e) {
        	// 设置类型为 text/plain 的 字符串
            e.dataTransfer.setData('text/plain', '我是拖放开始时被设置的字符串')
        })

        let located = document.querySelector('.location')
        
        located.addEventListener('dragenter', function(e) {
            e.preventDefault()
        })
        located.addEventListener('dragover', function(e) {
            e.preventDefault()
        })
        located.addEventListener('drop', function(e) {
        	// 将被拖放元素放置到目标元素时获取字符串
            let res = e.dataTransfer.getData('text/plain')
            console.log(res);
        })
    </script>
</body>
</html>
```
:::

来看下测试结果

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020092809435367.gif#pic_center)

#### （2）属性

在 **dataTransfer对象** 上还有两个比较常用的属性，如下表所示

| 属性          | 含义                   |
| ------------- | ---------------------- |
| dropEffect    | 被拖放元素的放置行为   |
| effectAllowed | 目标元素支持的放置行为 |

首先说一下，这个两个属性需要搭配使用，它们决定了**被拖放元素** 和 **目标元素** 之间的关系的，当设定好两者的关系后，在进行拖动操作的时候，鼠标会根据不同的关系显示不同的样式，除此之外，没有别的特别的作用。

------

**dropEffect** 可以设置以下几个属性

| 值   | 含义                                         |
| ---- | -------------------------------------------- |
| none | 默认值。不能把拖动的元素放在这里             |
| move | 应该把拖动的元素移动到该目标元素             |
| copy | 应该把拖动元素复制到该目标元素               |
| link | 表示目标元素会打开被拖放进来的元素对应的链接 |

【注意】：**dropEffect** 属性必须在 **dragenter事件** 中设置，否则无效

------

**effectAllowed** 可以设置以下几个属性

| 值            | 含义                                               |
| ------------- | -------------------------------------------------- |
| uninitialized | 被拖放元素没有设置放置性为                         |
| none          | 被拖放元素不能有放置性为                           |
| copy          | 只允许值为 ‘copy’ 的 dropEffect 目标元素           |
| link          | 只允许值为 ‘link’ 的 dropEffect 目标元素           |
| move          | 只允许值为 ‘move’ 的 dropEffect 目标元素           |
| copyLink      | 只允许值为 ‘copy’ 和 ‘link’ 的 dropEffect 目标元素 |
| copymove      | 只允许值为 ‘copy’ 和 ‘move’ 的 dropEffect 目标元素 |
| linkMove      | 只允许值为 ‘link’ 和 ‘move’ 的 dropEffect 目标元素 |
| all           | 只允许任意值的 dropEffect 目标元素                 |

【注意】：**effectAllowed** 属性必须在 **dragstart事件** 中设置，否则无效

------

上面也说了，这两个属性基本上只是用来改变鼠标样式的，所以如果想实现特定的功能还得我们自己来重写事件的处理函数。

下面来看一个拖放实例：

**需求：** 将一段文本拖放到一个元素中

因为文本是默认支持的拖放元素，所以我们可以不对其做任何的事件绑定。

::: demo [vanilla]
```html
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .location{
            width: 100px;
            height: 100px;
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <div class="box">我是一段测试文字</div>
    <div class="location"></div>
    <script>    
        let located = document.querySelector('.location')

        located.addEventListener('dragenter', function(e) {
            e.dataTransfer.dropEffect = 'copy'
            e.preventDefault()
        })
        located.addEventListener('dragover', function(e) {
            e.preventDefault()
        })
        located.addEventListener('drop', function(e) {
            e.target.innerHTML = e.dataTransfer.getData('text/plain')
        })
    </script>
</body>
</html>
```
:::

测试动图

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200928104149170.gif#pic_center)





## Blob对象

在一般的 Web 开发中，很少会用到 Blob，但 Blob 可以满足一些场景下的特殊需求。Blob，Binary Large Object 的缩写，代表二进制类型的对象。Blob 的概念在一些数据库中有使用到，例如，MYSQL中的 BLOB 类型就表示二进制数据的容器。在 Web 中，Blob 类型的对象表示不可变的类似文件对象的原始数据。通俗点说，Blob对象是二进制数据，但它是类似文件对象的二进制数据，因此可以像操作 File 对象一样操作 Blob 对象，实际上，File 继承自Blob。

### 1. Blob 基本用法

###### 1.1 创建

通过Blob的构造函数创建Blob对象：

```javascript
Blob( blobParts[, options] )
参数说明：
blobParts：数组类型，数组中的每一项连接起来构成 Blob 对象的数据， 数组中的每项元素可以是 ArrayBuffer, 
		   ArrayBufferView, Blob, DOMString 。
options：可选项，字典格式类型，可以指定如下两个属性：
	type：默认值为 ""，它代表了将会被放入到 blob 中的数组内容的 MIME 类型。
	endings：默认值为"transparent"，用于指定包含行结束符\n的字符串如何被写入。
		它是以下两个值中的一个： 
			 "native"，表示行结束符会被更改为适合宿主操作系统文件系统的换行符； 
			 "transparent"，表示会保持blob中保存的结束符不变。
```

**eg:**

```javascript
    var data1 = "a";
    var data2 = "b";
    var data3 = "<div style='color:red;'>This is a blob</div>";
    var data4 = { "name": "abc" };

    var blob1 = new Blob([data1]);
    var blob2 = new Blob([data1, data2]);
    var blob3 = new Blob([data3]);
    var blob4 = new Blob([JSON.stringify(data4)]);
    var blob5 = new Blob([data4]);
    var blob6 = new Blob([data3, data4]);

    console.log(blob1);  //输出：Blob {size: 1, type: ""}
    console.log(blob2);  //输出：Blob {size: 2, type: ""}
    console.log(blob3);  //输出：Blob {size: 44, type: ""}
    console.log(blob4);  //输出：Blob {size: 14, type: ""}
    console.log(blob5);  //输出：Blob {size: 15, type: ""}
    console.log(blob6);  //输出：Blob {size: 59, type: ""}
```

**size:Blob 对象中所包含数据的字节数;**
**注意:** 使用字符串和普通对象创建 Blob 时的不同，blob4 使用通过 JSON.stringify 把 data4 对象转换成 json 字符串，blob5 则直接使用 data4 创建，两个对象的 size 分别为 14 和 15 。blob4 的 size 等于 14 很容易理解，因为 JSON.stringify(data4) 的结果为："{“name”:“abc”}"，正好14 个字节(不包含最外层的引号)。blob5 的 size 等于 15 是如何计算而来的呢？实际上，当使用普通对象创建 Blob 对象时，相当于调用了普通对象的 toString() 方法得到字符串数据，然后再创建 Blob 对象。所以，blob5 保存的数据是 “[object Object]” ，是 15 个字节(不包含最外层的引号)。

###### 1.2 slice方法

slice 方法，返回一个新的 Blob对象，包含了原 Blob 对象中指定范围内的数据。

```javascript
slice([start[, end[, contentType]]])
参数说明：
start： 可选，代表 Blob 里的下标，表示第一个会被会被拷贝进新的 Blob 的字节的起始位置。
	         如果传入的是一个负数，那么这个偏移量将会从数据的末尾从后到前开始计算。
end： 可选，代表的是 Blob 的一个下标，这个下标 -1 的对应的字节将会是被拷贝进新的Blob 的最后一个字节。
           如果你传入了一个负数，那么这个偏移量将会从数据的末尾从后到前开始计算。
contentType： 可选，给新的 Blob 赋予一个新的文档类型。这将会把它的 type 属性设为被传入的值。它的默认值是一个空的字符串。
```

**eg:**

```javascript
    var data = "abcdef";
    var blob1 = new Blob([data]);
    var blob2 = blob1.slice(0,3);
    
    console.log(blob1);  //输出：Blob {size: 6, type: ""}
    console.log(blob2);  //输出：Blob {size: 3, type: ""}
```

通过slice方法，从blob1中创建出一个新的blob对象，size等于3；

### 2. Blob 使用场景

##### 2.1 分片上传

前面已经说过，File 继承自 Blob，因此我们可以调用 slice 方法对大文件进行分片长传。

```javascript
function uploadFile(file) {
  var chunkSize = 1024 * 1024;   // 每片1M大小
  var totalSize = file.size;
  var chunkQuantity = Math.ceil(totalSize/chunkSize);  //分片总数
  var offset = 0;  // 偏移量
  
  var reader = new FileReader();
  reader.onload = function(e) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST","http://xxxx/upload?fileName="+file.name);
    xhr.overrideMimeType("application/octet-stream");
    
    xhr.onreadystatechange = function() {
      if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
        ++offset;
        if(offset === chunkQuantity) {
          alert("上传完成");
        } else if(offset === chunkQuantity-1){
          blob = file.slice(offset*chunkSize, totalSize);   // 上传最后一片
          reader.readAsBinaryString(blob);
        } else {
          blob = file.slice(offset*chunkSize, (offset+1)*chunkSize); 
          reader.readAsBinaryString(blob);
        }
      }else {
        alert("上传出错");
      }
    }
    
    if(xhr.sendAsBinary) {
      xhr.sendAsBinary(e.target.result);   // e.target.result是此次读取的分片二进制数据
    } else {
      xhr.send(e.target.result);
    }
  }
   var blob = file.slice(0, chunkSize);
   reader.readAsBinaryString(blob);
}
```

这段代码还可以进一步丰富，比如显示当前的上传进度，使用多个XMLHttpRequest对象并行上传对象（需要传递分片数据的位置参数给服务器端）等。

##### 2.2 Blob URL

Blob URL 是 blob 协议的 URL，它的格式如下：

```javascript
blob:http://XXX
```

Blob URL 可以通过 URL.createObjectURL(blob) 创建。在绝大部分场景下，我们可以像使用 Http 协议的 URL 一样，使用 Blob URL。常见的场景有：作为文件的下载地址和作为图片资源地址

1. 文件下载地址

```javascript
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Blob Test</title>
    <script>
        function createDownloadFile() {
            var content = "Blob Data";
            var blob = new Blob([content]);
            var link = document.getElementsByTagName("a")[0];
            link.download = "file";
            link.href = URL.createObjectURL(blob);
        }
        window.onload = createDownloadFile;
    </script>
</head>

<body>
    <a>下载</a>
</body>

</html>
```

点击下载按钮，浏览器将会下载一个名为 file 的文件，文件的内容是：Blob Data。通过 Blob 对象，我们在前端代码中就可以动态生成文件，提供给浏览器下载。打开 Chrome 浏览器调试窗口，在 Elements 标签下可以看到生成的 Blob URL 为：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200719220000404.png)

1. 图片资源地址
   为图片文件创建一个Blob URL，赋值给 < img > 标签：

```javascript
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Blob Test</title>
    <script>
        function handleFile(e) {
            var file = e.files[0];
            var blob = URL.createObjectURL(file);
            var img = document.getElementsByTagName("img")[0];
            img.src = blob;
            img.onload = function(e) {
                URL.revokeObjectURL(this.src);  // 释放createObjectURL创建的对象##
            }
        }
    </script>
</head>

<body>
    <input type="file" accept="image/*" onchange="handleFile(this)" />
    <br/>
    <img style="width:200px;height:200px">
</body>

</html>
```

input 中选择的图片会在< img >里显示出来，如图所示：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200719220014613.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1pZUzEwMDAw,size_16,color_FFFFFF,t_70)
同时，可以在Network标签栏，发现这个Blob URL的请求信息：
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020071922033658.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1pZUzEwMDAw,size_16,color_FFFFFF,t_70)
这个请求信息和平时我们使用的 Http URL 获取图片几乎完全一样。我们还可以使用 Data URL 加载图片资源：

```javascript
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Blob Test</title>
    <script>
        function handleFile(e) {
            var file = e.files[0];
            var fileReader = new FileReader();
            var img = document.getElementsByTagName("img")[0];
            fileReader.onload = function(e) {
                img.src = e.target.result;
            }
            fileReader.readAsDataURL(file);
        }
    </script>
</head>

<body>
    <input type="file" accept="image/*" onchange="handleFile(this)" />
    <br/>
    <img style="width:200px;height:200px">
</body>

</html>
```

FileReader 的 readAsDataURL 生成一个 Data URL，如图所示：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200719220627392.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1pZUzEwMDAw,size_16,color_FFFFFF,t_70)
Data URL 对大家来说应该并不陌生，Web 性能优化中有一项措施：把小图片用 base64 编码直接嵌入到 HTML 文件中，实际上就是利用了 Data URL 来获取嵌入的图片数据。

1. 那么 Blob URL 和 Data URL 有什么区别呢？
   （1）Blob URL 的长度一般比较短，但Data URL因为直接存储图片base64编码后的数据，往往很长，如上图所示，浏览器在显 示 Data URL 时使用了省略号（…）。当显式大图片时，使用Blob URL能获取更好的可能性。
   （2）Blob URL 可以方便的使用 XMLHttpRequest 获取源数据；而 Data URL，并不是所有浏览器都支持通过 XMLHttpRequest 获取源数据的。例如：

```javascript
var blobUrl = URL.createObjectURL(new Blob(['Test'], {type: 'text/plain'}));
var x = new XMLHttpRequest();
// 如果设置x.responseType = 'blob'，将返回一个Blob对象，而不是文本:
// x.responseType = 'blob';
x.onload = function() {
    alert(x.responseText);   // 输出 Test
};
x.open('get', blobUrl);
x.send();
```

（3）**Blob URL 只能在当前应用内部使用**，把Blob URL复制到浏览器的地址栏中，是无法获取数据的。Data URL 相比之下，就有很好的移植性，你可以在任意浏览器中使用。

除了可以用作图片资源的网络地址，Blob URL 也可以用作其他资源的网络地址，例如 html 文件、json 文件等，为了保证浏览器能正确的解析 Blob URL 返回的文件类型，需要在创建 Blob 对象时指定相应的 type：

```javascript
// 创建 HTML 文件的 Blob URL
var data = "<div style='color:red;'>This is a blob</div>";
var blob = new Blob([data], { type: 'text/html' });
var blobURL = URL.createObjectURL(blob);

// 创建 JSON 文件的 Blob URL
var data = { "name": "abc" };
var blob = new Blob([JSON.stringify(data)], { type: 'application/json' });
var blobURL = URL.createObjectURL(blob);
```





参考链接：

1. https://www.yuque.com/coloring/web-notes/
2. https://blog.csdn.net/l_ppp/article/details/10884181412
3. https://blog.csdn.net/ZYS10000/article/details/107449321