---
title: svg
date: 2021-01-30 17:31:14
permalink: /pages/60aca1/
categories:
  - HTML5
tags:
  - SVG
---
SVG 是一种基于 XML 语法的图像格式，全称是可缩放矢量图（Scalable Vector Graphics）。其他图像格式都是基于像素处理的，SVG 则是属于对图像的形状描述，所以它本质上是文本文件，体积较小，且不管放大多少倍都不会失真。

<!-- more -->

## 一、认识 SVG

位图与矢量图的区别：

位图是由像素点构成的，矢量图则是由一些形状元素构成。下图中放大位图可以看到点，而放大矢量图看到的仍然是形状。SVG 属于矢量图，因此理论上能够无限缩放，而不会导致马赛克。

![image-20190721201713555](https://img.wuxd.top/img/20210130220546.png!webp)

简单的 SVG 样式：

::: demo [vanilla]
```html
<html>
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">  <path fill="#000" fill-rule="evenodd" d="M8.667 8L12 11.333l-.667.667L8 8.667 4.667 12 4 11.333 7.333 8 4 4.667 4.667 4 8 7.333 11.333 4l.667.667z" opacity=".64"/></svg>
</html>
```
:::


SVG 的几大特点：

1）能使用 CSS/JS 进行控制。

2）与分辨率无关，在任何分辨率的设备上都能清晰地展示。就不需要为高清屏准备二倍图、三倍图了。

3）容易编辑

4）高度可压缩

来看下 SVG 格式与 JPG、GIF、PNG 图片格式在使用上的区别：

![image-20190728170717428](https://img.wuxd.top/img/20210130220917.png!webp)

## 二、SVG 使用方法

使用 SVG 图片有多种方式，每种方式都有自己的优缺点，选择合适的方式就好。

1、在 Img 中引入

```html
<img src="logo.svg" alt="Logo" height="65" width="68">
```

需要注意的是，使用这种方法在交互性上有很多限制，如不能使用 JS 来控制，不支持 CSS3 动画。



2、通过 CSS 中 Background-image 引入

```css
.logo { background-image: url(logo.svg);}
```

使用背景图片方法需要注意的一点是，最好不要使用 base64 编码来格式化 SVG 图片，因为它在加载完前会阻塞其它资源的下载。使用这种方法在交互性上也有很多限制，如不能使用 JS 来控制，不支持 CSS3 动画。



3、通过 Iframe 引入

```html
<iframe src="logo.svg">Your browser does not support iframes</iframe>
```

不确定这是不是还是一种好的使用方法。



4、通过 Embed 引入

```html
<embed type="image/svg+xml" src="logo.svg" />
```

大多数浏览器都支持，但最好还是不要使用这种方法。



5、通过 Object 引入

```html
<object type="image/svg+xml" data="bblogo.svg">Your browser does not support SVGs</object>
```

如果你想使用 JS 来进行交互控制，`<object>` 是 SVG 使用方法中很好的一个选择。 只需要把它放到 HTML 中就可以了。



6、将 SVG 元素直接加入到 HTML 中

```html
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 68 65">
  <path fill="#1A374D" d="M42 27v-20c0-3.7-3.3-7-7-7s-7 3.3-7 7v21l12 15-7 15.7c14.5 13.9 35 2.8 35-13.7 0-13.3-13.4-21.8-26-18zm6 25c-3.9 0-7-3.1-7-7s3.1-7 7-7 7 3.1 7 7-3.1 7-7 7z"/>
</svg>
```

把 SVG 直接插入到 HTML 中，可以节省 HTTP 请求，而且很方便使用 JS 来控制。但是这样，SVG 资源就不能被浏览器缓存。同时使用 JS 来操控 SVG 会发生重绘行为。



这几种使用方式的特点：

![image-20190728170355429](https://img.wuxd.top/img/20210130221207.png!webp)



## 三、SVG 语法

### 2.1 `<svg>`标签

SVG 代码都放在顶层标签`<svg>`之中。下面是一个例子。

::: demo [vanilla]
```html
<html>
<svg width="100%" height="100%">
  <circle id="mycircle" cx="50" cy="50" r="50" />
</svg>
</html>
```
:::

`<svg>`的width属性和height属性，指定了 SVG 图像在 HTML 元素中所占据的宽度和高度。除了相对单位，也可以采用绝对单位（单位：像素）。如果不指定这两个属性，SVG 图像默认大小是300像素（宽） x 150像素（高）。


如果只想展示 SVG 图像的一部分，就要指定`viewBox`属性。

::: demo [vanilla]
```html
<html>
<svg width="100" height="100" viewBox="50 50 50 50">
	<circle id="mycircle" cx="50" cy="50" r="50" />
</svg>
</html>
```
:::

`<viewBox>`属性的值有四个数字，分别是左上角的横坐标和纵坐标、视口的宽度和高度。上面代码中，SVG 图像是100像素宽 x 100像素高，`viewBox`属性指定视口从`(50, 50)`这个点开始。所以，实际看到的是右下角的四分之一圆。

注意，视口必须适配所在的空间。上面代码中，视口的大小是 50 x 50，由于 SVG 图像的大小是 100 x 100，所以视口会放大去适配 SVG 图像的大小，即放大了四倍。

如果不指定`width`属性和`height`属性，只指定`viewBox`属性，则相当于只给定 SVG 图像的长宽比。这时，SVG 图像的默认大小将等于所在的 HTML 元素的大小。

### 2.2 `<circle>`标签

`<circle>`标签代表圆形。

::: demo [vanilla]
```html
<html>
<svg width="300" height="180">
    <circle cx="30"  cy="50" r="25" />
    <circle cx="90"  cy="50" r="25" class="red" />
    <circle cx="150" cy="50" r="25" class="fancy" />
</svg>
</html>
<style>
	.red {
        fill: red;
    }

    .fancy {
        fill: none;
        stroke: black;
        stroke-width: 3pt;
    }
</style>
```
:::

上面的代码定义了三个圆。`<circle>`标签的`cx`、`cy`、`r`属性分别为横坐标、纵坐标和半径，单位为像素。坐标都是相对于`<svg>`画布的左上角原点。

`class`属性用来指定对应的 CSS 类。

SVG 的 CSS 属性与网页元素有所不同。

> - fill：填充色
> - stroke：描边色
> - stroke-width：边框宽度

### 2.3 `<line>`标签

`<line>`标签用来绘制直线。

::: demo [vanilla]
```html
<html>
<svg width="300" height="180">
    <line x1="0" y1="0" x2="200" y2="0" style="stroke:rgb(0,0,0);stroke-width:5" />
</svg>
</html>
```
:::

上面代码中，`<line>`标签的`x1`属性和`y1`属性，表示线段起点的横坐标和纵坐标；`x2`属性和`y2`属性，表示线段终点的横坐标和纵坐标；`style`属性表示线段的样式。

### 2.4 `<polyline>`标签

`<polyline>`标签用于绘制一根折线。

::: demo [vanilla]
```html
<html>
<svg width="300" height="180">
    <polyline points="3,3 30,28 3,53" fill="none" stroke="black" />
</svg>
</html>
```
:::

`<polyline>`的`points`属性指定了每个端点的坐标，横坐标与纵坐标之间与逗号分隔，点与点之间用空格分隔。

### 2.5 `<rect>`标签

`<rect>`标签用于绘制矩形。

::: demo [vanilla]
```html
<html>
<svg width="300" height="180">
    <rect x="0" y="0" height="100" width="200" style="stroke: #70d5dd; fill: #dd524b" />
</svg>
</html>
```
:::

`<rect>`的`x`属性和`y`属性，指定了矩形左上角端点的横坐标和纵坐标；`width`属性和`height`属性指定了矩形的宽度和高度（单位像素）。

### 2.6 `<ellipse>`标签

`<ellipse>`标签用于绘制椭圆。

::: demo [vanilla]
```html
<html>
<svg width="300" height="180">
	<ellipse cx="60" cy="60" ry="40" rx="20" stroke="black" stroke-width="5" fill="silver"/>
</svg>
</html>
```
:::

`<ellipse>`的`cx`属性和`cy`属性，指定了椭圆中心的横坐标和纵坐标（单位像素）；`rx`属性和`ry`属性，指定了椭圆横向轴和纵向轴的半径（单位像素）。

### 2.7 `<polygon>`标签

`<polygon>`标签用于绘制多边形。

::: demo [vanilla]
```html
<html>
<svg width="300" height="180">
    <polygon fill="green" stroke="orange" stroke-width="1" points="0,0 100,0 100,100 0,100 0,0"/>
</svg>
</html>
```
:::

`<polygon>`的`points`属性指定了每个端点的坐标，横坐标与纵坐标之间与逗号分隔，点与点之间用空格分隔。

### 2.8 `<path>`标签

`<path>`标签用于制路径。

::: demo [vanilla]
```html
<html>
<svg width="300" height="180">
<path d="
M 18,3
L 46,3
L 46,40
L 61,40
L 32,68
L 3,40
L 18,40
Z
"></path>
</svg>
</html>
```
:::

`<path>`的`d`属性表示绘制顺序，它的值是一个长字符串，每个字母表示一个绘制动作，后面跟着坐标。

> - M：移动到（moveto）
> - L：画直线到（lineto）
> - Z：闭合路径

### 2.9 `<text>`标签

`<text>`标签用于绘制文本。

::: demo [vanilla]
```html
<html>
<svg width="300" height="180">
	<text x="50" y="25">Hello World</text>
</svg>
</html>
```
:::

`<text>`的`x`属性和`y`属性，表示文本区块基线（baseline）起点的横坐标和纵坐标。文字的样式可以用`class`或`style`属性指定。

### 2.10 `<use>`标签

`<use>`标签用于复制一个形状。

::: demo [vanilla]
```html
<html>
<svg viewBox="0 0 30 10" xmlns="http://www.w3.org/2000/svg">
    <circle id="myCircle1" cx="5" cy="5" r="4"/>

    <use href="#myCircle1" x="10" y="0" fill="blue" />
    <use href="#myCircle1" x="20" y="0" fill="white" stroke="blue" />
</svg>
</html>
```
:::

`<use>`的`href`属性指定所要复制的节点，`x`属性和`y`属性是`<use>`左上角的坐标。另外，还可以指定`width`和`height`坐标。

### 2.11 `<g>`标签

`<g>`标签用于将多个形状组成一个组（group），方便复用。

::: demo [vanilla]
```html
<html>
<svg width="300" height="100">
    <g id="myCircle2">
     <text x="25" y="20">圆形</text>
     <circle cx="50" cy="50" r="20"/>
    </g>

    <use href="#myCircle2" x="100" y="0" fill="blue" />
    <use href="#myCircle2" x="200" y="0" fill="white" stroke="blue" />
</svg>
</html>
```
:::

### 2.12 `<defs>`标签

::: demo [vanilla]
```html
<html>
<svg width="300" height="100">
    <defs>
     <g id="myCircle3">
       <text x="25" y="20">圆形</text>
       <circle cx="50" cy="50" r="20"/>
     </g>
    </defs>
    <use href="#myCircle3" x="100" y="0" fill="blue" />
    <use href="#myCircle3" x="200" y="0" fill="white" stroke="blue" />
</svg>
</html>
```
:::

`<defs>`标签用于自定义形状，它内部的代码不会显示，仅供引用。

### 2.13 `<pattern>`标签

`<pattern>`标签用于自定义一个形状，该形状可以被引用来平铺一个区域。

::: demo [vanilla]
```html
<html>
<svg width="500" height="500">
    <defs>
     <pattern id="dots" x="0" y="0" width="100" height="100" patternUnits="userSpaceOnUse">
       <circle fill="#bee9e8" cx="50" cy="50" r="35" />
     </pattern>
    </defs>
    <rect x="0" y="0" width="100%" height="100%" fill="url(#dots)" />
</svg>
</html>
```
:::

上面代码中，`<pattern>`标签将一个圆形定义为`dots`模式。`patternUnits="userSpaceOnUse"`表示`<pattern>`的宽度和长度是实际的像素值。然后，指定这个模式去填充下面的矩形。

### 2.14 `<image>`标签

`<image>`标签用于插入图片文件。

::: demo [vanilla]
```html
<html>
<svg viewBox="0 0 100 100" width="100" height="100">
<image xlink:href="https://img.wuxd.top/img/20210130231924.png!webp"
 width="50%" height="50%"/>
</svg>
</html>
```
:::

上面代码中，`<image>`的`xlink:href`属性表示图像的来源。

### 2.15 `<animate>`标签

`<animate>`标签用于产生动画效果。

> Firefox 4 利用[Synchronized Multimedia Integration Language](http://www.w3.org/TR/REC-smil) (SMIL)引入了对动画SVG的支持。SMIL允许你：
>
> - 变动一个元素的数字属性（x、y……）
> - 变动变形属性（translation或rotation）
> - 变动颜色属性
> - 物件方向与运动路径方向同步
>
> 自从Chrome 45，SMIL动画被弃用了，以利于CSS动画和Web动画。

> **SMIL动画**虽然已经被部分浏览器弃用，但目前还属于可使用状态，但建议如简单的属性变化，动画部分建议用css动画替代，但是**沿着特定路径动画**依然需要使用`animateMotion`，如果是简单的svg形状甚至可以直接用`html+css`代替；

> **描边动画**、**形状改变动画**、**沿着特定路径动画**都是目前css比较难以直接替代的一些动画操作，这几种类型的动画svg都是一把好手。

::: demo [vanilla]
```html
<html>
<svg width="500px" height="300px">
    <rect x="0" y="0" width="100" height="100" fill="#feac5e">
     <animate attributeName="x" from="0" to="500" dur="2s" repeatCount="indefinite" />
     <animate attributeName="width" to="500" dur="2s" repeatCount="indefinite" />
    </rect>
</svg>
</html>
```
:::

上面代码中，矩形会不断移动，产生动画效果。

`<animate>`的属性含义如下。

> - attributeName：发生动画效果的属性名。
> - from：单次动画的初始值。
> - to：单次动画的结束值。
> - dur：单次动画的持续时间。
> - repeatCount：动画的循环模式。

### 2.16 `<animateTransform>`标签

`<animate>`标签对 CSS 的`transform`属性不起作用，如果需要变形，就要使用`<animateTransform>`标签。

::: demo [vanilla]
```html
<html>
<svg width="500px" height="500px">
    <rect x="250" y="250" width="50" height="50" fill="#4bc0c8">
     <animateTransform attributeName="transform" type="rotate" begin="0s" dur="10s" from="0 200 200" to="360 400 400" repeatCount="indefinite" />
    </rect>
</svg>
</html>
```
:::

上面代码中，`<animateTransform>`的效果为旋转（`rotate`），这时`from`和`to`属性值有三个数字，第一个数字是角度值，第二个值和第三个值是旋转中心的坐标。`from="0 200 200"`表示开始时，角度为0，围绕`(200, 200)`开始旋转；`to="360 400 400"`表示结束时，角度为360，围绕`(400, 400)`旋转。

## 三、JavaScript 操作

### 3.1 DOM 操作

如果 SVG 代码直接写在 HTML 网页之中，它就成为网页 DOM 的一部分，可以直接用 DOM 操作。

::: demo [vanilla]
```html
<html>
<svg id="mysvg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 600" preserveAspectRatio="xMidYMid meet">
  <circle id="mycircle4" cx="400" cy="300" r="50" />
  <svg>

</html>
<style>
  #mycircle4 {
    stroke-width: 5;
    stroke: #f00;
    fill: #ff0;
  }
  #mycircle4:hover {
    stroke: #090;
    fill: #fff;
  }
</style>
<script>
  var mycircle = document.getElementById('mycircle4');
  mycircle.addEventListener('click', function(e) {
    console.log('circle clicked - enlarging');
    mycircle.setAttribute('r', 60);
  }, false);
</script>
```
:::

### 3.2 获取 SVG DOM

使用`<object>`、`<iframe>`、`<embed>`标签插入 SVG 文件，可以获取 SVG DOM。

```javascript
var svgObject = document.getElementById('object').contentDocument;
var svgIframe = document.getElementById('iframe').contentDocument;
var svgEmbed = document.getElementById('embed').getSVGDocument();
```

注意，如果使用`<img>`标签插入 SVG 文件，就无法获取 SVG DOM。

### 3.3 读取 SVG 源码

由于 SVG 文件就是一段 XML 文本，因此可以通过读取 XML 代码的方式，读取 SVG 源码。

```html
<div id="svg-container">
<svg
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 xml:space="preserve" width="500" height="440"
  >
 <!-- svg code -->
</svg>
</div>
```

使用`XMLSerializer`实例的`serializeToString()`方法，获取 SVG 元素的代码。

```javascript
var svgString = new XMLSerializer().serializeToString(document.querySelector('svg'));
```

### 3.4 SVG 图像转为 Canvas 图像

首先，需要新建一个`Image`对象，将 SVG 图像指定到该`Image`对象的`src`属性。

```javascript
var img = new Image();
var svg = new Blob([svgString], {type: "image/svg+xml;charset=utf-8"});

var DOMURL = self.URL || self.webkitURL || self;
var url = DOMURL.createObjectURL(svg);

img.src = url;
```

然后，当图像加载完成后，再将它绘制到`<canvas>`元素。

```javascript
img.onload = function () {
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
ctx.drawImage(img, 0, 0);
};
```



## 四、实例：折线图

下面将一张数据表格画成折线图。

```
Date |Amount
-----|------
2014-01-01 | $10
2014-02-01 | $20
2014-03-01 | $40
2014-04-01 | $80
```



上面的图形，可以画成一个坐标系，`Date`作为横轴，`Amount`作为纵轴，四行数据画成一个数据点。

::: demo [vanilla]
```html
<html>
<svg width="350" height="160">
    <g class="layer" transform="translate(60,10)">
     <circle r="5" cx="0"   cy="105" />
     <circle r="5" cx="90"  cy="90"  />
     <circle r="5" cx="180" cy="60"  />
     <circle r="5" cx="270" cy="0"   />

     <g class="y axis">
       <line x1="0" y1="0" x2="0" y2="120" />
       <text x="-40" y="105" dy="5">$10</text>
       <text x="-40" y="0"   dy="5">$80</text>
     </g>
     <g class="x axis" transform="translate(0, 120)">
       <line x1="0" y1="0" x2="270" y2="0" />
       <text x="-30"   y="20">January 2014</text>
       <text x="240" y="20">April</text>
     </g>
    </g>
</svg>
</html>
```
:::





参考：

1. 阮一峰的网络日志，[SVG 图像入门教程](http://www.ruanyifeng.com/blog/2018/08/svg.html)
2.  [liu, summerqy](http://www.alloyteam.com/author/summerqy-liu/) ，[SVG优化探索](http://www.alloyteam.com/2019/07/13782/)
3. Apollozz，[SVG-动画的一把好手](https://juejin.cn/post/6844903731855638535)

