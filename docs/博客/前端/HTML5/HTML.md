---
title: HTML
date: 2021-01-20 19:40:15
permalink: /pages/0cfba5/
categories:
  - HTML5
tags:
  - html
article: false
---
## 一、开发规范

> **尽量遵循 HTML 标准和语义，但是不要以牺牲实用性为代价。任何时候都要尽量使用最少的标签并保持最小的复杂度。**

### 文件夹命名

| 文件           | 命名          |
| -------------- | ------------- |
| 源代码         | src           |
| 图片           | img           |
| JavaScript脚本 | js            |
| 第三方依赖包   | dep           |
| 文档           | doc           |
| 测试           | test          |
| 公共资源       | common        |
| 静态资源       | public/static |
| 组件           | component     |
| 模板文件       | view/tpl      |

### 基本属性

> 根据HTML5规范：应在html标签上加上lang属性。这会给语音工具和翻译工具帮助，告诉它们应当怎么去发音和翻译。

> 字符编码：通过声明一个明确的字符编码，让浏览器轻松、快速的确定适合网页内容的渲染方式，通常指定为'UTF-8'。

> IE兼容模式：用 `<meta>` 标签可以指定页面应该用什么版本的IE来渲染；

> viewport：一般用来定义浏览器窗口内容区的大小，不包含工具条、选项卡等内容；特别是最H5移动端适配建议加上。

```html
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    </head>
</html>
```

### 引入CSS, JS

根据HTML5规范, 通常在引入CSS和JS时不需要指明 type，因为 `text/css` 和 `text/javascript` 分别是他们的默认值。

```html
<!-- External CSS -->
<link rel="stylesheet" href="code_guide.css">

<!-- In-document CSS -->
<style>

</style>

<!-- External JS -->
<script src="code_guide.js"></script>

<!-- In-document JS -->
<script>

</script>
```

### 属性顺序

属性应该按照特定的顺序出现以保证易读性（如下属性从前往后）；

1. class
2. id
3. name
4. data-*
5. src, for, type, href, value , max-length, max, min, pattern
6. placeholder, title, alt
7. aria-*, role
8. required, readonly, disabled

> class是为高可复用组件设计的，所以应处在第一位；

> id更加具体且应该尽量少使用，所以将它放在第二位。

```html
<a class="..." id="..." data-modal="toggle" href="#">Example link</a>

<input class="form-control" type="text">

<img src="..." alt="...">
```

### 布尔值属性

boolean属性指不需要声明取值的属性，XHTML需要每个属性声明取值，但是HTML5并不需要；boolean属性的存在表示取值为true，不存在则表示取值为false。

```html
<input type="text" disabled>

<input type="checkbox" value="1" checked>

<select>
    <option value="1" selected>1</option>
</select>
```



## 二、文档声明

DOCTYPE是Document Type(文档类型）的缩写。<!DOCTYPE>元素用于声明一个页面的文档类型定义（Document Type Declaration,DTD)。通过对其定义，告知浏览器当前文件的类型是HTML,这样浏览器才会以合适的方式加载它。

### 1. 关于DOCTYPE

* DOCTYPE标签是单独出现的，没有结束标签
* 文档类型定义在HTML文档的第一行，在html标签之前
* 文档类型，会使浏览器使用相应标准加载网页并显示
* 文档不定义DOCTYPE,浏览器将无法获知HTML或XHTML文档的类型，有些浏览器会进入怪异解析模式
* DOCTYPE与doctype同理，不区分大小写，均可使用

### 2. 当前使用的文档声明

众多的文档声明当中，当前使用的是HTML5.0的版本，即文档声明的代码书写为：

```html
<!DOCTYPE HTML> 即 <!doctype html>
```



## 三、<meta\> 元素

### 概要

meta标签提供关于HTML文档的元数据。元数据不会显示在页面上，但是对于机器是可读的。它可用于浏览器（如何显示内容或重新加载页面），搜索引擎（关键词），或其他 web 服务。

meta是用来在HTML文档中模拟HTTP的响应头报文。meta标签位于网页的`<head>`与`</head>`中，meta的属性有两种：name和http-equiv。

name属性主要用于描述网页，对应于content属性（网页内容，以便于搜索引擎机器人查找、分类）。

http-equiv属性，相当于是http的文件头作用，它可以向浏览器传递一些有用的信息，以帮助正确和精确地显示网页内容，与之对应的content属性，其内容就是各个参数的变量值。

**属性**

| 属性       | 值                                                           | 描述                                   | 是否必须 |
| ---------- | ------------------------------------------------------------ | -------------------------------------- | -------- |
| http-equiv | content-type / expire / refresh / set-cookie                 | 把content属性关联到HTTP头部。          | 可选     |
| name       | author / description / keywords / generator / revised / others | 把 content 属性关联到一个name。        | 可选     |
| content    | some text                                                    | 定义与http-equiv或name属性相关的元信息 | 必要     |

### 网页相关

- **申明编码**

```html
<meta charset="UTF-8">
```

> 该行代码的位置，建议书写在`<head></head>`标签中的第一位置（即作为head元系的第一个子元素)，放置位置要前于`<title></title>`。这样放置的主要原因在于：网页文档是自上而下加载的，在title当中有时也会使用汉字，如果将title标签放置在`<meta charset="UTF-8">`之前，那么title中的中文内容有可能变成乱码，同时在IE8等浏览器中，整个网页的加载都会被“阻塞”。

- **优先使用 IE 最新版本和 Chrome**

```html
<!-- 关于X-UA-Compatible -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> <!-- 推荐 -->

<meta http-equiv="X-UA-Compatible" content="IE=6" ><!-- 使用IE6 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" ><!-- 使用IE7 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" ><!-- 使用IE8 -->
```

- **浏览器内核控制**：国内浏览器很多都是双内核（webkit和Trident），webkit内核高速浏览，IE内核兼容网页和旧版网站。而添加meta标签的网站可以控制浏览器选择何种内核渲染。[参考文档](http://se.360.cn/v6/help/meta.html)

```html
默认用极速核(Chrome)：<meta name="renderer" content="webkit"> 
默认用ie兼容内核（IE6/7）：<meta name="renderer" content="ie-comp"> 
默认用ie标准内核（IE9/IE10/IE11/取决于用户的IE）：<meta name="renderer" content="ie-stand"> 
```

国内双核浏览器默认内核模式如下：

1. 搜狗高速浏览器、QQ浏览器：IE内核（兼容模式）
2. 360极速浏览器、遨游浏览器：Webkit内核（极速模式）

- **禁止浏览器从本地计算机的缓存中访问页面内容**：这样设定，访问者将无法脱机浏览。

```html
<meta http-equiv="Pragma" content="no-cache">
```

- **Windows 8**

```html
<meta name="msapplication-TileColor" content="#000"/> <!-- Windows 8 磁贴颜色 -->
<meta name="msapplication-TileImage" content="icon.png"/> <!-- Windows 8 磁贴图标 -->
```

- **站点适配**：主要用于PC-手机页的对应关系。

```html
<meta name="mobile-agent"content="format=[wml|xhtml|html5]; url=url">
<!--
[wml|xhtml|html5]根据手机页的协议语言，选择其中一种；
url="url" 后者代表当前PC页所对应的手机页URL，两者必须是一一对应关系。
 -->
```

- **转码申明**：用百度打开网页可能会对其进行转码（比如贴广告），避免转码可添加如下meta。

```html
<meta http-equiv="Cache-Control" content="no-siteapp" />
```

### SEO优化

[参考文档](http://msdn.microsoft.com/zh-cn/library/ff724016)

- **页面关键词**，每个网页应具有描述该网页内容的一组唯一的关键字。
  使用人们可能会搜索，并准确描述网页上所提供信息的描述性和代表性关键字及短语。标记内容太短，则搜索引擎可能不会认为这些内容相关。另外标记不应超过 874 个字符。

```html
<meta name="keywords" content="your tags" />
```

- **页面描述**，每个网页都应有一个不超过 150 个字符且能准确反映网页内容的描述标签。

```html
<meta name="description" content="150 words" />
```

- **搜索引擎索引方式**，robotterms是一组使用逗号(,)分割的值，通常有如下几种取值：none，noindex，nofollow，all，index和follow。确保正确使用nofollow和noindex属性值。

```html
<meta name="robots" content="index,follow" />
<!--
    all：文件将被检索，且页面上的链接可以被查询；
    none：文件将不被检索，且页面上的链接不可以被查询；
    index：文件将被检索；
    follow：页面上的链接可以被查询；
    noindex：文件将不被检索；
    nofollow：页面上的链接不可以被查询。
 -->
```

- **页面重定向和刷新**：content内的数字代表时间（秒），既多少时间后刷新。如果加url,则会重定向到指定网页（搜索引擎能够自动检测，也很容易被引擎视作误导而受到惩罚）。

```html
<meta http-equiv="refresh" content="0;url=" />
```

- **其他**

```html
<meta name="author" content="author name" /> <!-- 定义网页作者 -->
<meta name="google" content="index,follow" />
<meta name="googlebot" content="index,follow" />
<meta name="verify" content="index,follow" />
```

### 移动设备

- **viewport**：能优化移动浏览器的显示。如果不是响应式网站，不要使用initial-scale或者禁用缩放。

```html
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimun-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
<!--这是常用的移动meta设置-->
```

1. width：宽度（数值 或  device-width）（范围从200 到10,000，默认为980 像素）
2. height：高度（数值 或 device-height）（范围从223 到10,000）
3. initial-scale：初始的缩放比例 （范围从>0 到10）
4. minimum-scale：允许用户缩放到的最小比例
5. maximum-scale：允许用户缩放到的最大比例
6. user-scalable：用户是否可以手动缩 (no,yes)

**注意**，很多人使用initial-scale=1到非响应式网站上，这会让网站以100%宽度渲染，用户需要手动移动页面或者缩放。如果和initial-scale=1同时使用user-scalable=no或maximum-scale=1，则用户将不能放大/缩小网页来看到全部的内容。

- **WebApp全屏模式**：伪装app，离线应用。

```html
<meta name="apple-mobile-web-app-capable" content="yes" /> <!-- 启用 WebApp 全屏模式 -->
```

- **主题颜色**

```html
<meta name="theme-color" content="#11a8cd">
```

- **隐藏状态栏/设置状态栏颜色**：只有在开启WebApp全屏模式时才生效。content的值为default | black | black-translucent 。

```html
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
```

- **添加到主屏后的标题**

```html
<meta name="apple-mobile-web-app-title" content="标题">
```

- **忽略数字自动识别为电话号码**

```html
<meta content="telephone=no" name="format-detection" />
```

- **忽略识别邮箱**

```html
<meta content="email=no" name="format-detection" />
```

- **添加智能 App 广告条 Smart App Banner**：告诉浏览器这个网站对应的app，并在页面上显示下载banner(如下图)。[参考文档](https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/PromotingAppswithAppBanners/PromotingAppswithAppBanners.html)

```html
<meta name="apple-itunes-app" content="app-id=myAppStoreID, affiliate-data=myAffiliateData, app-argument=myURL">
```

![](https://cdn.jsdelivr.net/gh/xugaoyi/image_store/blog/20200221134638.png)

- **其他** [参考文档](http://fex.baidu.com/blog/2014/10/html-head-tags/?qq-pf-to=pcqq.c2c)

```html
<!-- 针对手持设备优化，主要是针对一些老的不识别viewport的浏览器，比如黑莓 -->
<meta name="HandheldFriendly" content="true">
<!-- 微软的老式浏览器 -->
<meta name="MobileOptimized" content="320">
<!-- uc强制竖屏 -->
<meta name="screen-orientation" content="portrait">
<!-- QQ强制竖屏 -->
<meta name="x5-orientation" content="portrait">
<!-- UC强制全屏 -->
<meta name="full-screen" content="yes">
<!-- QQ强制全屏 -->
<meta name="x5-fullscreen" content="true">
<!-- UC应用模式 -->
<meta name="browsermode" content="application">
<!-- QQ应用模式 -->
<meta name="x5-page-mode" content="app">
<!-- windows phone 点击无高光 -->
<meta name="msapplication-tap-highlight" content="no">
```

### 常用移动端页面meta设置

```html
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimun-scale=1.0,maximum-scale=1.0,user-scalable=no">
```



## 媒体

### 图片标签

```html
<img src="" alt="" height="" width="">
<!--如果只设置了宽和高的一个,另外一个会等比例自动变化-->
```

### 视频标签

```html
<video src=" " controls="controls" autoplay="autoplay" width=" " height="" preload=" " loop="loop">您的浏览器不支持video(当浏览器不支持video时显示)</video>
<!--这是视频的标准语法,标签中的文字是当视频加载不成功的时候才会显示出来的-->
<!--
	controls="controls"添加浏览器为视频设置的默认控件 
	autoplay="autoplay"设置网页中视频加载就绪后自动播放 
	loop="loop"设置媒介文件循坏播放
	preload="auto/none/meta" 值:1.none表示不加载任何视频 2.meta表示只加载元数据（长度，尺寸等 3.auto表示让浏览器自己决定怎么做（如果引用了autoplay属性，则忽略该属性）
	poster='' ‘’指定加载视频时要显示的图像，接受所需图像的URL(如果引用了autoplay属性，则忽略该属性)	使用方法：<video poster="网址"></video>
	muted="muted" 设置是否静音
-->
```

### 音频标签

```html
<audio src="url" controls="controls" autoplay="autoplay" preload=" " loop="loop">浏览器不支持audio</audio>
```

### source兼容

**< source>< source>可以连接不同的媒体文件**

```html
<video width="" height="" controls="controls">
 	<source src=" " type="">
 	<source src=" " type="">
	<source src=""  type="">           
	浏览器不支持video元素
</video>
<!--
	type用于指定视频类型 一般有三种格式所以type一般为video/webm video/mp4 video/ogg
	兼容原因：由于全球五大浏览器只支持各自的视频，source能在其中挑选出一个符合该浏览器格式的视频播放
-->
```



## 超链接

### 标签

```html
<a href="" target="" hreflang="" title=""></a>
<!--
	href是要跳转页面的链接地址
	注意:<a href="">会刷新当前网页<a hred="#">不会刷新当前网页但会到网页最顶端,可以通过设置锚点的形	式跳转到指定页面的指定位置(锚点由ID写入)
	target设置打开目标窗口的方式 如：_blank:打开一个新的窗口加载
	hreflang规定目标URL的基准语言
	title用做提示信息(可以作为简写后的补充说明)
-->
```



### map标签

```html
<!--<map>用来创建图像映射，与<img>元素相关联 图像映射是指一个图像建立多个链接，在图像上定义多个区域，每个区域链接不同的地址-->
<img src="" usemap="#图像映射名称">
<map name="图像映射名称">
<area shape="形状（circle rect poly四边形等)" coords="坐标" href="" title="">
<area shape="形状（circle rect poly四边形等)" coords="坐标" href="" title="">
</map>
```



### base标签

```html
<base hred="url" target="">
<!--
位于<head>部分，用于浏览器不使用当前文档的url而使用<base>定义的，这样会影响到后面的元素。这个标签主要为了解决web编程的时候一些相对路径的问题
-->
```



### iframe标签

```html
<iframe src="" frameborder="0" scrollinig="no">
    你的浏览器不支持,请使用高版本浏览器
</iframe>
<!--
	iframe标签能在原本页面中再内嵌入一个页面,这个标签的显示模式是inline-block,可以横排显示
	src中写要嵌入页面的域名
	frameborder属性就是这个标签的边框,有两个值，分别为0和1,0就是没有边框,而1是有边框,基本上都是设置为0,同时这个边框和边框线border是不冲突的,可以同时设置
	scrolling是控制这个标签周围是否出现滚动条,有三个值yes,no和auto,默认是yes,一般都是设置no来和页面契合
	注意:这个标签因为是在一个页面中再次嵌入多个页面,所以加载速度会很慢,还有很多安全性问题,尽量减少使用
-->
```

```html
<!--iframe标签可以和a标签搭配使用-->
<a href="http:\\www.tmall.com" target="tmall">跳转到天猫</a>
<iframe src="http:\\www.baidu.com" frameborder="0" scrollinig="no" name="tmall">
你的浏览器不支持,请使用高版本浏览器
</iframe>
<!--
	通过a标签的target绑定iframe标签的name值让点击a标签的时候不是自己网页发生跳转而是iframe中的页面发生跳转,从百度跳转到天猫
-->
```



## 表格 

**表格是网页制作的元老级别标签,这个标签以前用做制作网页主体,所以有许多独属于表格的属性和用法，现在不推荐使用table布局**

### 标签

| 标签    | 用途                               | 单/双标签 | 一个表格中数量 |
| ------- | ---------------------------------- | --------- | -------------- |
| table   | 设置表格                           | 双标签    | 1              |
| caption | 表格题目                           | 双标签    | 0或1           |
| thead   | 表格的头部                         | 双标签    | 0或1           |
| tbody   | 表格的主体                         | 双标签    | 0或n           |
| tfoot   | 表格的尾部                         | 双标签    | 0或1           |
| tr      | 表格的一行                         | 双标签    | 0或n           |
| th      | 表头的单元格(内容居中并以粗体显示) | 双标签    | 0或1           |
| td      | 每一个单元格                       | 双标签    | 0或n           |

### 示例

::: demo [vanilla]
```html
<html>
  <table>
    <caption>
      表格标题
    </caption>
    <thead>
      <tr>
        <th colspan="2">表格头部</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1-1</td>
        <td>1-2</td>
      </tr>
      <tr>
        <td>2-1</td>
        <td>2-2</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="2">表格尾部</td>
      </tr>
    </tfoot>
  </table>
</html>

<style>
  table,
  th,
  td {
    border: 1px solid #000;
    border-spacing: 0;
    border-collapse: collapse;
  }
</style>

```
:::

```html
<!--
	<table border="1">说明这个表格为带表格边框的表格,但这个属性尽量不要用,能用样式解决就不要用属性
	<td colspan="数字"></td>跨列,表示列合并中间的数字表示要合并的单元格列合并
	<td rowspan="数字">跨行,表示行合并，行和列的合并可以一起用
	如果在合并的时候出现了合并列有数据的情况可以直接把那一列删除,因为已经合并了列那一列也不需要了
	表格可以嵌套表格
	<thead></thead>表示表格的头
	<tbody></tbody>表示表格的身体
	<tfoot></tfoot>表示表格的结尾
	上面三个属性是对表格结构上的解析,在写代码的时候可以不写上面三个,正常情况下不会影响表格格局，但是可以用CSS改变表格样式,浏览器解析的时候会自动为表格加上上面的tbody属性,所以在控制js进行选择的时候要注意,建议都加上
-->

<!--
	对于table中内容的解释:
	1.tbody里面的单元格默认根据内容百分比平方table的宽度和高度
	2.行和列的宽度高度取决于当前行列中最宽最高的单元格
	3.th内容上下左右居中对齐
	4.td内容上下居中左对齐
	5.给所有td固定宽高时会将表格完全等分,也可以单独给某一个td设置
	6.td不支持margin,并且padding会很奇怪
	7.table到td之间不能嵌套任何元素,td和th中可以有任何元素
-->
```

**table样式**

```css
table{
    border:1px solid #000；
    border-spacing:0;
    border-collapse:collapse;
}
/*
	在不设置属性border="1"的时候通过上面设置css样式来让表格有边框
	border-spacing属性时边框直接的距离,浏览器默认会为table旁加上这个属性并且有值,这个时候就会看见每个表格的边框都时分离并且有一段距离的,而把这个属性设置为0时就会让距离为0,但是会出现边框合并变粗的问题
	border-collapse属性决定是否将表格边框合并默认的值时spareate分开,当使用上面的collapse属性时则会合并表格边框,而且用了这个属性之后border-spacing属性的距离会失效,因为边框已经合并了就没有边框距离可言了
*/
```

**表格的特性**

- 表格有一个属于自己的表现形式就是display:table

- 表格的子标签td,th等的表现形式为display:table-cell意味表格细胞

- 独占一行

- 不给宽高的时候,高度和宽度由内容撑开,不向块级标签一样宽度默认是百分之百

- 支持margin属性并且支持margin:0 auto这样的形式,但是对padding属性的支持效果特别奇怪,只有当不写其余和高度宽度等相关属性的时候才生效,所以可以说是不支持padding属性的



## 表单

### form标签

```html
<form name="" method="" action="url">
</form>
<!--
	name表示表单的名称
	method表示提交信息的方式,取值为post和get，默认为get，区别是get提交快但有限制，post提交慢无限制,get是通过网址传递，name信息写在网址后面，有缓存post不通过网址，要在控制台中查看,没有缓存
	action用来指定处理表单数据的程序文件所在的位置
-->
```



### input标签

```html
 <input autpfocus="autofocus" name="名称" type="类型" readonly="readonly" （只读）size="文本长度" maxlength="最大可输入字符" value="默认值">
 <!--input
	autpfocus="autofocus"属性表示文本输入字段被设置为当页面加载时获得焦点
    type属性一般不可省，如果省略则默认为text类型
                         
    type="password"为密码框
    type="submit"为提交按钮
    type="reset"为重置按钮，重置文本信息
    type="button"为普通按钮(点击按钮没有任何操作，需要用js)
   	type="radio"为单选框，用于创建单选选项，两个单选框的name属性值必须是一样的 同时可设计checked="checked"设置默认选择
	type="checkbox"为多选框，同上
	type="url"为输入路径值，自动进行验证，不合法会有提示语句
    type="email"与url相同
    type="color" 可以在选择颜色框里任意选择颜色
    type="file"用于可以点击传入文件
    type="hidden"定义一个用户看不见的input框
	type="date/month/week/time/datetime/datatime-local"date为选择日，月，年 month为选择月，年 week为选择周，年 time选择时间 datetime输入时间后，会验证是否符合格式(只有这个是输入) datetime-local选择时间.日期.月,年(本地时间）
    <input type="image" src="" width="" height="">形成一个图像域也是图像按钮
 	<input type="range或number" name="名称" min="" max="" step="步长" value="初始值">step为数字间的间隔，上方依旧为选择数字，调整数字时只能调整以step为间隔的数字,range与number只是在数字的选择形式上有差别   
                                                                           
 	size表示input表单的宽度
	vaule表示文本框里的值，如果赋值，打开网页就直接出现相关的值，如果没有则默认打开文本框为空
	readonly为只读属性，将使得文本框既不能输入也不能编辑
	placeholder="提示文本"可以在文本区域显示一段提示语句，光标地位时语句就会消失
	required="required"检测输入的数据是否为空，如果为空不能提交并显示错误
	pattern="正则表达式"用于验证input的输入是否符合规则，不符合规则就提交不了，具体的正则表达式查表
    formacti在sumbit按钮里定义提交地址,这样就不用在form的action中获取提交地址了
-->
```

```css
/*像input标签这样能够输入内容的独有的css属性*/
input:focus{
    outline:red dashed 2px;/*这个是input的外边框,和border的属性值色设置刚好相反*/
    outline-offset:5px/*边框偏移量,设置边框在偏移input外边框多远的距离显示*/
}
/*	
	focus表示只有当聚焦在input
	一般这个ouline属属性需要用时都是设置为none
*/
input::-webkit-input-placeholder{
	color:red;/*选中palceholder */
}
```



### textarea标签

```html
    <textarea rows="行数" cols="列数">
    </textarea>
	<!--
		<textarea>为多行文本区域,用于需要大量文字的地方
	-->
```



### select标签

```html
<!--
	<select></select>为选择菜单，能够下拉进行选择
-->
<select multiple="multiple">
<option value="列表中的值">说明</option>
<option value="列表中的值">说明</option>
<option value="列表中的值">说明</option>
</select> <!--里面不要加label-->
<!--
	可以加上optgroup进行分组<optgroup lable="分组名"></outgroup>
	selected="selected"属性，则该选项就被默认选中
	multiple="multiple" multiple属性可以多选，多选是按住ctrl
-->
```

**拓展标签datalist(不推荐使用)**

```html
<!--<datalist></datalist>这个标签也可以建立列表，效果和select类似,但不能单独使用，必须和一个可输入文本框类型一起配合使用,并且可以用label属性
用法如下：
-->
<input type="" list="要绑定的datalist的id" name="名称">
<datalist id="datalist的id">

<option label="列表项的说明" value="列表项的值"></option>

<option label="列表项的说明" value="列表项的值"></option>

<option label="列表项的说明" value="列表项的值"></option>

</datalist>
<!--
	datalist标记只能用于标记区域范围
	label属性设置列表项的标记
	在设置option时必须设置value
-->
```



### label标签

```html
<label for="控件id名称">
<!--
	如果你在label标签内点击文本，就会触发此控件，这个标签也可以用做包裹input框
-->
```



### fieldset标签

```html
<fieldset>
<legend>控件组的标题</legend>

。。。。

。。。。

</fieldset><!--将表单周围围起来-->
<!--
    <fieldset></fieldset>对表单内部控件进行分组，还会在周围生成边框线
    <legend></legend>用做标记标题使用
-->
```






参考链接：

1. https://www.yuque.com/coloring/web-notes/ozswsk#67246e5a
2. https://xugaoyi.com/pages/8309a5b876fc95e3/
3. https://www.kancloud.cn/donaf/vue/633913