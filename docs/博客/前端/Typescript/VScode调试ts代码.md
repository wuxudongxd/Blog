---
title: VScode调试ts代码
date: 2021-04-18 19:57:07
permalink: /pages/b09cab/
categories:
  - 博客
  - 前端
  - Typescript
tags:
  - 
---
需求：在vscode中较为方便地调试typescript单文件，尽可能少写乱七八糟的配置文件

背景：vscode在调试c，python，JavaScript这些的时候非常方便，在左侧调试选项中选中`运行和调试`，然后选择对应的环境就好。可是到了Typescript这里事情就有点微妙了。Typescript以JavaScript为编译目标，目前能直接执行ts的只有`ts-node` 和`deno`，`deno`暂时观望，直接执行ts的工作先交给 `ts-node`。

然后我就在谷歌上一顿搜索，结果发现靠谱的教程根本没几个，大致的思路是有两个吧：1. vscode监控ts文件，实时转换为js，配置调试；2. 在`.vscode/launch.json`中配置ts-node的路径和执行的ts文件路径，如下：

```json
// node -r ts-node/register src/index.ts
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "node",
      "request": "launch",
      "name": "Launch Program",
      "runtimeArgs": [
        "-r",
        "ts-node/register"
      ],
      "args": [
        "${workspaceFolder}/src/index.ts"
      ]
    }
  ]
}
```

第一个方法繁琐且不优雅就不说了，第二个需要将`ts-node`安装在文件根路径，也就意味着我想随便刷道题，写了个简单的ts函数，想要调试一下需要初始化`package.json`，安装`ts-node`生成`node_modules`，配置`.vscode/launch.json`，说不定还得配一下`tsconfig.json`...... 所以这个方法比较适合一个项目来配置，写一个单文件配一大堆东西有点得不偿失。于是我尝试`runtimeArgs`里的`ts-node`写全局安装的路径，然后报错`Could not read source map for internal/deps/acorn/acorn/dist/acorn.js`。应该是有解决办法的，当我懒得查了，没什么意思。

然后教程翻着翻着发现有提到vscode有一个`JavaScript Debug Terminal`控制台，之前也见到过这个，不过试了下好像没太大区别就没管了，现在发现这个东西可以帮我大忙。这个的用法非常简单，<mark>控制台执行`tsc -init`初始化生成一个`tsconfig.json`文件，然后切换到`JavaScript Debug Terminal`控制台，代码中记得加入断点，然后控制台执行`ts-node [ts文件名]`，就会自动进入调试状态。</mark>如果没有加断点就会直接执行代码，教程中提到要打开`"inlineSourceMap": true`选项，但我尝试后发现不打开只使用默认的`tsconfig.json`也能正常调试，就没打开了。

临时记录的，排版有点乱，总之面向配置文件编程真的不爽啊啊啊啊





补：这方法似乎有bug，文件名是中文会导致断点失效

再补：研究了一下，似乎是ts-node的问题，使用上边第二种方法，文件名是中文可以复现问题