---
title: 有关于vue的碎碎念
date: 2020-09-25 20:12:21
permalink: /pages/06c7f9/
categories: 
  - 前端
  - vue
tags: 
  - 

---

1. vue的脚手架，2.x.x的名字是vue-cli，3之后的名字是@vue/cli，安装的时候要分清

2. vue设置代理解决跨域问题

   ```js
   // vue.config.js
   module.exports = {
     devServer: {
       proxy: {
         "/api": {
           target: "http://192.168.1.195:8090", // api接口基础路径
           changeOrigin: true, // 是否支持跨域
           ws: true,
           pathRewrite: {
             "^/api": "" // 重写路径：去掉路径中开头的 '/api'
           }
         }
       }
     },
     productionSourceMap: false // 打包时不会生成 .map 文件，加快打包速度
   };
   ```
   
```js
   // src/request/request.js
   // 对网络请求进行处理
   import axios from "axios";
   
   const service = axios.create({
     baseURL: "api/",
     timeout: 5000,
     headers: {
       "Content-Type": "application/json;charset=utf-8"
     }
   });
   
   // request拦截器
   service.interceptors.request.use(
     config => {
       return config;
     },
     error => {
       return Promise.reject(error);
     }
   );
   
   // response拦截器
   service.interceptors.response.use(
     config => {
       return config;
     },
     error => {
       return Promise.reject(error);
     }
   );
   
   export default service;
   ```
   
   ```js
// src/request/request.js
   import service from "@/request/request";
   
   export function getVerifyCode() {
     return service.post("user/verifyCode");
   }
   ```
   
   ```vue
<!--App.vue-->
   <template>
     <div id="app">
       <button @click="verifyCode">verifyCode</button>
     </div>
   </template>
   
   <style lang="scss"></style>
   
   <script>
   import { getVerifyCode } from "@/request/api";
   export default {
     name: "app",
     methods: {
       verifyCode() {
         getVerifyCode()
           .then(res => {
             console.log(res);
           })
           .catch(err => {
             console.log(err);
           });
       }
     }
   };
   </script>
   ```
   
   记得重新运行项目，热重启可能会没有效果

   用代理, 首先得有一个标识, 告诉他这个连接要用代理. 不然的话, 可能 html, css, js这些静态资源都跑去代理. 所以我们只要接口用代理, 静态文件用本地.
   
   `'/api': {}`, 就是告诉`node`, 我接口只要是`'/api'`开头的才用代理。所以接口就要这么写 `/api/xx/xx`. 最后代理的路径就是 `http://xxx.xx.com/api/xx/xx`.
   
   可是不对啊, 我正确的接口路径里面没有`/iclient`啊. 所以就需要 `pathRewrite`,用`'^/iclient':''`, 把`'/iclient'`去掉, 这样既能有正确标识, 又能在请求接口的时候去掉`iclient`.
   
   `'^/api'` 其实是一个正则表达式，`'^/api'` 应该拆分成 `'^'` 和 `'/api'` 两个字符串，其中 `'^'` 匹配的是字符串最开始的位置。也就是说，axios 的请求URL写成了 `'/api/myAPI/path'` 的话，最后会在经过 `http-proxy-middleware` 的代理服务器时，会变成 `'/myAPI/path'` ，然后代理到 `http://localhost:8080` 这个代理服务器下面。

