---
title: vue开发规范
date: 2020-10-18 17:15:20
permalink: /pages/cd4268/
categories:
  - 前端
  - vue
tags:
  - 
---
## 命名规范

### id和class的命名原则

* class：采用"中划线法命名法"，命名规则用"-" 隔开，避免驼峰命名，如 el-button
* id：采用"小驼峰命名法"

> 书写CSS要注意先后顺序和嵌套问题，从性能上考虑尽量减少选择器的层级

### 文件夹、文件名

1. 文件夹

   目录由小写的名词、动名词或分词命名，由两个以上的词组成，以"-"进行分隔

   * 由名词组成 （car、order、cart）
   * 尽量是名词 （good：car）（bad：greet）
   * 以小写开头 （good： car） （bad： Car）

2. 资源文件名

   资源文件名一律以小写字符命名，由两个以上的词组成，以"-"进行分隔。例如：mian.css、index.js

### Vue文件命名

* Vue文件统一以大驼峰命名法命名，仅入口文件index.vue采用小写
*   views下面的Vue文件夹代表着页面的名字，放在模块文件夹之下。只有一个文件的情况下不会出现文件夹，而是直接放在views目录下面，如Login、Home等
* 尽量使用名词，且大写字母开头，开头的单词就是所属模块名字（CarDetail、CarEdit、CarList）.
* 常用结尾单词有（Detail、Edit、List、Info、Report）
* 以Item结尾的代表着组件 (CarListItem、CarInfoItem)
* components 文件夹存放的是可复用公共组件，放在模块文件夹下，文件夹命令同模块命名，小写字母开头，如common。大写字母开头，如common/Footer、common/Header(文件名采用"大驼峰命名法")

### Vue路由命名

采用带问号的history路由路径方式命名。路由及参数名称采用首字母小写的驼峰法命名，比如`http://www.test.com/test?name=abc&testId=124`

### Vue方法放置顺序

* name
* components
* props
* data
* created
* mounted
* filter
* computed
* watch
* activied
* update
* beforeRouteUpdate
* metodes

### method 自定义方法命名

* 动宾短语（good：jumpPage、openCarInfoDialog）（bad：go、nextPage、show、open、login）
* Ajax 方法以get、post开头，以Data结尾（good：getListData、postFormData） （bad：takeData、confirmData、getList、postForm）
* 事件方法以on开头（onTypeChange、onUsernameInput），init，refresh单词除外
* 尽量使用常用单词开头（set、get、open、close、jump）
* 驼峰命名（good：getListData）（bad：get_list_data、getlistData）

（1）data props 方法注意点

* 使用data里的变量时先在data里初始化
* props指定类型，也就是type
* props改变父组件数据基础类型用$emit，复杂类型直接改

（2）自定义方法命名注意事项

* 表单数据请包裹一层form
* 不在mounted、created之类的方法写逻辑，取Ajax数据
* 在created里面监听Bus事件

### 注意事项

1. 尽量避免直接操作DOM
2. 尽量使用Vue的语法糖，比如可以用:style替代v-bind:style ，用@click替代v-on:click
3. 将业务类型的CSS单独写一个文件，然而功能型的CSS最后和组件一起，不推荐拆离，比如一个通用的confirm确认框

