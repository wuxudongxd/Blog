---
title: nuxt开发笔记
date: 2020-10-23 15:14:17
permalink: /pages/74bdff/
categories:
  - 前端
  - vue
tags:
  - 
---
2. 配置服务端和客户端的跨域

   nuxt的跨域配置需要同时满足服务端和客户端的请求，最好是在axios配置项里加入 `prefix: '/api'`作为前缀。

   ```javascript
   // nuxt.config.js  
   // Modules (https://go.nuxtjs.dev/config-modules)
     modules: [
       // https://go.nuxtjs.dev/axios
       '@nuxtjs/axios',
     ],
   
     axios: {
       proxy: true, // 表示开启代理
       prefix: '/api', // 表示给请求url加个前缀 /api
       credentials: true, // 表示跨域请求时是否需要使用凭证
     },
   
     proxy: {
       '/api': {
         target: 'http://xxx.com', // 目标接口域名
         changeOrigin: true, // 表示是否跨域
         pathRewrite: {
           '^/api': '/', // 把 /api 替换成 /
         },
       },
     },
   ```

   反例：

   ```javascript
   // nuxt.config.js
   env: {
       baseUrl: '/api',
     },
         
   axios: {
       proxy: true, // 表示开启代理
       credentials: true, // 表示跨域请求时是否需要使用凭证
     },
   
     proxy: {
       '/api': {
         target: 'http://xxx.com', // 目标接口域名
         changeOrigin: true, // 表示是否跨域
      pathRewrite: {
           '^/api': '/', // 把 /api 替换成 /
         },
       },
     }, 
   ```
   
   ```javascript
   // axios.js
   export default function ({ app: { $axios } }) {
     // 基本配置
     $axios.defaults.baseURL = process.env.baseUrl
   }
   
   ```
   
   没有设置`prefix`，而是设置axios的`baseURL`，像这样配置会导致服务器端请求的是127.0.0.1:80，而客户端请求正常，原因正在研究

3. 自动化部署

   

   