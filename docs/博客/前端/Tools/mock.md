---
title: mock
date: 2020-12-24 14:57:38
permalink: /pages/3e1e41/
categories:
  - 博客
  - 前端
tags:
  - 
article: false
---
```javascript
// 使用 Mock
let Mock = require('mockjs');
Mock.mock('http://1.json','get',{
    // 属性 list 的值是一个数组，其中含有 1 到 3 个元素
    'list|1-3': [{
        // 属性 sid 是一个自增数，起始值为 1，每次增 1
        'sid|+1': 1,
        // 属性 userId 是一个5位的随机码
        'userId|5': '',
        // 属性 sex 是一个bool值
        "sex|1-2": true,
        // 属性 city对象 是对象值中2-4个的值
        "city|2-4": {
            "110000": "北京市",
            "120000": "天津市",
            "130000": "河北省",
            "140000": "山西省"
        },
        //属性 grade 是数组当中的一个值
        "grade|1": [
            "1年级",
            "2年级",
            "3年级"
        ],
        //属性 guid 是唯一机器码
        'guid': '@guid',
        //属性 id 是随机id
        'id': '@id',
        //属性 title 是一个随机长度的标题
        'title': '@title()',
        //属性 paragraph 是一个随机长度的段落
        'paragraph': '@cparagraph',
        //属性 image 是一个随机图片 参数分别为size, background, text
        'image': "@image('200x100', '#4A7BF7', 'Hello')",
        //属性 address 是一个随机地址
        'address': '@county(true)',
        //属性 date 是一个yyyy-MM-dd 的随机日期
        'date': '@date("yyyy-MM-dd")',
        //属性 time 是一个 size, background, text 的随机时间
        'time': '@time("HH:mm:ss")',
        //属性 url 是一个随机的url
        'url': '@url',
        //属性 email 是一个随机email
        'email': '@email',
        //属性 ip 是一个随机ip
        'ip': '@ip',
        //属性 regexp 是一个正则表达式匹配到的值 如aA1
        'regexp': /[a-z][A-Z][0-9]/,
    }]
})
```

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<script src="http://mockjs.com/dist/mock.js"></script>
		<title>测试mockjs生成测试数据</title>
	</head>
	<body>
	</body>
	<script type="text/javascript">
		// 拓展mockjs
		Mock.Random.extend({
		  phone: function () {
		    var phonePrefixs = ['132', '135', '189'] // 自己写前缀哈
		    return this.pick(phonePrefixs) + Mock.mock(/\d{8}/) //Number()
		  }
		})
		
		// 使用 Mock
		// var Mock = require('mockjs')
		// var data = Mock.mock({
		//     // 属性 list 的值是一个数组，其中含有 1 到 10 个元素
		//     'list|1-10': [{
		//         // 属性 id 是一个自增数，起始值为 1，每次增 1
		//         'id|+1': 1,
		// 	'cname': '@cname',
		// 	'phoneNum': '@phone'
		//     }]
		// })
		// // console.log(Mock.mock('@cname'))
		// // 输出结果
		
		
		// 如果是相关联的几组数据，最好让项目经理/产品经理给一组假数据（可以更加体现专业性，不至于很不合理的数据），并不是说一定要用随机假数据
		// - 比如水位可能和雨量有关联（雨量大水位高）
		var data = Mock.mock({
			'sw|7': ['@float(20, 29, 2, 2)'],
			'yl|7': ['@integer(0, 249)'],
			'll|7': ['@integer(0, 799)'],
			'ph|7': ['@float(1, 13, 1, 1)'],
			'zd|7': ['@float(0, 0.9, 2, 2)'],
			// zl 的范围是 0-0.5，因为 mockjs 的 @float(0, 0.5, 2, 2) 随机出来的结果其实是 0 - 1 直接的，所以要自己处理一下
			'zl|7': ['@natural(0, 50)']
		})
		// js 批量对那个小数数组做处理
		for(var i = 0; i < data.zl.length; i++) {
			data.zl[i] = data.zl[i] / 100
		}
		console.log(data)
		/** 数据
		{
			"sw": [25.18, 25.19, 26.68, 27.26, 23.29, 29.76, 28.57],
			"yl": [49, 125, 230, 171, 138, 35, 240],
			"ll": [566, 435, 132, 199, 309, 789, 176],
			"ph": [12.8, 6.4, 12.8, 13.7, 6.1, 11.8, 7.2],
			"zd": [0.02, 0.87, 0.24, 0.36, 0.53, 0.37, 0.06],
			"zl": [0.39, 0.05, 0.15, 0.45, 0.01, 0.05, 0.1]
		}
		*/
		console.log(JSON.stringify(data))
		
		// console.log(JSON.stringify(data, null, 4))
		// Mock.Random.natural(0, 100) / 100
		
		
		// console.log(Mock.Random.province())
		// 生成省：@mock=@province()
		// 生成市：@mock=@city()
		// 生成县：@mock=@county()
		// 生成url：@mock=@url()
		// 生成姓名：@mock=@cname()
		// 生成汉字：@mock=@cword(2,5)
		// 生成句子：@mock=@csentence(2,5)生成段落：@mock=@cparagraph(3)
		// 生成图片：@mock=@img(100x100)
		// 生成颜色：@mock=@imgcolor()
		// 生成日期：@mock=@date(yy-mm-dd)
		// 生成时间：@mock=@time(hh-mm-ss)
		// 生成自增：@mock=@increment(1)
		// 生成自然数：@mock=@natural(1,100)
		// 生成整数：@mock=@integer(1,100)
		// 生成小数：@mock=@float(1,100,2,3)
		
		
		var data = Mock.mock({'data|7': [{
			'sw': '@float(20, 29, 2, 2)',
			'yl': '@integer(0, 249)',
			'll': '@integer(0, 799)',
			'ph': '@float(1, 13, 1, 1)',
			'zd': '@float(0, 0.9, 2, 2)',
			// zl 的范围是 0-0.5，因为 mockjs 的 @float(0, 0.5, 2, 2) 随机出来的结果其实是 0 - 1 直接的，所以要自己处理一下
			'zl': '@natural(0, 50)'
		}]}).data
		// js 批量对那个小数数组做处理
		for(var i = 0; i < data.length; i++) {
			data[i].zl = data[i].zl / 100
		}
		console.log(data)
		/** 数据
		[
			{"sw":27.27,"yl":2,"ll":379,"ph":11.7,"zd":0.65,"zl":0.09},
			{"sw":25.65,"yl":117,"ll":665,"ph":10.7,"zd":0.38,"zl":0.41},
			{"sw":23.16,"yl":234,"ll":386,"ph":2.1,"zd":0.84,"zl":0.42},
			{"sw":21.08,"yl":137,"ll":797,"ph":9.2,"zd":0.74,"zl":0.4},
			{"sw":21.22,"yl":222,"ll":381,"ph":6.7,"zd":0.97,"zl":0.16},
			{"sw":25.28,"yl":93,"ll":278,"ph":5.7,"zd":0.89,"zl":0.17},
			{"sw":24.13,"yl":69,"ll":463,"ph":5.7,"zd":0.46,"zl":0.5}
		]
		*/
		console.log(JSON.stringify(data))
		
	</script>
</html>
```



```javascript
const Mock = require("mockjs");
let data = Mock.mock({
    "data|100": [ //生成100条数据 数组
        {
            "shopId|+1": 1,//生成商品id，自增1
            "shopMsg": "@ctitle(10)", //生成商品信息，长度为10个汉字
            "shopName": "@cname",//生成商品名 ， 都是中国人的名字
            "shopTel": /^1(5|3|7|8)[0-9]{9}$/,//生成随机电话号
            "shopAddress": "@county(true)", //随机生成地址
            "shopStar|1-5": "★", //随机生成1-5个星星
            "salesVolume|30-1000": 30, //随机生成商品价格 在30-1000之间
            "shopLogo": "@Image('100x40','#c33', '#ffffff','小北鼻')", //生成随机图片，大小/背景色/字体颜色/文字信息
            "food|7": [ //每个商品中再随机生成七个food
                {
                    "foodName": "@cname", //food的名字
                    "foodPic": "@Image('100x40','#c33', '#ffffff','小可爱')",//生成随机图片，大小/背景色/字体颜色/文字信息
                    "foodPrice|1-100": 20,//生成1-100的随机数
                    "aname|14": [
                        { 
                            "aname": "@cname", 
                            "aprice|30-60": 20 
                        }
                    ]
                }
            ]
        }
    ]
})
Mock.mock(/goods\/goodAll/, 'post', () => { //三个参数。第一个路径，第二个请求方式post/get，第三个回调，返回值
    return data
})
```

