---
title: npm与yarn
date: 2020-09-25 15:44:34
permalink: /pages/90327c/
categories: 
  - 前端
tags: 
  - 前端
---

| yarn                      | npm                              |
| ------------------------- | -------------------------------- |
| yarn init                 | npm init                         |
| yarn                      | npm install                      |
| yarn global add xxx@x.x.x | npm install xxx@x.x.x -g         |
| yarn add xxx@x.x.x        | npm install xxx@x.x.x --save     |
| yarn add xxx@x.x.x --dev  | npm install xxx@x.x.x --save-dev |
| yarn remove xxx           | npm uninstall xxx --save(-dev)   |
| yarn run xxx              | npm run xxx                      |

yarn卸载全局包的时候不需要指定版本号`yarn global remove <package> `，如：title: npm与yarn

```powershell
PS C:\Users\DongDong\Downloads> yarn global list --depth=0
yarn global v1.22.5
Done in 0.06s.
PS C:\Users\DongDong\Downloads> yarn  global add @vue/cli
yarn global v1.22.5
info There appears to be trouble with your network connection. Retrying...
[2/4] Fetching packages...
info fsevents@1.2.13: The platform "win32" is incompatible with this module.
 it from installation.
[3/4] Linking dependencies...
[4/4] Building fresh packages...
success Installed "@vue/cli@4.5.6" with binaries:
      - vue
Done in 67.45s.
PS C:\Users\DongDong\Downloads> yarn global list --depth=0
yarn global v1.22.5
info "@vue/cli@4.5.6" has binaries:
   - vue
Done in 1.09s.
PS C:\Users\DongDong\Downloads> yarn global remove @vue/cli@4.5.6
yarn global v1.22.5
[1/2] Removing module @vue/cli@4.5.6...
error This module isn't specified in a package.json file.
info Visit https://yarnpkg.com/en/docs/cli/global for documentation about this command.
PS C:\Users\DongDong\Downloads> yarn global remove @vue/cli
yarn global v1.22.5
[1/2] Removing module @vue/cli...
[2/2] Regenerating lockfile and installing missing dependencies...
success Uninstalled packages.
Done in 8.55s.
```

目前的npm6安装依赖速度还是慢，接着用yarn，yarn好处多多 