// nav
module.exports = [
  {
    text: "首页",
    link: "/",
  },
  {
    text: "归档",
    link: "/archives/",
  },
  // {
  //   text: "开发",
  //   link: "/note/frontend/",
  // },
  {
    text: "收藏",
    link: "/collection/",
  },
  {
    text: "友链",
    link: "/friends/",
  },
  {
    text: "关于",
    link: "/about/",
  },
];
