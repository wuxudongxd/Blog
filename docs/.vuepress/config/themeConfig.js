const nav = require("./themeConfig/nav.js");
const htmlModules = require("./themeConfig/htmlModules.js");

// 主题配置
module.exports = {
  nav,
  sidebarDepth: 2, // 侧边栏显示深度，默认1，最大2（显示到h3标题）
  logo: "", // 导航栏logo
  searchMaxSuggestions: 10, // 搜索结果显示最大数
  lastUpdated: "上次更新", // 开启更新时间，并配置前缀文字   string | boolean (取值为git提交时间)
  docsDir: "docs", // 编辑的文件夹
  editLinks: true, // 启用编辑
  editLinkText: "编辑",
  contentBgStyle: 1, // 文章内容块的背景风格，默认无. 1 => 方格 | 2 => 横线 | 3 => 竖线 | 4 => 左斜线 | 5 => 右斜线 | 6 => 点状

  updateBar: {
    // 最近更新栏
    showToArticle: false, // 显示到文章页底部，默认true
    moreArticle: "/archives", // “更多文章”跳转的页面，默认'/archives'
  },
  sidebarOpen: false, // 初始状态是否打开侧边栏，默认true

  author: {
    // 文章默认的作者信息，可在md文件中单独配置此信息 String | {name: String, link: String}
    name: "东东", // 必需
    link: "https://github.com/wuxudongxd", // 可选的
  },
  blogger: {
    // 博主信息，显示在首页侧边栏
    avatar: "/img/avatar.jpg",
    name: "东东",
    // slogan: '前端界的小学生'
  },
  social: {
    // 社交图标，显示于博主信息栏和页脚栏
    // iconfontCssFile: '//at.alicdn.com/t/font_1678482_u4nrnp8xp6g.css', // 可选，阿里图标库在线css文件地址，对于主题没有的图标可自由添加
    icons: [
      {
        iconClass: "icon-youjian",
        title: "发邮件",
        link: "mailto:wuxudongxd@qq.com",
      },
      {
        iconClass: "icon-github",
        title: "GitHub",
        link: "https://github.com/wuxudongxd",
      },
    ],
  },
  footer: {
    // 页脚信息
    createYear: 2020, // 博客创建年份
    copyrightInfo:
      '<br>备案号：<a href="https://beian.miit.gov.cn/" target="_blank">蜀ICP备19039601号-2</a>' +
      '<br>本站博客除另有说明外，均遵循<a href="https://creativecommons.org/licenses/by-sa/4.0/deed.zh" target="_blank"> CC 4.0 BY-SA </a> 版权协议，转载请附上原文出处链接和本声明', // 博客版权信息，支持a标签
  },
  htmlModules, // 插入hmtl(广告)模块
};
