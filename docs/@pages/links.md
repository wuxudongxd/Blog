---
title: 友链
date: 2019-12-25 14:27:01
permalink: /friends
article: false
sidebar: false
---

<!-- 
普通卡片列表容器，可用于友情链接、项目推荐、古诗词展示等。
cardList 后面可跟随一个数字表示每行最多显示多少个，选值范围1~4，默认3。在小屏时会根据屏幕宽度减少每行显示数量。
-->
::: cardList
```yaml
- name: hanbaoaaa
  desc: 钟大佬的博客
  avatar: https://hanbaoaaa.xyz/favicon.ico # 可选
  link: https://hanbaoaaa.xyz/ # 可选
  bgColor: '#CBEAFA' # 可选，默认var(--bodyBg)。颜色值有#号时请添加单引号
  textColor: '#6854A1' # 可选，默认var(--textColor)
- name: c0's Blog
  desc: printf("Hi.");
  avatar: https://cdn.jsdelivr.net/npm/czs/avatar/192x192.png # 可选
  link: https://ciph.zone # 可选
  bgColor: '#eaff8f' # 可选，默认var(--bodyBg)。颜色值有#号时请添加单引号
  textColor: '#212121' # 可选，默认var(--textColor)
- name: 吃猫的鱼
  desc: 再烂的牌，也会有一张有最大的
  avatar: https://img.wuxd.top/img/20210111215101.png!webp # 可选
  link: https://www.cnblogs.com/ast935478677/ # 可选
  bgColor: '#ffe5b4' # 可选，默认var(--bodyBg)。颜色值有#号时请添加单引号
  textColor: '#a05f2c' # 可选，默认var(--textColor)
- name: DiliTech
  desc: ZYLHL
  avatar: https://avatars1.githubusercontent.com/u/47456685?s=460&v=4 # 可选
  link: https://dlstudio.top/ # 可选
  bgColor: '#B7DBFF' # 可选，默认var(--bodyBg)。颜色值有#号时请添加单引号
  textColor: '#3a5e82' # 可选，默认var(--textColor)
- name: lengaotian
  desc: 冷傲天的博客
  avatar: http://lengat.top/img/avatar.jpg
  link: http://lengat.top/ # 可选
  bgColor: '#FBEBEC' # 可选，默认var(--bodyBg)。颜色值有#号时请添加单引号
  textColor: '#603420' # 可选，默认var(--textColor)
```
:::